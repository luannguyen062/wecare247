import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:wecare247/shared/index.dart';

class CustomDateRangePicker extends StatefulWidget {
  final ValueNotifier<DateTime> startDateNotifier;
  final ValueNotifier<DateTime> endDateNotifier;
  CustomDateRangePicker({
    Key key,
    this.startDateNotifier,
    this.endDateNotifier,
  }) : super(key: key);

  @override
  _CustomDateRangePickerState createState() => _CustomDateRangePickerState();
}

class _CustomDateRangePickerState extends State<CustomDateRangePicker> {
  DateFormat dateFormat;
  DateFormat timeFormat;

  @override
  void initState() {
    super.initState();
    dateFormat = DateFormat("dd/MM/yyyy");
    timeFormat = DateFormat.Hm();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SfDateRangePicker(
          selectionMode: DateRangePickerSelectionMode.range,
          showNavigationArrow: true,
          initialSelectedRange: PickerDateRange(
              widget.startDateNotifier.value, widget.endDateNotifier.value),
          onSelectionChanged: (arg) {
            widget.startDateNotifier.value =
                (arg.value as PickerDateRange).startDate;
            widget.endDateNotifier.value =
                (arg.value as PickerDateRange).endDate;
          },
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Row(
            children: [
              Text(
                "Từ",
                style: fontBook.copyWith(fontSize: 14),
              ),
              SizedBox(width: 8),
              Expanded(
                child: ValueListenableBuilder(
                    valueListenable: widget.startDateNotifier,
                    builder: (context, value, child) {
                      String display = "";
                      if (value != null) {
                        display = DateFormat("dd/MM/yyyy").format(value);
                      }
                      return Text(
                        display,
                        textAlign: TextAlign.right,
                        style: fontBook.copyWith(fontSize: 14),
                      );
                    }),
              ),
              SizedBox(width: 8),
              Text(
                "đến",
                style: fontBook.copyWith(fontSize: 14),
              ),
              SizedBox(width: 8),
              Expanded(
                child: ValueListenableBuilder(
                    valueListenable: widget.endDateNotifier,
                    builder: (context, value, child) {
                      String display = "";
                      if (value != null) {
                        display = DateFormat("dd/MM/yyyy").format(value);
                      }
                      return Text(
                        display,
                        textAlign: TextAlign.right,
                        style: fontBook.copyWith(fontSize: 14),
                      );
                    }),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
