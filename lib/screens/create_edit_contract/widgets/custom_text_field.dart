import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final TextEditingController controller;
  final String title;
  final bool isDisabled;
  final TextInputType keyboardType;
  final String Function(String) validator;
  final int maxLines;
  final TextInputAction textInputAction;
  final void Function(String) onChanged;
  CustomTextField(
      {Key key,
      this.controller,
      this.title,
      this.isDisabled = false,
      this.keyboardType,
      this.validator,
      this.onChanged,
      this.maxLines,
      this.textInputAction = TextInputAction.next})
      : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  FocusNode focusNode;
  @override
  void initState() {
    super.initState();
    // focusNode = FocusNode()
    //   ..addListener(() {
    //     if (!focusNode.hasFocus) {
    //       if (widget.onSaved != null) {
    //         widget.onSaved(widget.controller.text);
    //       }
    //     }
    //   });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: widget.validator,
      controller: widget.controller,
      // focusNode: focusNode,
      decoration: InputDecoration(
        border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[600], width: 2),
            borderRadius: BorderRadius.circular(10)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: UIColors.mainColor(), width: 2),
            borderRadius: BorderRadius.circular(10)),
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 2),
            borderRadius: BorderRadius.circular(10)),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[600], width: 2),
            borderRadius: BorderRadius.circular(10)),
        disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[600], width: 2),
            borderRadius: BorderRadius.circular(10)),
        labelText: widget.title,
        labelStyle: fontMedium.copyWith(color: Colors.grey[600], fontSize: 14),
        contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
        isDense: true,
      ),
      autovalidateMode: AutovalidateMode.disabled,
      enabled: !widget.isDisabled,
      maxLines: widget.maxLines ?? 1,
      style: fontMedium.copyWith(fontSize: 14, color: Colors.black),
      keyboardType: widget.keyboardType,
      textInputAction: TextInputAction.next,
      onChanged: widget.onChanged,
      onEditingComplete: () {
        if (widget.textInputAction == TextInputAction.next)
          FocusScope.of(context).nextFocus();
        else
          FocusScope.of(context).unfocus();
      },
    );
  }
}
