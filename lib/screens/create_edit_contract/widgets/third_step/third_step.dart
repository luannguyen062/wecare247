import 'package:wecare247/models/index.dart';
import 'package:wecare247/screens/create_edit_contract/utils/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/third_step/widgets/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ThirdStep extends StatefulWidget {
  final Key formKey;
  final ListNotifier serviceListNotifier;
  final ListNotifier<ServiceEditData> serviceCreationListNotifier;
  ThirdStep(
      {Key key,
      this.formKey,
      this.serviceListNotifier,
      this.serviceCreationListNotifier})
      : super(key: key);

  @override
  _ThirdStepState createState() => _ThirdStepState();
}

class _ThirdStepState extends State<ThirdStep> {
  Widget buildServicePicker() {
    return ValueListenableBuilder(
        builder: (context, value, child) {
          return ServicePicker(
            services: value,
            serviceCreation: widget.serviceCreationListNotifier.value[0],
            serviceCreationListNotifier: widget.serviceCreationListNotifier,
            deletable: false,
          );
        },
        valueListenable: widget.serviceListNotifier);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget.formKey,
      child: ListView(
        padding: EdgeInsets.all(24),
        children: [
          TitleRow(title: "Đăng kí dịch vụ"),
          SizedBox(height: 16),
          buildServicePicker(),
          SizedBox(height: 16),
        ],
      ),
    );
  }
}
