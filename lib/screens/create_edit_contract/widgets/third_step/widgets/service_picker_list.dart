import 'package:wecare247/models/index.dart';
import 'package:wecare247/screens/create_edit_contract/utils/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/third_step/widgets/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';

class ServicePickerList extends StatefulWidget {
  final List<Service> services;
  final ListNotifier<ServiceEditData> serviceCreationListNotifier;
  final ValueNotifier<Location> locationNotifier;
  ServicePickerList(
      {Key key,
      this.services,
      this.serviceCreationListNotifier,
      this.locationNotifier})
      : super(key: key);

  @override
  _ServicePickerListState createState() => _ServicePickerListState();
}

class _ServicePickerListState extends State<ServicePickerList> {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<List<ServiceEditData>>(
      valueListenable: widget.serviceCreationListNotifier,
      builder: (context, value, child) {
        List<Widget> children = [];
        if (value.isNotEmpty) {
          children.addAll(List<Widget>.from(value.map((e) => ServicePicker(
                serviceCreation: e,
                services: widget.services,
                serviceCreationListNotifier: widget.serviceCreationListNotifier,
              ))));
        }
        children.add(TextButton(
          child: Center(
              child: Text(
            "Thêm dịch vụ",
            style: fontMedium.copyWith(fontSize: 16, color: Colors.white),
          )),
          style: TextButton.styleFrom(
            backgroundColor: UIColors.mainColor(),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
                side: BorderSide(color: UIColors.mainColor(), width: 2)),
          ),
          onPressed: () => widget.serviceCreationListNotifier.add(
              ServiceEditData(locationId: widget.locationNotifier.value.id)),
        ));
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: children,
        );
      },
    );
  }
}
