import 'package:wecare247/models/index.dart';
import 'package:wecare247/screens/create_edit_contract/utils/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ServicePicker extends StatefulWidget {
  final List<Service> services;
  final ListNotifier<ServiceEditData> serviceCreationListNotifier;
  final ServiceEditData serviceCreation;
  final bool deletable;
  ServicePicker({
    Key key,
    this.services,
    this.serviceCreationListNotifier,
    this.serviceCreation,
    this.deletable = true,
  }) : super(key: key);

  @override
  _ServicePickerState createState() => _ServicePickerState();
}

class _ServicePickerState extends State<ServicePicker> {
  ValueNotifier<DateTime> fromDate;
  ValueNotifier<DateTime> toDate;
  ValueNotifier<Service> service;
  TextEditingController priceController;

  @override
  void initState() {
    super.initState();
    fromDate = ValueNotifier<DateTime>(widget.serviceCreation.from);
    toDate = ValueNotifier<DateTime>(widget.serviceCreation.to);
    service = ValueNotifier<Service>(widget.services.firstWhere(
        (item) => item.id == widget.serviceCreation.serviceId,
        orElse: () => null));
    priceController = TextEditingController(
        text: (widget.serviceCreation.price ?? 0).toString());
    priceController.addListener(() {
      return widget.serviceCreation.price =
          double.tryParse(priceController.text);
    });
    fromDate.addListener(() => widget.serviceCreation.from = fromDate.value);
    toDate.addListener(() => widget.serviceCreation.to = toDate.value);
    service.addListener(() {
      widget.serviceCreation.serviceId = service.value.id;
      widget.serviceCreation.price = (service.value.price ?? 0);
      priceController.text = widget.serviceCreation.price.toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    final serviceName = "Dịch vụ " +
        (widget.serviceCreationListNotifier.value
                    .indexOf(widget.serviceCreation) +
                1)
            .toString();
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        TitleRow(
          title: serviceName,
        ),
        SizedBox(height: 8),
        CustomDropdown<Service>(
          choices: widget.services,
          notifier: service,
          textConverter: (item) => item.name,
          title: "Loại hình dịch vụ",
          validator: (e) => e != null ? null : "",
        ),
        SizedBox(height: 8),
        ValueListenableBuilder<Service>(
          valueListenable: service,
          builder: (context, value, child) {
            if (value == null) {
              return Container();
            }
            return Column(
              children: [
                TitleRow(title: "Giá dịch vụ theo ngày"),
                CustomTextField(
                  controller: priceController,
                  keyboardType: TextInputType.number,
                  title: "Giá dịch vụ theo ngày",
                  isDisabled: !value.priceNegotiation,
                  validator: validateNumber,
                ),
              ],
            );
          },
        ),
        SizedBox(height: 8),
        TitleRow(title: "Thời gian"),
        CustomDateRangePicker(
          startDateNotifier: fromDate,
          endDateNotifier: toDate,
        ),
        SizedBox(height: 8),
        widget.deletable
            ? ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.transparent,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                      side: BorderSide(color: Colors.red, width: 2)),
                ),
                child: Center(
                    child: Text(
                  "Xóa $serviceName",
                  style: fontMedium.copyWith(fontSize: 14, color: Colors.red),
                )),
                onPressed: () => widget.serviceCreationListNotifier
                    .remove(widget.serviceCreation),
              )
            : Container(),
        SizedBox(height: 8),
      ],
    );
  }

  @override
  void dispose() {
    fromDate.dispose();
    toDate.dispose();
    service.dispose();
    priceController.dispose();
    super.dispose();
  }
}
