import 'package:wecare247/models/index.dart';
import 'package:wecare247/screens/create_edit_contract/utils/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class FourthStep extends StatefulWidget {
  final TextEditingController contractNoteController;
  final ListNotifier locationListNotifier;
  final ValueNotifier<Location> locationNotifier;
  final ListNotifier locationDepartmentListNotifier;
  final ValueNotifier<LocationDepartment> locationDepartmentNotifier;
  final ListNotifier contractSourceListNotifier;
  final ValueNotifier<ContractSource> contractSourceNotifier;
  final Key formKey;
  FourthStep(
      {Key key,
      this.contractNoteController,
      this.locationListNotifier,
      this.locationNotifier,
      this.locationDepartmentListNotifier,
      this.locationDepartmentNotifier,
      this.contractSourceListNotifier,
      this.contractSourceNotifier,
      this.formKey})
      : super(key: key);

  @override
  _SecondStepState createState() => _SecondStepState();
}

class _SecondStepState extends State<FourthStep> {
  Widget buildContractLocationDropdown() {
    return ValueListenableBuilder(
      valueListenable: widget.locationListNotifier,
      builder: (context, value, child) {
        if (value == null) {
          return Container();
        }
        return CustomDropdown<Location>(
          choices: value,
          notifier: widget.locationNotifier,
          textConverter: (item) => item.name,
          validator: (value) => validateNonEmptyString(value?.name),
          title: "Nơi đăng ký hợp đồng",
        );
      },
    );
  }

  Widget buildLocationDepartmentDropdown() {
    return ValueListenableBuilder(
      valueListenable: widget.locationDepartmentListNotifier,
      builder: (context, value, child) {
        if (value == null) {
          return Container();
        }
        return CustomDropdown<LocationDepartment>(
          choices: value,
          notifier: widget.locationDepartmentNotifier,
          textConverter: (item) => item.name,
          validator: (value) => validateNonEmptyString(value?.name),
          title: "Khoa bệnh",
        );
      },
    );
  }

  Widget buildContractSourceDropdown() {
    return ValueListenableBuilder(
      valueListenable: widget.contractSourceListNotifier,
      builder: (context, value, child) {
        if (value == null) {
          return Container();
        }
        return CustomDropdown<ContractSource>(
          choices: value,
          notifier: widget.contractSourceNotifier,
          textConverter: (item) => item.name,
          validator: (value) => validateNonEmptyString(value?.name),
          title: "Nguồn hợp đồng",
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final todayInString = DateFormat.yMd().add_jm().format(DateTime.now());
    return Form(
      key: widget.formKey,
      child: ListView(
        padding: EdgeInsets.all(24),
        children: [
          TitleRow(title: "Thông tin hợp đồng"),
          SizedBox(height: 8),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Ngày làm hợp đồng",
                  style: fontMedium.copyWith(
                    color: Colors.green,
                    fontSize: 14,
                  ),
                ),
                FittedBox(
                  fit: BoxFit.contain,
                  child: Text(
                    todayInString,
                    style: fontMedium.copyWith(
                      color: Colors.black,
                      fontSize: 14,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 16),
          buildContractSourceDropdown(),
          SizedBox(height: 16),
          buildContractLocationDropdown(),
          SizedBox(height: 16),
          buildLocationDepartmentDropdown(),
          SizedBox(height: 16),
          TitleRow(title: "Lưu ý khác"),
          SizedBox(height: 16),
          CustomTextField(
              controller: widget.contractNoteController, maxLines: 6)
        ],
      ),
    );
  }
}
