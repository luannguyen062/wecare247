import 'package:wecare247/screens/create_edit_contract/utils/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/index.dart';
import 'package:wecare247/models/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SecondStep extends StatefulWidget {
  final List<PatientCarerSkill> capabilities;
  final ListNotifier capabilityGroupNotifier;
  final ListNotifier weightGroupNotifier;
  final ValueNotifier<PatientCarerSkill> weightNotifier;
  final ValueNotifier<PatientCarerSkill> actionCapabilityNotifier;
  final ListNotifier actionCapabilityGroupNotifier;
  final Key formKey;
  SecondStep(
      {Key key,
      this.formKey,
      this.actionCapabilityNotifier,
      this.capabilities,
      this.capabilityGroupNotifier,
      this.weightGroupNotifier,
      this.actionCapabilityGroupNotifier,
      this.weightNotifier})
      : super(key: key);

  @override
  _SecondStepState createState() => _SecondStepState();
}

class _SecondStepState extends State<SecondStep> {
  Widget buidlCapabilitiesOption() {
    return ValueListenableBuilder(
      valueListenable: widget.capabilityGroupNotifier,
      builder: (context, value, child) {
        if (value == null) {
          return Container();
        }
        return Column(
          children:
              List<Widget>.from((value as List<PatientCarerSkill>).map((input) {
            return Padding(
              padding: EdgeInsets.only(top: 8),
              child: CustomCheckboxRow<PatientCarerSkill>(
                itemList: widget.capabilities,
                item: input,
                itemAsString: (skill) => skill.description,
              ),
            );
          })),
        );
      },
    );
  }

  Widget buildActionCapabilityDropdown() {
    return ValueListenableBuilder(
      valueListenable: widget.actionCapabilityGroupNotifier,
      builder: (context, value, child) {
        if (value == null) {
          return Container();
        }
        return CustomDropdown<PatientCarerSkill>(
          choices: value,
          notifier: widget.actionCapabilityNotifier,
          textConverter: (item) => item.description,
          title: "Khả năng vặn động",
        );
      },
    );
  }

  Widget buildWeightDropdown() {
    return ValueListenableBuilder(
      valueListenable: widget.weightGroupNotifier,
      builder: (context, value, child) {
        if (value == null) {
          return Container();
        }
        return CustomDropdown<PatientCarerSkill>(
          choices: value,
          notifier: widget.weightNotifier,
          textConverter: (item) => item.description,
          title: "Cân nặng",
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget.formKey,
      child: ListView(
        padding: EdgeInsets.all(24),
        children: [
          TitleRow(title: "Kỹ năng yêu cầu"),
          SizedBox(height: 16),
          buildWeightDropdown(),
          SizedBox(height: 16),
          buildActionCapabilityDropdown(),
          SizedBox(height: 16),
          buidlCapabilitiesOption(),
          SizedBox(height: 16),
        ],
      ),
    );
  }
}
