import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wecare247/bloc/contract/contract_bloc.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/screens/create_edit_contract/utils/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wecare247/shared/index.dart';

class FirstStep extends StatefulWidget {
  final TextEditingController customerNameController;
  final TextEditingController customerPhoneController;
  final TextEditingController customerStreetController;
  final TextEditingController customerNoteController;
  final ValueNotifier<int> customerGenderNotifier;
  final ValueNotifier<Province> provinceNotifier;
  final ValueNotifier<District> districtNotifier;
  final ValueNotifier<Ward> wardNotifier;
  final ListNotifier provinceListNotifier;
  final ListNotifier districtListNotifier;
  final ListNotifier wardListNotifier;
  final TextEditingController patientFullNameController;
  final TextEditingController patientWeightController;
  final ValueNotifier<int> patientGenderNotifier;
  final ValueNotifier<int> patientYearOfBirthNotifier;
  final ValueNotifier<bool> isDefaultInfoNotifier;
  final Key formKey;
  FirstStep({
    Key key,
    this.customerNameController,
    this.customerPhoneController,
    this.formKey,
    this.provinceNotifier,
    this.districtNotifier,
    this.wardNotifier,
    this.provinceListNotifier,
    this.districtListNotifier,
    this.wardListNotifier,
    this.customerGenderNotifier,
    this.customerStreetController,
    this.customerNoteController,
    this.patientFullNameController,
    this.patientYearOfBirthNotifier,
    this.patientWeightController,
    this.patientGenderNotifier,
    this.isDefaultInfoNotifier,
  }) : super(key: key);

  @override
  _FirstStepState createState() => _FirstStepState();
}

class _FirstStepState extends State<FirstStep> {
  Widget buildInfoForm() {
    return BlocListener<ContractBloc, ContractState>(
        listener: (context, state) {
          if (state is TypeaheadCustomerGotten) {
            if (state.customer.fullName == null) {
              setState(() {
                widget.customerGenderNotifier.value = null;
                widget.provinceNotifier.value = null;
                widget.wardNotifier.value = null;
                widget.districtNotifier.value = null;
                widget.customerNameController.clear();
                widget.customerStreetController.clear();
                widget.customerNoteController.clear();
              });
            } else {
              setState(() {
                widget.customerGenderNotifier.value = state.customer.gender;
                widget.provinceNotifier.value = state.customer.province;
                widget.wardNotifier.value = state.customer.ward;
                widget.districtNotifier.value = state.customer.district;
                widget.customerNameController.text = state.customer.fullName;
                widget.customerStreetController.text = state.customer.street;
                widget.customerNoteController.text = state.customer.note;
              });
            }
          }
        },
        child: Column(
          children: [
            TitleRow(
              title: "Khách hàng",
            ),
            SizedBox(height: 8),
            CustomTextField(
              title: "Số điện thoại",
              controller: widget.customerPhoneController,
              keyboardType: TextInputType.phone,
              validator: (text) {
                String errorText = validateNonEmptyString(text);
                if (errorText != null) return errorText;
                return validateNumber(text);
              },
            ),
            SizedBox(height: 16),
            CustomTextField(
              title: "Họ và tên",
              controller: widget.customerNameController,
              keyboardType: TextInputType.name,
              validator: validateNonEmptyString,
            ),
            SizedBox(height: 16),
            TitleRow(
              title: "Địa chỉ",
            ),
            SizedBox(height: 8),
            ValueListenableBuilder(
              valueListenable: widget.provinceListNotifier,
              builder: (context, value, state) {
                Key key = ValueKey(value);
                return CustomDropdown<Province>(
                  key: key,
                  title: "Thành phố",
                  notifier: widget.provinceNotifier,
                  choices: value,
                  textConverter: (e) => e.name,
                  validator: (e) {
                    return e != null ? null : "";
                  },
                );
              },
            ),
            SizedBox(height: 8),
            ValueListenableBuilder(
              valueListenable: widget.districtListNotifier,
              builder: (context, value, state) {
                Key key = ValueKey(value);
                return CustomDropdown<District>(
                  key: key,
                  title: "Quận/Huyện",
                  notifier: widget.districtNotifier,
                  choices: value,
                  textConverter: (e) => e.name,
                  validator: (e) {
                    return e != null ? null : "";
                  },
                );
              },
            ),
            SizedBox(height: 8),
            ValueListenableBuilder(
              valueListenable: widget.wardListNotifier,
              builder: (context, value, state) {
                Key key = ValueKey(value);
                return CustomDropdown<Ward>(
                  key: key,
                  title: "Xã/Phường",
                  notifier: widget.wardNotifier,
                  choices: value,
                  textConverter: (e) => e.name,
                  validator: (e) {
                    return e != null ? null : "";
                  },
                );
              },
            ),
            SizedBox(height: 8),
            CustomTextField(
              title: "Số nhà và tên đường",
              controller: widget.customerStreetController,
              keyboardType: TextInputType.streetAddress,
              validator: validateNonEmptyString,
            ),
            SizedBox(height: 8),
            CustomDropdown<int>(
                title: "Giới tính",
                notifier: widget.customerGenderNotifier,
                validator: (value) => value != null ? null : "",
                choices: [0, 1, 2],
                textConverter: convertIntToGender),
            SizedBox(height: 16),
            CustomTextField(
              controller: widget.customerNoteController,
              title: "Ghi chú thêm",
            ),
            TitleRow(title: "Bệnh nhân"),
            SizedBox(height: 8),
            CustomTextField(
              controller: widget.patientFullNameController,
              title: "Họ tên",
              validator: validateNonEmptyString,
              keyboardType: TextInputType.name,
            ),
            SizedBox(height: 8),
            CustomDropdown<int>(
              choices: List<int>.generate(
                100,
                (index) => DateTime.now().year - index,
              ),
              notifier: widget.patientYearOfBirthNotifier,
              title: "Năm sinh",
              validator: (value) {
                return validateNonEmptyString(value?.toString());
              },
            ),
            SizedBox(height: 8),
            CustomTextField(
              controller: widget.patientWeightController,
              title: "Cân nặng",
              validator: validateNumber,
              keyboardType: TextInputType.number,
            ),
            SizedBox(height: 8),
            CustomDropdown<int>(
                title: "Giới tính",
                notifier: widget.patientGenderNotifier,
                validator: (value) => value != null ? null : "",
                choices: [0, 1, 2],
                textConverter: convertIntToGender),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget.formKey,
      child: ListView(
        padding: EdgeInsets.all(24),
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Chưa có thông tin",
                style: fontMedium.copyWith(fontSize: 14),
              ),
              ValueListenableBuilder(
                valueListenable: widget.isDefaultInfoNotifier,
                builder: (context, value, _) => Theme(
                  data: Theme.of(context)
                      .copyWith(unselectedWidgetColor: UIColors.mainColor()),
                  child: SizedBox(
                    width: 30,
                    height: 30,
                    child: Checkbox(
                      value: value,
                      onChanged: (value) {
                        widget.isDefaultInfoNotifier.value = value;
                      },
                      checkColor: Colors.white,
                      activeColor: UIColors.mainColor(),
                    ),
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 8),
          ValueListenableBuilder<bool>(
              valueListenable: widget.isDefaultInfoNotifier,
              builder: (context, value, child) {
                if (value) return Container();
                return buildInfoForm();
              }),
        ],
      ),
    );
  }
}
