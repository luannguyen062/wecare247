import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';

class CustomProgressBar extends StatefulWidget {
  final ValueNotifier<int> progressNotifier;
  final int max;
  CustomProgressBar({Key key, this.progressNotifier, this.max})
      : super(key: key);

  @override
  _CustomProgressBarState createState() => _CustomProgressBarState();
}

class _CustomProgressBarState extends State<CustomProgressBar>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> curve;
  Tween<double> valueTween;
  double value;
  double oldValue;
  double screenWidth;

  Widget buildBall(int index) {
    return Container(
      width: 40,
      height: 40,
      decoration: BoxDecoration(
          color: index <= widget.progressNotifier.value
              ? UIColors.mainColor()
              : Colors.grey[300],
          borderRadius: BorderRadius.circular(20)),
      child: Center(
        child: Text(
          (index + 1).toString(),
          style: fontMedium.copyWith(
              fontSize: 16,
              color: index <= widget.progressNotifier.value
                  ? Colors.white
                  : UIColors.mainColor()),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      duration: Duration(milliseconds: 500),
      vsync: this,
    );
    curve = CurvedAnimation(
      parent: controller,
      curve: Curves.easeInOut,
    );
    // Build the initial required tweens.
    valueTween = Tween<double>(
      begin: 0,
      end: widget.progressNotifier.value / (widget.max - 1),
    );
    controller.forward();
  }

  Widget buildPainter() {
    return ValueListenableBuilder(
        valueListenable: widget.progressNotifier,
        builder: (context, value, child) {
          double beginValue = oldValue ?? 0;
          double endValue = widget.progressNotifier.value / (widget.max - 1);
          if (endValue != oldValue) {
            // Update the value tween.
            valueTween = Tween<double>(
              begin: beginValue,
              end: endValue,
            );
            oldValue = endValue;
            controller
              ..value = 0
              ..forward();
          }
          return AnimatedBuilder(
            animation: curve,
            builder: (context, child) {
              return CustomPaint(
                child: Container(),
                foregroundPainter: LineProgressBarPainter(
                  backgroundColor: Colors.grey[300],
                  foregroundColor: UIColors.mainColor(),
                  value: valueTween.evaluate(curve),
                  strokeWidth: 6,
                ),
              );
            },
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    // Calculate distance between ball point.
    final distanceBetweenBalls =
        (screenWidth * 0.8 - 40 * widget.max) / (widget.max - 1);
    List<Widget> children = List<Widget>.from(
        List<int>.generate(widget.max, (index) => index)
            .map((index) => Positioned(
                  left: (40 + distanceBetweenBalls) * index,
                  child: buildBall(index),
                )));
    children.insert(0, buildPainter());
    return Center(
      child: Container(
        width: screenWidth * 0.8,
        height: 40,
        child: Stack(children: children),
      ),
    );
  }
}

class LineProgressBarPainter extends CustomPainter {
  final double strokeWidth;
  final double value;
  final Color backgroundColor;
  final Color foregroundColor;

  LineProgressBarPainter({
    this.backgroundColor,
    @required this.value,
    @required this.foregroundColor,
    double strokeWidth,
  }) : this.strokeWidth = strokeWidth ?? 6;

  @override
  void paint(Canvas canvas, Size size) {
    final Offset start = size.centerLeft(Offset.zero);
    final foregroundPaint = Paint()
      ..color = this.foregroundColor
      ..strokeWidth = this.strokeWidth
      ..style = PaintingStyle.stroke;
    final backgroundPaint = Paint()
      ..color = this.backgroundColor
      ..strokeWidth = this.strokeWidth
      ..style = PaintingStyle.stroke;
    canvas.drawLine(start, size.centerRight(Offset.zero), backgroundPaint);
    canvas.drawLine(
        start, start.translate(size.width * value, 0), foregroundPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    final oldPainter = (oldDelegate as LineProgressBarPainter);
    return oldPainter.value != value ||
        oldPainter.backgroundColor != backgroundColor ||
        oldPainter.foregroundColor != foregroundColor ||
        oldPainter.strokeWidth != strokeWidth;
  }
}
