import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:tiengviet/tiengviet.dart';

class CustomDropdown<T> extends StatefulWidget {
  final ValueNotifier<T> notifier;
  final List<T> choices;
  final String title;
  final String Function(T value) validator;
  final String Function(T value) textConverter;
  final bool Function(T value, String) searchFunction;
  CustomDropdown(
      {Key key,
      this.notifier,
      this.choices,
      this.title,
      this.validator,
      this.textConverter,
      this.searchFunction})
      : super(key: key);

  @override
  _CustomDropdownState<T> createState() => _CustomDropdownState<T>();
}

class _CustomDropdownState<T> extends State<CustomDropdown<T>> {
  @override
  Widget build(BuildContext context) {
    T currentValue;
    if (widget.choices != null &&
        widget.choices.contains(widget.notifier.value)) {
      currentValue = widget.notifier.value;
    } else {
      currentValue = null;
    }
    return ValueListenableBuilder(
      valueListenable: widget.notifier,
      builder: (context, value, child) {
        return child;
      },
      child: DropdownSearch<T>(
        validator: widget.validator,
        items: widget.choices,
        selectedItem: currentValue,
        mode: Mode.BOTTOM_SHEET,
        label: widget.title,
        filterFn: widget.searchFunction ??
            (item, string) {
              return string.isEmpty ||
                  TiengViet.parse(
                    widget.textConverter(item),
                  )
                      .toLowerCase()
                      .contains(TiengViet.parse(string).toLowerCase());
            },
        autoValidateMode: AutovalidateMode.disabled,
        itemAsString: widget.textConverter,
        showSearchBox: true,
        showSelectedItem: false,
        popupTitle: Container(
          padding: EdgeInsets.all(10),
          color: UIColors.mainColor(),
          alignment: Alignment.center,
          child: Text(
            widget.title,
            style: fontMedium.copyWith(fontSize: 12, color: Colors.white),
          ),
        ),
        onChanged: (newValue) => widget.notifier.value = newValue,
        searchBoxDecoration: InputDecoration(
          border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[600], width: 2),
              borderRadius: BorderRadius.circular(10)),
        ),
        dropdownSearchDecoration: InputDecoration(
          labelStyle: fontMedium.copyWith(color: Colors.black54),
          focusColor: UIColors.mainColor(),
          border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[600], width: 2),
              borderRadius: BorderRadius.circular(10)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: UIColors.mainColor(), width: 2),
              borderRadius: BorderRadius.circular(10)),
          errorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red, width: 2),
              borderRadius: BorderRadius.circular(10)),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[600], width: 2),
              borderRadius: BorderRadius.circular(10)),
          disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[600], width: 2),
              borderRadius: BorderRadius.circular(10)),
          contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
          isDense: true,
        ),
      ),
    );
  }
}
