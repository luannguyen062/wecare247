import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CustomDatePickerBox extends StatefulWidget {
  final ValueNotifier<DateTime> dateNotifier;
  final String title;
  final double width;
  final bool isDisabled;
  final bool withSpecifiedHour;
  CustomDatePickerBox(
      {Key key,
      this.dateNotifier,
      this.title,
      this.width,
      this.isDisabled = false,
      this.withSpecifiedHour = false})
      : super(key: key);

  @override
  _CustomDatePickerBoxState createState() => _CustomDatePickerBoxState();
}

class _CustomDatePickerBoxState extends State<CustomDatePickerBox> {
  void pickDate() async {
    if (widget.isDisabled) return;
    final currentDate = DateTime.now();
    DateTime date = await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: currentDate.subtract(Duration(days: 365 * 200)),
        lastDate: currentDate.add(Duration(days: 365 * 200)),
        builder: (context, child) {
          return Theme(
            child: child,
            data: Theme.of(context).copyWith(
                colorScheme: ColorScheme(
                    primary: UIColors.mainColor(),
                    primaryVariant: UIColors.mainColor(0.8),
                    secondary: UIColors.secondColor(),
                    secondaryVariant: UIColors.secondColor(0.8),
                    surface: UIColors.mainColor(),
                    background: Colors.white,
                    error: Colors.red[700],
                    onPrimary: Colors.white,
                    onSecondary: Colors.black,
                    onSurface: Colors.black,
                    onBackground: UIColors.mainColor(),
                    onError: Colors.white,
                    brightness: Brightness.light)),
          );
        });
    if (date != null) {
      if (!widget.withSpecifiedHour) {
        widget.dateNotifier.value = date;
        return;
      }
      final timeOfDay = await showTimePicker(
        context: context,
        initialTime: TimeOfDay(hour: 0, minute: 0),
        cancelText: "Hủy bỏ",
        confirmText: "Xác nhận",
      );
      if (timeOfDay != null) {
        date = date
            .add(Duration(hours: timeOfDay.hour, minutes: timeOfDay.minute));
        widget.dateNotifier.value = date;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width,
      child: TextButton(
          onPressed: pickDate,
          style: TextButton.styleFrom(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12),
                side: BorderSide(color: Colors.grey[600], width: 2)),
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          ),
          child: FittedBox(
            child: Row(
              children: [
                ValueListenableBuilder<DateTime>(
                  valueListenable: widget.dateNotifier,
                  builder: (context, value, child) {
                    String dateString;
                    if (value == null) {
                      dateString = widget.title;
                    } else {
                      dateString =
                          DateFormat("dd/MM/yyyy").add_Hm().format(value);
                    }
                    return Text(dateString ?? "", style: fontMedium);
                  },
                ),
                SizedBox(
                  width: 8,
                ),
                Icon(Icons.edit_rounded)
              ],
            ),
          )),
    );
  }
}
