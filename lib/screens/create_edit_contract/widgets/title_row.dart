import 'package:wecare247/shared/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TitleRow extends StatelessWidget {
  final String title;
  const TitleRow({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      child: Stack(
        children: [
          Center(
            child: Container(
              height: 4,
              width: double.infinity,
              color: UIColors.mainColor(),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                title ?? "",
                style: fontMedium.copyWith(color: Colors.black, fontSize: 14),
              ),
            ),
          )
        ],
      ),
    );
  }
}
