import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';

class CustomCheckboxRow<T> extends StatefulWidget {
  final T item;
  final List<T> itemList;
  final String Function(T value) itemAsString;
  CustomCheckboxRow({Key key, this.itemList, this.item, this.itemAsString})
      : super(key: key);

  @override
  _CustomCheckboxRowState<T> createState() => _CustomCheckboxRowState<T>();
}

class _CustomCheckboxRowState<T> extends State<CustomCheckboxRow<T>> {
  ValueNotifier<bool> notifier;
  @override
  void initState() {
    notifier = ValueNotifier<bool>(widget.itemList.contains(widget.item));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          widget.itemAsString(widget.item),
          style: fontMedium.copyWith(fontSize: 14),
        ),
        ValueListenableBuilder(
          valueListenable: notifier,
          builder: (context, value, _) => Theme(
            data: Theme.of(context)
                .copyWith(unselectedWidgetColor: UIColors.mainColor()),
            child: SizedBox(
              width: 30,
              height: 30,
              child: Checkbox(
                value: value,
                onChanged: (value) {
                  if (value) {
                    widget.itemList.add(widget.item);
                  } else {
                    widget.itemList.remove(widget.item);
                  }
                  notifier.value = value;
                },
                checkColor: Colors.white,
                activeColor: UIColors.mainColor(),
              ),
            ),
          ),
        )
      ],
    );
  }
}
