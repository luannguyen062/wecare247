import 'package:flutter/foundation.dart';

class ListNotifier<T> extends ValueListenable<List<T>> with ChangeNotifier {
  List<T> value;
  void setValue(List<T> newValue) {
    value = newValue;
    notifyListeners();
  }

  void add(T newItem) {
    value.add(newItem);
    notifyListeners();
  }

  void removeAt(int index) {
    value.removeAt(index);
    notifyListeners();
  }

  void remove(T item) {
    value.remove(item);
    notifyListeners();
  }
}
