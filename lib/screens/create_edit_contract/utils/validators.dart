String validateNonEmptyString(String text) {
  if (text == null || text.isEmpty) {
    return "Vui lòng không bỏ trống";
  }
  return null;
}

String validateNumber(String text) {
  if (text == null || double.tryParse(text) == null) {
    return "Không hợp lệ";
  }
  return null;
}
