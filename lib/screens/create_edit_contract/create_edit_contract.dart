import 'package:wecare247/bloc/common_state.dart';
import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/screens/create_edit_contract/utils/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CreateEditContractScreen extends StatefulWidget {
  CreateEditContractScreen({
    Key key,
  }) : super(key: key);

  @override
  _ContractScreenState createState() => _ContractScreenState();
}

class _ContractScreenState extends State<CreateEditContractScreen> {
  final stepController = PageController();
  final indexNotifier = ValueNotifier<int>(0);
  final dataBloc = DataBloc();
  final contractBloc = ContractBloc();
  List<GlobalKey<FormState>> formKeys;
  List<Widget> stepWidgets;
  // First Step
  TextEditingController contractNoteController;
  ListNotifier locationListNotifier;
  ValueNotifier<Location> locationNotifier;
  ListNotifier locationDepartmentListNotifier;
  ValueNotifier<LocationDepartment> locationDepartmentNotifier;
  ListNotifier contractSourceListNotifier;
  ValueNotifier<ContractSource> contractSourceNotifier;
  ValueNotifier<bool> isDefaultInfoNotifier;
  // Second Step
  TextEditingController customerNoteController;
  TextEditingController customerNameController;
  TextEditingController customerPhoneController;
  TextEditingController customerStreetController;
  ValueNotifier<int> customerGenderNotifier;
  TextEditingController patientFullNameController;
  TextEditingController patientWeightController;
  ValueNotifier<int> patientYearOfBirthNotifier;
  ValueNotifier<int> patientGenderNotifier;
  ListNotifier wardListNotifier;
  ListNotifier districtListNotifier;
  ListNotifier provinceListNotifier;
  ValueNotifier<Province> provinceNotifier;
  ValueNotifier<District> districtNotifier;
  ValueNotifier<Ward> wardNotifier;
  // Third Step
  ValueNotifier<PatientCarerSkill> weightNotifier;
  ValueNotifier<PatientCarerSkill> actionCapabilityNotifier;
  List<PatientCarerSkill> capabilities;
  // Patient Carer Skill Group Notifier
  ListNotifier weightGroupNotifier; // GRP01
  ListNotifier actionCapabilityGroupNotifer; // GRP02
  ListNotifier capabilityGroupNotifier; // GRP03
  // Fourth Step
  ListNotifier serviceListNotifier;
  ListNotifier<ServiceEditData> serviceCreationListNotifier;
  bool inTransition;

  bool validateServiceEditData(ServiceEditData service) {
    return service.from != null &&
        service.to != null &&
        !service.from.isAfter(service.to) &&
        service.serviceId != null;
  }

  bool validateFirstStep() {
    return formKeys[0].currentState.validate();
  }

  bool validateSecondStep() {
    if (!formKeys[1].currentState.validate()) {
      return false;
    }
    if (weightNotifier.value == null) {
      showErrorFlushbar("Cân nặng không thể bỏ trống.");
      return false;
    }
    if (actionCapabilityNotifier.value == null) {
      showErrorFlushbar("Khả năng vận động không thể bỏ trống.");
      return false;
    }
    return true;
  }

  bool validateThirdStep() {
    if (!formKeys[2].currentState.validate()) {
      return false;
    }
    if (serviceCreationListNotifier.value.isEmpty) {
      showErrorFlushbar("Vui lòng chọn ít nhất một dịch vụ.");
      return false;
    } else if (serviceCreationListNotifier.value
        .any((e) => !validateServiceEditData(e))) {
      showErrorFlushbar(
          "Vui lòng kiểm tra lại thông tin. Ngày bắt đầu dịch vụ phải sớm hơn ngày kết thúc.");
      return false;
    }
    return true;
  }

  bool validateFourthStep() {
    return formKeys[3].currentState.validate();
  }

  bool validateStep() {
    switch (indexNotifier.value) {
      case 0:
        return validateFirstStep();
      case 1:
        return validateSecondStep();
      case 2:
        return validateThirdStep();
      case 3:
        return validateFourthStep();
      default:
        return false;
    }
  }

  void createContract() async {
    final requiredSkills = [
      weightNotifier.value..value = true,
      actionCapabilityNotifier.value..value = true,
    ]..addAll(capabilities.map((e) => e..value = true));
    CreateContractDto dto;
    if (isDefaultInfoNotifier.value) {
      dto = CreateContractDto(
        note: contractNoteController.text,
        locationDepartmentId: locationDepartmentNotifier.value.id,
        contractSourceId: contractSourceNotifier.value.id,
        contractLocationId: locationNotifier.value.id,
        customer: defaultCustomer,
        patient: defaultPatient,
        requiredSkills: requiredSkills,
        useDefaultInformation: true,
        services: serviceCreationListNotifier.value,
      );
    } else {
      dto = CreateContractDto(
        note: contractNoteController.text,
        locationDepartmentId: locationDepartmentNotifier.value.id,
        contractSourceId: contractSourceNotifier.value.id,
        contractLocationId: locationNotifier.value.id,
        useDefaultInformation: false,
        customer: CreateCustomerDto(
            fullName: customerNameController.text,
            wardId: wardNotifier.value.id,
            contactPhone: customerPhoneController.text,
            districtId: districtNotifier.value.id,
            provinceId: provinceNotifier.value.id,
            street: customerStreetController.text,
            gender: customerGenderNotifier.value,
            note: customerNoteController.text),
        patient: CreatePatientDto(
            fullName: patientFullNameController.text,
            gender: patientGenderNotifier.value,
            yearOfBirth: patientYearOfBirthNotifier.value,
            weight: double.parse(patientWeightController.text)),
        services: serviceCreationListNotifier.value,
        requiredSkills: requiredSkills,
      );
    }
    contractBloc.createContract(dto: dto);
  }

  void initializeControllersAndNotifiers() {
    formKeys = List<GlobalKey<FormState>>.generate(
        4, (index) => GlobalKey<FormState>());
    // First Step
    contractNoteController = TextEditingController();
    locationNotifier = ValueNotifier<Location>(null);
    locationListNotifier = ListNotifier();
    locationDepartmentListNotifier = ListNotifier();
    locationDepartmentNotifier = ValueNotifier<LocationDepartment>(null);
    contractSourceListNotifier = ListNotifier();
    contractSourceNotifier = ValueNotifier<ContractSource>(null);
    isDefaultInfoNotifier = ValueNotifier<bool>(false);
    // Second Step
    customerNameController = TextEditingController();
    customerPhoneController = TextEditingController()
      ..addListener(() {
        if (customerPhoneController.text.length < 7) {
          return;
        }
        contractBloc.getTypeaheadCustomer(
            contactPhone: customerPhoneController.text);
      });
    customerStreetController = TextEditingController();
    customerGenderNotifier = ValueNotifier<int>(null);
    customerNoteController = TextEditingController();
    patientFullNameController = TextEditingController();
    patientGenderNotifier = ValueNotifier<int>(null);
    patientWeightController = TextEditingController();
    patientYearOfBirthNotifier = ValueNotifier<int>(null);
    provinceNotifier = ValueNotifier<Province>(null);
    districtNotifier = ValueNotifier<District>(null);
    wardNotifier = ValueNotifier<Ward>(null);
    provinceListNotifier = ListNotifier();
    districtListNotifier = ListNotifier();
    wardListNotifier = ListNotifier();
    // Third Step
    weightNotifier = ValueNotifier<PatientCarerSkill>(null);
    actionCapabilityNotifier = ValueNotifier<PatientCarerSkill>(null);
    capabilities = [];
    actionCapabilityGroupNotifer = ListNotifier();
    capabilityGroupNotifier = ListNotifier();
    weightGroupNotifier = ListNotifier();
    // Fourth Step
    serviceListNotifier = ListNotifier();
    serviceCreationListNotifier = ListNotifier<ServiceEditData>();
    serviceCreationListNotifier.setValue([ServiceEditData()]);
  }

  void initializeStepPage() {
    stepWidgets = [
      FirstStep(
        customerStreetController: customerStreetController,
        customerNameController: customerNameController,
        customerPhoneController: customerPhoneController,
        customerGenderNotifier: customerGenderNotifier,
        patientFullNameController: patientFullNameController,
        patientGenderNotifier: patientGenderNotifier,
        patientWeightController: patientWeightController,
        patientYearOfBirthNotifier: patientYearOfBirthNotifier,
        provinceNotifier: provinceNotifier,
        districtNotifier: districtNotifier,
        wardNotifier: wardNotifier,
        wardListNotifier: wardListNotifier,
        districtListNotifier: districtListNotifier,
        provinceListNotifier: provinceListNotifier,
        customerNoteController: customerNoteController,
        isDefaultInfoNotifier: isDefaultInfoNotifier,
        formKey: formKeys[0],
      ),
      SecondStep(
        weightGroupNotifier: weightGroupNotifier,
        weightNotifier: weightNotifier,
        actionCapabilityNotifier: actionCapabilityNotifier,
        actionCapabilityGroupNotifier: actionCapabilityGroupNotifer,
        capabilities: capabilities,
        capabilityGroupNotifier: capabilityGroupNotifier,
        formKey: formKeys[1],
      ),
      ThirdStep(
        serviceListNotifier: serviceListNotifier,
        serviceCreationListNotifier: serviceCreationListNotifier,
        formKey: formKeys[2],
      ),
      FourthStep(
        contractNoteController: contractNoteController,
        locationNotifier: locationNotifier,
        locationListNotifier: locationListNotifier,
        locationDepartmentNotifier: locationDepartmentNotifier,
        locationDepartmentListNotifier: locationDepartmentListNotifier,
        contractSourceListNotifier: contractSourceListNotifier,
        contractSourceNotifier: contractSourceNotifier,
        formKey: formKeys[3],
      ),
    ];
  }

  Future<void> goToPreviousStep() async {
    if (inTransition) {
      return;
    }
    if (indexNotifier.value > 0) {
      setState(() {
        indexNotifier.value -= 1;
      });
      inTransition = true;
      await stepController.previousPage(
          duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
      inTransition = false;
    }
  }

  Future<void> goToNextStep(DateTime today) async {
    if (!validateStep()) return;
    if (indexNotifier.value == 3) {
      createContract();
    } else {
      if (inTransition) {
        return;
      }
      setState(() {
        indexNotifier.value += 1;
      });
      inTransition = true;
      await stepController.nextPage(
          duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
      inTransition = false;
    }
  }

  void splitSkillsIntoGroups(List<PatientCarerSkill> skills) {
    List<PatientCarerSkill> weightGroup = [];
    List<PatientCarerSkill> actionCapabilityGroup = [];
    List<PatientCarerSkill> capabilityGroup = [];
    for (PatientCarerSkill skill in skills) {
      switch (skill.groupId) {
        case "GRP01":
          weightGroup.add(skill);
          break;
        case "GRP02":
          actionCapabilityGroup.add(skill);
          break;
        case "GRP03":
          capabilityGroup.add(skill);
          break;
      }
    }
    weightGroupNotifier.setValue(weightGroup);
    actionCapabilityGroupNotifer.setValue(actionCapabilityGroup);
    capabilityGroupNotifier.setValue(capabilityGroup);
  }

  void listenToContractBloc(BuildContext context, state) async {
    if (state is CreatingContract) {
      showLoadingDialog(context);
    } else if (state is ContractCreated) {
      Navigator.of(context).popUntil(ModalRoute.withName("home"));
      showErrorFlushbar("Hợp dồng tạo thành công.");
    } else if (state is ErrorCreatingContract) {
      Navigator.of(context)
          .popUntil(ModalRoute.withName("create_edit_contract"));
      showErrorFlushbar(state.message);
    } else if (state is TokenExpireForContract) {
      BlocProvider.of<AuthenticationBloc>(context).logOut();
    }
  }

  void listenToDataBloc(BuildContext context, state) {
    if (state is ErrorState) {
      showErrorFlushbar(
          state.message + "\nVui lòng quay lại trang trước và thử lại.");
    }
    if (state is PatientCarerSkillGotten) {
      splitSkillsIntoGroups(state.skills);
    } else if (state is ProvinceListGotten) {
      provinceListNotifier.setValue(state.provinceList);
    } else if (state is DistrictListGotten) {
      districtListNotifier.setValue(state.districtList);
    } else if (state is WardListGotten) {
      wardListNotifier.setValue(state.wardList);
    } else if (state is LocationListGotten) {
      locationListNotifier.setValue(state.locationList);
    } else if (state is ServiceListGotten) {
      serviceListNotifier.setValue(state.serviceList);
    } else if (state is LocationDepartmentListGotten) {
      locationDepartmentListNotifier.setValue(state.departmentList);
    } else if (state is ContractSourceListGotten) {
      contractSourceListNotifier.setValue(state.contractSourceList);
    } else if (state is TokenExpireForData) {
      BlocProvider.of<AuthenticationBloc>(context).logOut();
    }
  }

  void getDistrictList() {
    if (provinceNotifier.value != null) {
      dataBloc.getDistrictList(provinceId: provinceNotifier.value.id);
    }
  }

  void getWardList() {
    if (districtNotifier.value != null) {
      dataBloc.getWardList(districtId: districtNotifier.value.id);
    }
  }

  void updateServiceLocation() {
    serviceCreationListNotifier.value
        .forEach((e) => e.locationId = locationNotifier.value.id);
  }

  void getLocationDepartmentList() {
    locationDepartmentListNotifier.value = [];
    if (locationNotifier.value != null) {
      dataBloc.getLocationDepartmentList(locationId: locationNotifier.value.id);
    }
  }

  @override
  void initState() {
    super.initState();
    initializeControllersAndNotifiers();
    initializeStepPage();
    dataBloc.getPatientCarerSkill();
    dataBloc.getProvinceList();
    if (provinceNotifier.value != null) {
      dataBloc.getDistrictList(provinceId: districtNotifier.value.id);
    }
    if (districtNotifier.value != null) {
      dataBloc.getWardList(districtId: districtNotifier.value.id);
    }
    dataBloc.getLocationList();
    dataBloc.getServiceList();
    dataBloc.getContractSourceList();
    provinceNotifier.addListener(getDistrictList);
    districtNotifier.addListener(getWardList);
    locationNotifier.addListener(updateServiceLocation);
    locationNotifier.addListener(getLocationDepartmentList);
    inTransition = false;
  }

  Widget buildBottomAppbar(DateTime today) {
    return BottomAppBar(
      elevation: 0,
      color: Colors.transparent,
      child: Container(
          margin: EdgeInsets.symmetric(
              horizontal: (MediaQuery.of(context).size.width - 302) / 2,
              vertical: 12),
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            border: Border.all(width: 1, color: Colors.grey.withOpacity(0.6)),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: goToPreviousStep,
                child: Container(
                  width: 150,
                  child: Text(
                    "Quay lại",
                    textAlign: TextAlign.center,
                    style: fontMedium.copyWith(color: UIColors.mainColor()),
                  ),
                ),
              ),
              Container(
                width: 150,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0)),
                    primary: Theme.of(context).accentColor,
                  ),
                  onPressed: () async => await goToNextStep(today),
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
                    child: Text(
                      indexNotifier.value != 3 ? 'Tiếp theo' : "Xác nhận",
                      style: fontMedium.copyWith(
                        fontSize: 12.0,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                ),
              )
            ],
          )),
    );
  }

  Widget buildTopAppBar() {
    return AppBar(
      title: Text(
        "THÊM MỚI HỢP ĐỒNG",
        style: fontMedium.copyWith(color: Colors.white, fontSize: 16),
      ),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () => Navigator.of(context).pop(),
      ),
      backgroundColor: UIColors.mainColor(),
      centerTitle: true,
      elevation: 0,
    );
  }

  @override
  Widget build(BuildContext context) {
    final today = DateTime.now();
    return BlocProvider<ContractBloc>.value(
      value: contractBloc,
      child: MultiBlocListener(
        listeners: [
          BlocListener<DataBloc, DataState>(
            listener: listenToDataBloc,
            bloc: dataBloc,
          ),
          BlocListener<ContractBloc, ContractState>(
            listener: listenToContractBloc,
            bloc: contractBloc,
          ),
        ],
        child: Scaffold(
          appBar: buildTopAppBar(),
          body: SafeArea(
            child: ListView(
              children: [
                Container(
                  height: 20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(25.0),
                        bottomRight: Radius.circular(25.0)),
                    color: Theme.of(context).accentColor,
                  ),
                ),
                SizedBox(height: 16),
                CustomProgressBar(
                  progressNotifier: indexNotifier,
                  max: 4,
                ),
                Container(
                  height: MediaQuery.of(context).size.height -
                      80 -
                      80 -
                      40 -
                      MediaQuery.of(context).padding.top,
                  child: PageView(
                    children: stepWidgets,
                    controller: stepController,
                    physics: NeverScrollableScrollPhysics(),
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: buildBottomAppbar(today),
        ),
      ),
    );
  }

  @override
  void dispose() {
    contractNoteController.dispose();
    locationListNotifier.dispose();
    locationNotifier.dispose();
    locationDepartmentListNotifier.dispose();
    locationDepartmentNotifier.dispose();
    contractSourceListNotifier.dispose();
    contractSourceNotifier.dispose();
    isDefaultInfoNotifier.dispose();
    // Second Step
    customerNoteController.dispose();
    customerNameController.dispose();
    customerPhoneController.dispose();
    customerStreetController.dispose();
    customerGenderNotifier.dispose();
    patientFullNameController.dispose();
    patientWeightController.dispose();
    patientYearOfBirthNotifier.dispose();
    patientGenderNotifier.dispose();
    wardListNotifier.dispose();
    districtListNotifier.dispose();
    provinceListNotifier.dispose();
    provinceNotifier.dispose();
    districtNotifier.dispose();
    wardNotifier.dispose();
    // Third Step
    weightNotifier.dispose();
    actionCapabilityNotifier.dispose();
    // Patient Carer Skill Group Notifier
    weightGroupNotifier.dispose();
    actionCapabilityGroupNotifer.dispose();
    capabilityGroupNotifier.dispose();
    // Fourth Step
    serviceListNotifier.dispose();
    serviceCreationListNotifier.dispose();
    super.dispose();
  }
}
