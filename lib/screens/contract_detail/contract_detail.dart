import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/contract.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/screens/contract_detail/widgets/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ContractDetailScreen extends StatefulWidget {
  final String id;
  ContractDetailScreen({Key key, this.id}) : super(key: key);

  @override
  _ContractDetailScreenState createState() => _ContractDetailScreenState();
}

class _ContractDetailScreenState extends State<ContractDetailScreen>
    with TickerProviderStateMixin {
  TabController tabController;
  ContractBloc contractBloc;
  DataBloc dataBloc;
  ContractDetail previousContract;
  ContractDetail currentContract;

  void listenToContractBloc(BuildContext context, state) {
    if (state is ErrorState) {
      showErrorFlushbar(state.message, true);
    }
    if (state is SpecificContractGotten) {
      previousContract = currentContract;
      currentContract = state.contract;
      if (previousContract != null &&
          previousContract.contractLocationId !=
              currentContract.contractLocationId) {
        BlocProvider.of<DataBloc>(context).getLocationDepartmentList(
            locationId: currentContract.contractLocationId);
      }
    } else if (state is ServiceEdited) {
      Navigator.of(context).pop();
      showErrorFlushbar("Cập nhật thông tin dịch vụ thành công.");
      contractBloc.getContractSpecific(id: widget.id);
    } else if (state is ContractEdited) {
      Navigator.of(context).pop();
      showErrorFlushbar("Cập nhật thông tin hợp đồng thành công.");
      contractBloc.getContractSpecific(id: widget.id);
    } else if (state is NewServiceCreated) {
      Navigator.of(context).pop();
      showErrorFlushbar("Tạo dịch vụ thành công.");
      contractBloc.getContractSpecific(id: widget.id);
    } else if (state is CustomerInfoEdited) {
      Navigator.of(context).pop();
      showErrorFlushbar("Cập nhật thông tin khách hàng/bệnh nhân thành công.");
      contractBloc.getContractSpecific(id: widget.id);
    } else if (state is EditingContract ||
        state is EditingCustomerInfo ||
        state is EditingService ||
        state is CreatingNewService) {
      showLoadingDialog(context);
    } else if (state is ErrorEditingContract || state is ErrorEditingService) {
      Navigator.of(context).pop();
    } else if (state is TokenExpireForContract) {
      BlocProvider.of<AuthenticationBloc>(context).logOut();
    }
  }

  void listenToDataBloc(BuildContext context, state) async {
    if (state is GettingLocationList ||
        state is GettingLocationDepartmentList ||
        state is GettingContractSourceList) {
      showLoadingDialog(context);
    } else if (state is LocationListGotten) {
      Navigator.of(context).pop();
      final value = await showDialog<Location>(
          context: context,
          builder: (context) => MultiChoicesScreen<Location>(
                choices: state.locationList,
                textConverter: (e) => e.name,
                title: "Thay đổi nơi nhận hợp đồng",
                currentChoice: state.locationList.firstWhere(
                    (e) => e.id == currentContract.contractLocationId,
                    orElse: () => null),
              ));
      if (value != null) {
        contractBloc.editContract(
            dto: EditContractDto(
                contractLocationId: value.id,
                id: currentContract.id,
                contractSourceId: currentContract.contractSourceId,
                locationDepartmentId: currentContract.locationDepartmentId,
                requiredSkills: currentContract.requiredSkills));
      }
    } else if (state is ContractSourceListGotten) {
      Navigator.of(context).pop();
      final value = await showDialog<ContractSource>(
          context: context,
          builder: (context) => MultiChoicesScreen<ContractSource>(
                choices: state.contractSourceList,
                textConverter: (e) => e.name,
                title: "Thay đổi nguồn hợp đồng",
                currentChoice: state.contractSourceList.firstWhere(
                    (e) => e.id == currentContract.contractSourceId,
                    orElse: () => null),
              ));
      if (value != null) {
        contractBloc.editContract(
            dto: EditContractDto(
                contractLocationId: currentContract.contractLocationId,
                id: currentContract.id,
                contractSourceId: value.id,
                locationDepartmentId: currentContract.locationDepartmentId,
                requiredSkills: currentContract.requiredSkills));
      }
    } else if (state is LocationDepartmentListGotten) {
      Navigator.of(context).pop();
      final value = await showDialog<LocationDepartment>(
          context: context,
          builder: (context) => MultiChoicesScreen<LocationDepartment>(
                choices: state.departmentList,
                textConverter: (e) => e.name,
                title: "Thay đổi khoa bệnh",
                currentChoice: state.departmentList.firstWhere(
                    (e) => e.id == currentContract.locationDepartmentId,
                    orElse: () => null),
              ));
      if (value != null) {
        contractBloc.editContract(
            dto: EditContractDto(
                contractLocationId: currentContract.contractLocationId,
                id: currentContract.id,
                contractSourceId: currentContract.contractSourceId,
                locationDepartmentId: value.id,
                requiredSkills: currentContract.requiredSkills));
      }
    } else if (state is TokenExpireForData) {
      BlocProvider.of<AuthenticationBloc>(context).logOut();
    }
  }

  @override
  void initState() {
    super.initState();
    tabController = TabController(vsync: this, length: 5, initialIndex: 0);
    contractBloc = ContractBloc();
    dataBloc = DataBloc();
    contractBloc.getContractSpecific(id: widget.id);
  }

  Widget buildTopAppBar() {
    return AppBar(
      title: Text(
        "Hợp đồng mã ${widget.id}",
        style: fontMedium.copyWith(color: Colors.white, fontSize: 14),
      ),
      backgroundColor: UIColors.mainColor(),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () => Navigator.of(context).pop(),
      ),
      centerTitle: true,
    );
  }

  Widget buildBody(ContractDetail contract) {
    switch (tabController.index) {
      case 0:
        return InfoTab(contract: contract);
      case 1:
        return CustomerTab(contract: contract);
      case 2:
        return PatientTab(contract: contract);
      case 3:
        return SkillRequireTab(contract: contract);
      case 4:
        return ServiceTab(contract: contract);
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ContractBloc>.value(
          value: contractBloc,
        ),
        BlocProvider<DataBloc>.value(
          value: dataBloc,
        )
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<ContractBloc, ContractState>(
            listener: listenToContractBloc,
          ),
          BlocListener<DataBloc, DataState>(
            listener: listenToDataBloc,
          ),
        ],
        child: Scaffold(
          appBar: buildTopAppBar(),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Material(
                color: UIColors.mainColor(0.7),
                child: Padding(
                  padding: EdgeInsets.only(top: 8, left: 8, right: 8),
                  child: TabBar(
                    tabs: [
                      Tab(text: "Thông tin"),
                      Tab(text: "Khách hàng"),
                      Tab(text: "Bệnh nhân"),
                      Tab(text: "Yêu cầu kĩ năng"),
                      Tab(text: "Dịch vụ"),
                    ],
                    controller: tabController,
                    isScrollable: true,
                    indicator: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(10))),
                    labelColor: UIColors.mainColor(),
                    unselectedLabelColor: Colors.white,
                    labelStyle: fontMedium.copyWith(fontSize: 18),
                    unselectedLabelStyle: fontBook.copyWith(fontSize: 14),
                    onTap: (index) => setState(() {}),
                  ),
                ),
              ),
              Expanded(
                child: ListView(
                  children: [
                    BlocBuilder<ContractBloc, ContractState>(
                      buildWhen: (prevState, curState) =>
                          curState is SpecificContractGotten ||
                          curState is GettingContract,
                      builder: (context, state) {
                        if (state is SpecificContractGotten) {
                          return buildBody(state.contract);
                        }
                        return Container(
                          height: 300,
                          child: Center(
                            child: CircularProgressIndicator(
                              valueColor:
                                  AlwaysStoppedAnimation(UIColors.mainColor()),
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    dataBloc.close();
    contractBloc.close();
    super.dispose();
  }
}
