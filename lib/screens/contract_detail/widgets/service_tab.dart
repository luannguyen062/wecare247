import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wecare247/bloc/contract/contract_bloc.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/screens/contract_detail/widgets/add_new_service.dart';
import 'package:wecare247/screens/contract_detail/widgets/index.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:wecare247/shared/index.dart';

class ServiceTab extends StatefulWidget {
  final ContractDetail contract;
  ServiceTab({Key key, this.contract}) : super(key: key);

  @override
  _ServiceTabState createState() => _ServiceTabState();
}

class _ServiceTabState extends State<ServiceTab> {
  void createNewService() async {
    final data = await showDialog<ServiceEditData>(
        context: context,
        builder: (context) => AddNewServiceScreen(
              contract: widget.contract,
            ));
    if (data != null) {
      BlocProvider.of<ContractBloc>(context).createNewService(data: data);
    }
  }

  @override
  Widget build(BuildContext context) {
    List<ServiceData> sortedService = widget.contract.services.sublist(0)
      ..sort((serviceA, serviceB) {
        if (serviceA.created.isAfter(serviceB.created)) {
          return 1;
        } else {
          return -1;
        }
      });
    List<Widget> children = List<Widget>.from(sortedService.map((e) =>
        ServiceExpandableDisplay(service: e, contract: widget.contract)));
    children.insertAll(0, [
      Center(
        child: UIButton(
          label: "Thêm dịch vụ mới",
          width: 200,
          labelStyle: fontMedium.copyWith(color: UIColors.mainColor()),
          borderColor: UIColors.mainColor(),
          borderRadius: BorderRadius.circular(20),
          backgroundColor: Colors.transparent,
          onPressCallback: createNewService,
        ),
      ),
      SizedBox(height: 12),
    ]);
    return Padding(
      padding: EdgeInsets.all(24),
      child: ExpandableTheme(
        data: ExpandableThemeData(
            headerAlignment: ExpandablePanelHeaderAlignment.center),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: children,
        ),
      ),
    );
  }
}
