import 'package:wecare247/bloc/data/data_bloc.dart';
import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/screens/create_edit_contract/utils/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/index.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EditCustomerInfoScreen extends StatefulWidget {
  final CustomerDetail customerInfo;
  EditCustomerInfoScreen({
    Key key,
    this.customerInfo,
  }) : super(key: key);

  @override
  _EditCustomerInfoScreenState createState() => _EditCustomerInfoScreenState();
}

class _EditCustomerInfoScreenState extends State<EditCustomerInfoScreen> {
  DataBloc dataBloc;
  GlobalKey<FormState> formKey;
  TextEditingController customerNameController;
  TextEditingController customerPhoneController;
  TextEditingController customerStreetController;
  TextEditingController customerNoteController;
  ValueNotifier<int> customerGenderNotifier;
  ListNotifier wardListNotifier;
  ListNotifier districtListNotifier;
  ListNotifier provinceListNotifier;
  ValueNotifier<Province> provinceNotifier;
  ValueNotifier<District> districtNotifier;
  ValueNotifier<Ward> wardNotifier;

  Widget buildTopAppBar() {
    return AppBar(
      title: Text(
        "Chỉnh sửa thông tin khách hàng",
        style: fontMedium.copyWith(color: Colors.white, fontSize: 14),
      ),
      backgroundColor: UIColors.mainColor(),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () => Navigator.of(context).pop(),
      ),
      centerTitle: true,
      actions: [
        IconButton(
          icon: Icon(Icons.check),
          color: Colors.white,
          onPressed: () {
            if (formKey.currentState.validate()) {
              Navigator.of(context).pop(EditCustomerInfoDto(
                id: widget.customerInfo.id,
                contactPhone: customerPhoneController.text,
                wardId: wardNotifier.value.id,
                districtId: districtNotifier.value.id,
                provinceId: provinceNotifier.value.id,
                gender: customerGenderNotifier.value,
                street: customerStreetController.text,
                note: customerNoteController.text,
                fullName: customerNameController.text,
              ));
            }
          },
        )
      ],
    );
  }

  Widget buildBody() {
    return Form(
      key: formKey,
      child: ListView(
        padding: EdgeInsets.all(24),
        children: [
          Column(
            children: [
              TitleRow(
                title: "Khách hàng",
              ),
              SizedBox(height: 8),
              CustomTextField(
                title: "Họ và tên",
                controller: customerNameController,
                keyboardType: TextInputType.name,
                validator: validateNonEmptyString,
              ),
              SizedBox(height: 16),
              CustomTextField(
                title: "Số điện thoại",
                controller: customerPhoneController,
                keyboardType: TextInputType.phone,
                validator: (text) {
                  String errorText = validateNonEmptyString(text);
                  if (errorText != null) return errorText;
                  return validateNumber(text);
                },
              ),
              SizedBox(height: 16),
              TitleRow(
                title: "Địa chỉ",
              ),
              SizedBox(height: 8),
              ValueListenableBuilder(
                valueListenable: provinceListNotifier,
                builder: (context, value, state) {
                  Key key = ValueKey(value);
                  return CustomDropdown<Province>(
                    key: key,
                    title: "Thành phố",
                    notifier: provinceNotifier,
                    choices: value,
                    textConverter: (e) => e.name,
                    validator: (e) {
                      return e != null ? null : "";
                    },
                  );
                },
              ),
              SizedBox(height: 8),
              ValueListenableBuilder(
                valueListenable: districtListNotifier,
                builder: (context, value, state) {
                  Key key = ValueKey(value);
                  return CustomDropdown<District>(
                    key: key,
                    title: "Quận/Huyện",
                    notifier: districtNotifier,
                    choices: value,
                    textConverter: (e) => e.name,
                    validator: (e) {
                      return e != null ? null : "";
                    },
                  );
                },
              ),
              SizedBox(height: 8),
              ValueListenableBuilder(
                valueListenable: wardListNotifier,
                builder: (context, value, state) {
                  Key key = ValueKey(value);
                  return CustomDropdown<Ward>(
                    key: key,
                    title: "Xã/Phường",
                    notifier: wardNotifier,
                    choices: value,
                    textConverter: (e) => e.name,
                    validator: (e) {
                      return e != null ? null : "";
                    },
                  );
                },
              ),
              SizedBox(height: 8),
              CustomTextField(
                title: "Số nhà và tên đường",
                controller: customerStreetController,
                keyboardType: TextInputType.streetAddress,
                validator: validateNonEmptyString,
              ),
              SizedBox(height: 8),
              CustomDropdown<int>(
                  title: "Giới tính",
                  notifier: customerGenderNotifier,
                  validator: (value) => value != null ? null : "",
                  choices: [0, 1, 2],
                  textConverter: convertIntToGender),
              SizedBox(height: 16),
              CustomTextField(
                controller: customerNoteController,
                title: "Ghi chú thêm",
                textInputAction: TextInputAction.done,
              ),
            ],
          )
        ],
      ),
    );
  }

  void getDistrictList() {
    districtNotifier.value = null;
    wardNotifier.value = null;
    if (provinceNotifier.value != null) {
      dataBloc.getDistrictList(provinceId: provinceNotifier.value.id);
    }
  }

  void getWardList() {
    wardNotifier.value = null;
    if (districtNotifier.value != null) {
      dataBloc.getWardList(districtId: districtNotifier.value.id);
    }
  }

  void listenToDataBloc(BuildContext context, state) {
    if (state is TokenExpireForData) {
      BlocProvider.of<AuthenticationBloc>(context).logOut();
    } else if (state is ProvinceListGotten) {
      provinceListNotifier.setValue(state.provinceList);
    } else if (state is DistrictListGotten) {
      districtListNotifier.setValue(state.districtList);
    } else if (state is WardListGotten) {
      wardListNotifier.setValue(state.wardList);
    }
  }

  @override
  void initState() {
    super.initState();
    formKey = GlobalKey<FormState>();
    dataBloc = DataBloc();
    dataBloc.getProvinceList();
    customerNameController =
        TextEditingController(text: widget.customerInfo.fullName);
    customerPhoneController =
        TextEditingController(text: widget.customerInfo.contactPhone);
    customerStreetController =
        TextEditingController(text: widget.customerInfo.street);
    customerGenderNotifier = ValueNotifier<int>(widget.customerInfo.gender);
    customerNoteController =
        TextEditingController(text: widget.customerInfo.note);
    provinceNotifier = ValueNotifier<Province>(widget.customerInfo.province);
    districtNotifier = ValueNotifier<District>(widget.customerInfo.district);
    wardNotifier = ValueNotifier<Ward>(widget.customerInfo.ward);
    provinceListNotifier = ListNotifier();
    districtListNotifier = ListNotifier();
    wardListNotifier = ListNotifier();
    provinceListNotifier.setValue(<Province>[]);
    districtListNotifier.setValue(<District>[]);
    wardListNotifier.setValue(<Ward>[]);
    if (widget.customerInfo.province != null) {
      dataBloc.getDistrictList(provinceId: widget.customerInfo.province.id);
    }
    if (widget.customerInfo.district != null) {
      dataBloc.getWardList(districtId: widget.customerInfo.district.id);
    }
    if (widget.customerInfo.ward != null) {
      wardListNotifier.add(widget.customerInfo.ward);
    }
    provinceNotifier.addListener(getDistrictList);
    districtNotifier.addListener(getWardList);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildTopAppBar(),
      body: BlocListener<DataBloc, DataState>(
          bloc: dataBloc, listener: listenToDataBloc, child: buildBody()),
    );
  }

  @override
  void dispose() {
    customerNoteController.dispose();
    customerNameController.dispose();
    customerPhoneController.dispose();
    customerStreetController.dispose();
    customerGenderNotifier.dispose();
    wardListNotifier.dispose();
    districtListNotifier.dispose();
    provinceListNotifier.dispose();
    provinceNotifier.dispose();
    districtNotifier.dispose();
    wardNotifier.dispose();
    super.dispose();
  }
}
