import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/screens/contract_detail/widgets/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SkillRequireTab extends StatefulWidget {
  final ContractDetail contract;
  SkillRequireTab({Key key, this.contract}) : super(key: key);

  @override
  _SkillRequireTabState createState() => _SkillRequireTabState();
}

class _SkillRequireTabState extends State<SkillRequireTab> {
  void editRequirementSkill() async {
    final newSkillList = await showDialog<List<PatientCarerSkill>>(
        context: context,
        builder: (context) => EditSkillRequirementScreen(
              previousSkillList: widget.contract.requiredSkills,
            ));
    if (newSkillList != null) {
      BlocProvider.of<ContractBloc>(context).editContract(
          dto: EditContractDto(
              contractLocationId: widget.contract.contractLocationId,
              requiredSkills: newSkillList,
              contractSourceId: widget.contract.contractSourceId,
              locationDepartmentId: widget.contract.locationDepartmentId,
              id: widget.contract.id));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TitleRow(
            title: "Yêu cầu kĩ năng cho NNB",
          ),
          Divider(),
          TextRow(
              title: "Cân nặng:",
              value: widget.contract.requiredSkills
                      .firstWhere((e) => e.skillId.endsWith("KG"),
                          orElse: () => null)
                      ?.skillName ??
                  "Chưa có"),
          Divider(),
          TextCol(
            title: "Khả năng vận động:",
            value: widget.contract.requiredSkills
                    .firstWhere((e) => e.skillId.startsWith("LV"),
                        orElse: () => null)
                    ?.skillName ??
                "Chưa có",
          ),
          Divider(),
          TextCol(
            title: "Các yêu cầu khác:",
            value: widget.contract.requiredSkills
                .where((e) =>
                    !e.skillId.endsWith("KG") && !e.skillId.startsWith("LV"))
                .map((e) => e.skillName)
                .join(", "),
          ),
          Divider(),
          Center(
            child: UIButton(
              onPressCallback: editRequirementSkill,
              label: "Thay đổi yêu cầu",
              width: 200,
              borderRadius: BorderRadius.circular(20),
              borderColor: UIColors.mainColor(),
              labelStyle: fontMedium.copyWith(color: UIColors.mainColor()),
            ),
          ),
        ],
      ),
    );
  }
}
