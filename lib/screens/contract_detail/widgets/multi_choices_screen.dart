import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';

class MultiChoicesScreen<T> extends StatefulWidget {
  final T currentChoice;
  final List<T> choices;
  final String Function(T value) textConverter;
  final String title;
  MultiChoicesScreen(
      {Key key,
      this.choices,
      this.textConverter,
      this.title,
      this.currentChoice})
      : super(key: key);

  @override
  _MultiChoicesScreenState<T> createState() => _MultiChoicesScreenState<T>();
}

class _MultiChoicesScreenState<T> extends State<MultiChoicesScreen<T>> {
  T currentValue;
  Widget buildTopAppBar() {
    return AppBar(
      title: Text(
        widget.title,
        style: fontMedium.copyWith(color: Colors.white, fontSize: 14),
      ),
      backgroundColor: UIColors.mainColor(),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () => Navigator.of(context).pop(),
      ),
      centerTitle: true,
      actions: [
        IconButton(
          icon: Icon(Icons.check),
          color: Colors.white,
          onPressed: () => Navigator.of(context).pop(currentValue),
        )
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    currentValue = widget.currentChoice;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildTopAppBar(),
      body: ListView(
        padding: EdgeInsets.all(12),
        children: List<Widget>.from(widget.choices.map((choice) {
          return Container(
            padding: EdgeInsets.all(8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Radio(
                  value: choice,
                  groupValue: currentValue,
                  onChanged: (newValue) => setState(() {
                    currentValue = newValue;
                  }),
                ),
                SizedBox(width: 8),
                Expanded(
                  child: Text(
                    widget.textConverter != null
                        ? widget.textConverter(choice)
                        : choice.toString(),
                    style: fontBook.copyWith(fontSize: 12),
                    softWrap: true,
                  ),
                )
              ],
            ),
          );
        })),
      ),
    );
  }
}
