import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/screens/contract_detail/widgets/edit_customer_info.dart';
import 'package:wecare247/screens/contract_detail/widgets/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CustomerTab extends StatefulWidget {
  final ContractDetail contract;
  CustomerTab({Key key, this.contract}) : super(key: key);

  @override
  _CustomerTabState createState() => _CustomerTabState();
}

class _CustomerTabState extends State<CustomerTab> {
  void editCustomerInfo() async {
    final info = await showDialog<EditCustomerInfoDto>(
        context: context,
        builder: (context) => EditCustomerInfoScreen(
              customerInfo: widget.contract.customer,
            ));
    if (info != null) {
      info.patient = EditPatientDto.fromDetail(widget.contract.patient);
      BlocProvider.of<ContractBloc>(context).editCustomerInfo(dto: info);
    }
  }

  @override
  Widget build(BuildContext context) {
    String genderName;
    switch (widget.contract.customer.gender) {
      case 0:
        genderName = "Khác";
        break;
      case 1:
        genderName = "Nam";
        break;
      case 2:
      default:
        genderName = "Nữ";
    }
    return Padding(
      padding: EdgeInsets.all(24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: UICircleAvatar(
              imageUrl: widget.contract.customer.avatar,
              size: 80,
            ),
          ),
          SizedBox(height: 8),
          Divider(),
          TextRow(
            title: "Mã khách hàng:",
            value: widget.contract.customer.id,
          ),
          Divider(),
          TextCol(
            title: "Họ tên:",
            value: widget.contract.customer.fullName,
          ),
          Divider(),
          TextRow(
            title: "Giới tính:",
            value: genderName,
          ),
          Divider(),
          TextRow(
              title: "Số điện thoại:",
              value: widget.contract.customer.contactPhone),
          Divider(),
          TextCol(
            title: "Địa chỉ liên lạc:",
            value: [
              widget.contract.customer.street,
              widget.contract.customer.ward.name,
              widget.contract.customer.district.name,
              widget.contract.customer.province.name
            ].join(", "),
          ),
          Divider(),
          TextCol(
            title: "Ghi chú thêm:",
            value: widget.contract.customer.note == null ||
                    widget.contract.customer.note.isEmpty
                ? "Không có"
                : widget.contract.customer.note,
          ),
          Divider(),
          Center(
            child: UIButton(
              onPressCallback: editCustomerInfo,
              label: "Thay đổi thông tin",
              width: 200,
              borderRadius: BorderRadius.circular(20),
              borderColor: UIColors.mainColor(),
              labelStyle: fontMedium.copyWith(color: UIColors.mainColor()),
            ),
          ),
        ],
      ),
    );
  }
}
