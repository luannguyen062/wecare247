import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:wecare247/bloc/contract/contract_bloc.dart';
import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/screens/create_edit_contract/utils/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/custom_text_field.dart';
import 'package:wecare247/shared/index.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ServiceExpandableDisplay extends StatefulWidget {
  final ServiceData service;
  final ContractDetail contract;
  ServiceExpandableDisplay({Key key, this.service, this.contract})
      : super(key: key);

  @override
  _ServiceExpandableDisplayState createState() =>
      _ServiceExpandableDisplayState();
}

class _ServiceExpandableDisplayState extends State<ServiceExpandableDisplay>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> rotateAnimation;
  ExpandableController expandableController;
  PatientCarerBloc patientCarerBloc;

  Widget buildPatientCarerDetail() {
    if (widget.service.patientCarerId == null) {
      return Text(
        "Chưa có",
        style: fontBook.copyWith(fontSize: 14),
      );
    }
    return BlocBuilder(
      bloc: patientCarerBloc,
      builder: (context, state) {
        if (state is PatientCarerGotten) {
          return Column(
            children: [
              TextRow(
                title: "Mã số:",
                value: state.patientCarer.id,
              ),
              TextRow(
                title: "Họ tên:",
                value: state.patientCarer.fullName,
              ),
              TextRow(
                title: "Điện thoại:",
                value: state.patientCarer.contactPhone,
              ),
            ],
          );
        }
        return Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation(UIColors.mainColor()),
          ),
        );
      },
    );
  }

  void changeServicePrice() async {
    final textController =
        TextEditingController(text: widget.service.price.toString());
    final formKey = GlobalKey<FormState>();
    final newPrice = await showDialog<double>(
        context: context,
        builder: (context) {
          return Center(
            child: Form(
              key: formKey,
              child: Card(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: Padding(
                  padding: EdgeInsets.all(24),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      CustomTextField(
                        controller: textController,
                        title: "Giá tiền theo ngày",
                        keyboardType: TextInputType.number,
                        validator: validateNumber,
                      ),
                      SizedBox(height: 12),
                      UIButton(
                        onPressCallback: () {
                          if (formKey.currentState.validate()) {
                            Navigator.of(context)
                                .pop(double.parse(textController.text));
                          }
                        },
                        label: "Thay đổi",
                        labelStyle: fontMedium.copyWith(color: Colors.white),
                        backgroundColor: UIColors.mainColor(),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
    if (newPrice != null) {
      BlocProvider.of<ContractBloc>(context).editService(
          data: ServiceEditData(
              serviceId: widget.service.serviceId,
              contractId: widget.contract.id,
              to: widget.service.to,
              from: widget.service.from,
              patientCarerId: widget.service.patientCarerId,
              price: newPrice),
          id: widget.service.id);
    }
  }

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 250));
    rotateAnimation =
        Tween<double>(begin: 0, end: 0.5).animate(animationController);
    expandableController = ExpandableController()
      ..addListener(() {
        if (expandableController.expanded) {
          animationController.forward();
        } else {
          animationController.reverse();
        }
      });
    patientCarerBloc = PatientCarerBloc();
    if (widget.service.patientCarerId != null) {
      patientCarerBloc.getPatientCarer(id: widget.service.patientCarerId);
    }
  }

  @override
  Widget build(BuildContext context) {
    final dateFormatter = DateFormat("dd/MM/yyyy");
    final numberFormat = NumberFormat("####,###.##");
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: UIColors.borderColor),
        borderRadius: BorderRadius.circular(8),
      ),
      margin: EdgeInsets.only(bottom: 16),
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Dịch vụ tạo lúc ${DateFormat("dd/MM/yyyy").add_jm().format(widget.service.created.toLocal())}",
            style: fontMedium.copyWith(color: Colors.black, fontSize: 15),
          ),
          SizedBox(height: 12),
          Stack(
            alignment: Alignment.center,
            children: [
              Divider(
                height: 30,
              ),
              Align(
                alignment: Alignment.centerRight,
                child: TextButton(
                    style: TextButton.styleFrom(
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                          side: BorderSide(color: UIColors.borderColor),
                        ),
                        padding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 4)),
                    onPressed: () {
                      setState(() {
                        expandableController.toggle();
                      });
                    },
                    child: RotationTransition(
                      child: Icon(
                        FontAwesomeIcons.angleDoubleDown,
                        color: Colors.black,
                        size: 12,
                      ),
                      turns: rotateAnimation,
                    )),
              )
            ],
          ),
          Expandable(
            controller: expandableController,
            collapsed: Container(),
            expanded: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextCol(
                  title: "Loại hình dịch vụ:",
                  value: widget.service.serviceName,
                ),
                TextRow(
                    title: "Thời gian bắt đầu:",
                    value: dateFormatter.format(widget.service.from.toLocal())),
                TextRow(
                  title: "Thời gian kết thúc:",
                  value: dateFormatter.format(widget.service.to.toLocal()),
                ),
                Divider(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 8),
                    Text("Người nuôi bệnh được chỉ định:",
                        style: fontMedium.copyWith(
                            color: Colors.black, fontSize: 15)),
                    buildPatientCarerDetail()
                  ],
                ),
                // Divider(),
                // TextRow(
                //   title: "Thời gian dự kiến:",
                //   value:
                //       numberFormat.format(widget.service.calendarHours ?? 0) +
                //           " giờ",
                // ),
                // TextRow(
                //   title: "Thời gian thực tế:",
                //   value: numberFormat.format(widget.service.actualHours ?? 0) +
                //       " giờ",
                // ),
                Divider(),
                TextRow(
                  title: "Tổng giá tiền:",
                  value: numberFormat.format(widget.service.totalPrice ?? 0) +
                      " VND",
                ),
                TextRow(
                  title: "Giá tiền theo ngày:",
                  value:
                      numberFormat.format(widget.service.price ?? 0) + " VND",
                  onTap: changeServicePrice,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }
}
