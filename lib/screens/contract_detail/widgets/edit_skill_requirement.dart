import 'package:wecare247/bloc/data/data_bloc.dart';
import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/index.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EditSkillRequirementScreen extends StatefulWidget {
  final List<PatientCarerSkill> previousSkillList;
  EditSkillRequirementScreen({
    Key key,
    this.previousSkillList,
  }) : super(key: key);

  @override
  _EditSkillRequirementScreenState createState() =>
      _EditSkillRequirementScreenState();
}

class _EditSkillRequirementScreenState
    extends State<EditSkillRequirementScreen> {
  DataBloc dataBloc;
  List<PatientCarerSkill> capabilities;
  ValueNotifier<PatientCarerSkill> weightNotifier;
  ValueNotifier<PatientCarerSkill> actionCapabilityNotifier;
  List<PatientCarerSkill> weightSkills = [];
  List<PatientCarerSkill> actionCapabilitySkills = [];
  List<PatientCarerSkill> capabilitySkills = [];

  Widget buildTopAppBar() {
    return AppBar(
      title: Text(
        "Chỉnh sửa nơi nhận hợp đồng",
        style: fontMedium.copyWith(color: Colors.white, fontSize: 14),
      ),
      backgroundColor: UIColors.mainColor(),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () => Navigator.of(context).pop(),
      ),
      centerTitle: true,
      actions: [
        IconButton(
          icon: Icon(Icons.check),
          color: Colors.white,
          onPressed: () {
            capabilities
                .addAll([weightNotifier.value, actionCapabilityNotifier.value]);
            Navigator.of(context).pop(capabilities);
          },
        )
      ],
    );
  }

  Widget buidlCapabilitiesOption(List<PatientCarerSkill> options) {
    return Column(
      children: List<Widget>.from(options.map((input) {
        return Padding(
          padding: EdgeInsets.only(top: 8),
          child: CustomCheckboxRow<PatientCarerSkill>(
            itemList: capabilities,
            item: input,
            itemAsString: (skill) => skill.description ?? skill.skillName,
          ),
        );
      })),
    );
  }

  Widget buildActionCapabilityDropdown(List<PatientCarerSkill> options) {
    return CustomDropdown<PatientCarerSkill>(
      choices: options,
      notifier: actionCapabilityNotifier,
      textConverter: (skill) => skill.description ?? skill.skillName,
      title: "Khả năng vặn động",
    );
  }

  Widget buildWeightDropdown(List<PatientCarerSkill> options) {
    return CustomDropdown<PatientCarerSkill>(
      choices: options,
      notifier: weightNotifier,
      textConverter: (skill) => skill.description ?? skill.skillName,
      title: "Cân nặng",
    );
  }

  Widget buildBody(List<PatientCarerSkill> skills) {
    for (PatientCarerSkill skill in skills) {
      switch (skill.groupId) {
        case "GRP01":
          if (weightSkills
              .every((e) => e.skillId != skill.id && e.id != skill.id)) {
            weightSkills.add(skill);
          }
          break;
        case "GRP02":
          if (actionCapabilitySkills
              .every((e) => e.skillId != skill.id && e.id != skill.id)) {
            actionCapabilitySkills.add(skill);
          }
          break;
        case "GRP03":
          if (capabilitySkills
              .every((e) => e.skillId != skill.id && e.id != skill.id)) {
            capabilitySkills.add(skill);
          }
          break;
      }
    }
    return ListView(
      children: [
        buildWeightDropdown(weightSkills),
        SizedBox(height: 8),
        buildActionCapabilityDropdown(actionCapabilitySkills),
        SizedBox(height: 8),
        buidlCapabilitiesOption(capabilitySkills),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    weightSkills = [
      widget.previousSkillList
          .firstWhere((e) => e.skillId.endsWith("KG"), orElse: () => null)
    ];
    actionCapabilitySkills = [
      widget.previousSkillList
          .firstWhere((e) => e.skillId.startsWith("LV"), orElse: () => null)
    ];
    capabilitySkills = List<PatientCarerSkill>.from(
        widget.previousSkillList.where((e) => e.skillId.startsWith("GRP03")));
    weightNotifier = ValueNotifier<PatientCarerSkill>(widget.previousSkillList
        .firstWhere((e) => e.skillId.endsWith("KG"), orElse: () => null));
    actionCapabilityNotifier = ValueNotifier<PatientCarerSkill>(widget
        .previousSkillList
        .firstWhere((e) => e.skillId.startsWith("LV"), orElse: () => null));
    capabilities = List<PatientCarerSkill>.from(widget.previousSkillList.where(
        (e) => !e.skillId.endsWith("KG") && !e.skillId.startsWith("LV")));
    dataBloc = DataBloc();
    dataBloc.getPatientCarerSkill();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildTopAppBar(),
      body: Padding(
        padding: EdgeInsets.all(24),
        child: BlocConsumer<DataBloc, DataState>(
          bloc: dataBloc,
          listener: (context, state) {
            if (state is TokenExpireForData) {
              BlocProvider.of<AuthenticationBloc>(context).logOut();
            }
          },
          builder: (context, state) {
            if (state is PatientCarerSkillGotten) {
              return buildBody(state.skills);
            }
            return Container(
              height: 300,
              child: Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(UIColors.mainColor()),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    weightNotifier.dispose();
    actionCapabilityNotifier.dispose();
    super.dispose();
  }
}
