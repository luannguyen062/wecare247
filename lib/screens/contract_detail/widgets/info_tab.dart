import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class InfoTab extends StatefulWidget {
  final ContractDetail contract;
  InfoTab({Key key, this.contract}) : super(key: key);

  @override
  _InfoTabState createState() => _InfoTabState();
}

class _InfoTabState extends State<InfoTab> {
  void updateLocationId() async {
    BlocProvider.of<DataBloc>(context).getLocationList();
  }

  void updateLocationDepartmentId() async {
    BlocProvider.of<DataBloc>(context).getLocationDepartmentList(
        locationId: widget.contract.contractLocationId);
  }

  void updateContractSourceId() async {
    BlocProvider.of<DataBloc>(context).getContractSourceList();
  }

  @override
  Widget build(BuildContext context) {
    final dateFormatter = DateFormat("dd/MM/yyyy");
    final numberFormat = NumberFormat("####,###.##");
    String statusText;
    Color statusColor;
    switch (widget.contract.status) {
      case 0:
        statusText = "Chờ xử lý";
        statusColor = Colors.orangeAccent;
        break;
      case 10:
        statusText = "Khởi tạo";
        statusColor = Colors.blueAccent;
        break;
      case 20:
        statusText = "Đang hiệu lực";
        statusColor = Colors.green;
        break;
      case 100:
        statusText = "Hoàn thành";
        statusColor = Colors.purple;
        break;
      case 99:
      default:
        statusText = "Đã hủy";
        statusColor = Colors.red;
    }
    return Padding(
      padding: EdgeInsets.all(24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Divider(),
          TextRow(
            title: "Nhận ngày:",
            value: dateFormatter.format(widget.contract.signingDate),
          ),
          Divider(),
          TextCol(
            title: "Nguồn hợp đồng:",
            value: widget.contract.contractSourceName,
            onTap: updateContractSourceId,
          ),
          Divider(),
          TextCol(
            title: "Nơi nhận hợp đồng:",
            value: widget.contract.contractLocationName,
            onTap: updateLocationId,
          ),
          Divider(),
          TextCol(
            title: "Khoa bệnh:",
            value: widget.contract.locationDepartmentName,
            onTap: updateLocationDepartmentId,
          ),
          Divider(),
          TextCol(
            title: "Ghi chú:",
            value: widget.contract.note ?? "Không có",
          ),
          Divider(),
          TextRow(
            title: "Trạng thái:",
            value: statusText,
            valueColor: statusColor,
          ),
          Divider(),
          TextRow(
            title: "Thời gian thực tế:",
            value:
                numberFormat.format(widget.contract.totalHours ?? 0) + " giờ",
          ),
          Divider(),
          TextRow(
            title: "Tổng tiền:",
            value:
                numberFormat.format(widget.contract.totalAmount ?? 0) + " VND",
          ),
          Divider(),
          TextRow(
            title: "Đã trả:",
            value: numberFormat.format(widget.contract.paymentAmount ?? 0) +
                " VND",
          ),
          Divider(),
          TextRow(
            title: "Còn lại:",
            value: numberFormat.format(widget.contract.remainingAmount ??
                    widget.contract.totalAmount ??
                    0) +
                " VND",
          ),
          Divider(),
        ],
      ),
    );
  }
}
