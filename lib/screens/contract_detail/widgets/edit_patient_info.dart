import 'package:wecare247/screens/create_edit_contract/utils/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/index.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EditPatientInfoScreen extends StatefulWidget {
  final PatientDetail patientInfo;
  EditPatientInfoScreen({Key key, this.patientInfo}) : super(key: key);

  @override
  _EditPatientInfoScreenState createState() => _EditPatientInfoScreenState();
}

class _EditPatientInfoScreenState extends State<EditPatientInfoScreen> {
  GlobalKey<FormState> formKey;
  TextEditingController patientFullNameController;
  TextEditingController patientWeightController;
  ValueNotifier<int> patientYearOfBirthNotifier;
  ValueNotifier<int> patientGenderNotifier;
  Widget buildTopAppBar() {
    return AppBar(
      title: Text(
        "Chỉnh sửa thông tin bệnh nhân",
        style: fontMedium.copyWith(color: Colors.white, fontSize: 14),
      ),
      backgroundColor: UIColors.mainColor(),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () => Navigator.of(context).pop(),
      ),
      centerTitle: true,
      actions: [
        IconButton(
          icon: Icon(Icons.check),
          color: Colors.white,
          onPressed: () {
            if (formKey.currentState.validate()) {
              Navigator.of(context).pop(EditPatientDto(
                  fullName: patientFullNameController.text,
                  id: widget.patientInfo.id,
                  gender: patientGenderNotifier.value,
                  yearOFBirth: patientYearOfBirthNotifier.value,
                  weight: double.tryParse(patientWeightController.text) ?? 0));
            }
          },
        )
      ],
    );
  }

  Widget buildBody() {
    return Form(
      key: formKey,
      child: ListView(
        padding: EdgeInsets.all(24),
        children: [
          Column(
            children: [
              TitleRow(title: "Bệnh nhân"),
              SizedBox(height: 8),
              CustomTextField(
                controller: patientFullNameController,
                title: "Họ tên",
                validator: validateNonEmptyString,
                keyboardType: TextInputType.name,
              ),
              SizedBox(height: 8),
              CustomDropdown<int>(
                choices: List<int>.generate(
                  100,
                  (index) => DateTime.now().year - index,
                ),
                notifier: patientYearOfBirthNotifier,
                title: "Năm sinh",
                validator: (value) {
                  return validateNonEmptyString(value?.toString());
                },
              ),
              SizedBox(height: 8),
              CustomTextField(
                controller: patientWeightController,
                title: "Cân nặng",
                validator: validateNumber,
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: 8),
              CustomDropdown<int>(
                  title: "Giới tính",
                  notifier: patientGenderNotifier,
                  validator: (value) => value != null ? null : "",
                  choices: [0, 1, 2],
                  textConverter: convertIntToGender),
            ],
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    formKey = GlobalKey<FormState>();
    patientFullNameController =
        TextEditingController(text: widget.patientInfo.fullName);
    patientGenderNotifier = ValueNotifier<int>(widget.patientInfo.gender);
    patientWeightController =
        TextEditingController(text: widget.patientInfo.weight.toString());
    patientYearOfBirthNotifier =
        ValueNotifier<int>(widget.patientInfo.yearOfBirth);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildTopAppBar(),
      body: buildBody(),
    );
  }

  @override
  void dispose() {
    patientFullNameController.dispose();
    patientWeightController.dispose();
    patientYearOfBirthNotifier.dispose();
    patientGenderNotifier.dispose();
    super.dispose();
  }
}
