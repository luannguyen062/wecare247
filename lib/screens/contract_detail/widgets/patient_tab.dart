import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/screens/contract_detail/widgets/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class PatientTab extends StatefulWidget {
  final ContractDetail contract;
  PatientTab({Key key, this.contract}) : super(key: key);

  @override
  _PatientTabState createState() => _PatientTabState();
}

class _PatientTabState extends State<PatientTab> {
  void editPatientInfo() async {
    final info = await showDialog<EditPatientDto>(
        context: context,
        builder: (context) => EditPatientInfoScreen(
              patientInfo: widget.contract.patient,
            ));
    if (info != null) {
      final dto = EditCustomerInfoDto.fromDetail(widget.contract.customer);
      dto.patient = info;
      BlocProvider.of<ContractBloc>(context).editCustomerInfo(dto: dto);
    }
  }

  @override
  Widget build(BuildContext context) {
    final numberFormat = NumberFormat("###,###.###");
    String genderName;
    switch (widget.contract.patient.gender) {
      case 0:
        genderName = "Khác";
        break;
      case 1:
        genderName = "Nam";
        break;
      case 2:
      default:
        genderName = "Nữ";
    }
    return Padding(
      padding: EdgeInsets.all(24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Divider(),
          TextRow(
            title: "Mã bệnh nhân:",
            value: widget.contract.patient.id,
          ),
          Divider(),
          TextCol(
            title: "Họ tên:",
            value: widget.contract.patient.fullName,
          ),
          Divider(),
          TextRow(
            title: "Giới tính:",
            value: genderName,
          ),
          Divider(),
          TextRow(
              title: "Năm sinh:",
              value: widget.contract.patient.yearOfBirth.toString()),
          Divider(),
          TextRow(
            title: "Cân nặng:",
            value: "${numberFormat.format(widget.contract.patient.weight)} kg",
          ),
          Divider(),
          Center(
            child: UIButton(
              onPressCallback: editPatientInfo,
              label: "Thay đổi thông tin",
              width: 200,
              borderRadius: BorderRadius.circular(20),
              borderColor: UIColors.mainColor(),
              labelStyle: fontMedium.copyWith(color: UIColors.mainColor()),
            ),
          ),
        ],
      ),
    );
  }
}
