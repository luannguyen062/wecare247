import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/screens/create_edit_contract/utils/index.dart';
import 'package:wecare247/screens/create_edit_contract/widgets/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AddNewServiceScreen extends StatefulWidget {
  final ContractDetail contract;
  final ServiceData serviceData;

  AddNewServiceScreen({Key key, this.contract, this.serviceData})
      : super(key: key);

  @override
  _AddNewServiceScreenState createState() => _AddNewServiceScreenState();
}

class _AddNewServiceScreenState extends State<AddNewServiceScreen> {
  DataBloc dataBloc;
  ServiceEditData data;
  ValueNotifier<DateTime> fromDate;
  ValueNotifier<DateTime> toDate;
  ValueNotifier<Service> service;
  GlobalKey<FormState> formKey;
  TextEditingController priceController;
  List<Service> serviceList;

  void returnData() {
    if (fromDate.value == null || toDate.value == null) {
      showErrorFlushbar("Vui lòng chọn thời gian bắt đầu và kết thúc dịch vụ");
    } else if (fromDate.value.isAfter(toDate.value)) {
      showErrorFlushbar("Thời gian bắt đầu phải trước thời gian kết thúc");
    } else if (service.value == null) {
      showErrorFlushbar("Vui lòng chọn loại hình dịch vụ trước khi tiếp tục");
    } else if (formKey.currentState.validate()) {
      Navigator.of(context).pop(data);
    }
  }

  @override
  void initState() {
    super.initState();
    formKey = GlobalKey<FormState>();
    dataBloc = DataBloc();
    dataBloc.getServiceList();
    data = ServiceEditData(
        locationId: widget.contract.contractLocationId,
        from: widget.serviceData?.from,
        to: widget.serviceData?.to,
        contractId: widget.contract?.id,
        serviceId: widget.serviceData?.serviceId,
        price: widget.serviceData?.totalPrice);
    fromDate = ValueNotifier<DateTime>(data.from);
    toDate = ValueNotifier<DateTime>(data.to);
    service = ValueNotifier<Service>(null);
    priceController = TextEditingController(
        text: (widget.serviceData?.totalPrice ?? 0).toString());
    fromDate.addListener(() => setState(() => data.from = fromDate.value));
    toDate.addListener(() => setState(() => data.to = toDate.value));
    service.addListener(() {
      data.serviceId = service.value.id;
      priceController.text = (service.value.price ?? 0).toString();
    });
    priceController
        .addListener(() => data.price = double.parse(priceController.text));
    serviceList = [];
    if (widget.serviceData != null) {
      final currentService = Service(
          id: widget.serviceData.serviceId,
          name: widget.serviceData.serviceName,
          price: widget.serviceData.totalPrice,
          priceNegotiation: false);
      serviceList.add(currentService);
      service.value = currentService;
    }
  }

  Widget buildTopAppBar() {
    return AppBar(
      title: Text(
        widget.serviceData == null ? "Thêm dịch vụ mới" : "Thay đổi dịch vụ",
        style: fontMedium.copyWith(color: Colors.white, fontSize: 14),
      ),
      backgroundColor: UIColors.mainColor(),
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () => Navigator.of(context).pop(),
      ),
      centerTitle: true,
      actions: [
        IconButton(
          icon: Icon(Icons.check),
          color: Colors.white,
          onPressed: returnData,
        )
      ],
    );
  }

  Widget buildServiceForm(List<Service> stateServiceList) {
    stateServiceList.forEach((service) {
      if (serviceList.isEmpty || serviceList.every((e) => e.id != service.id)) {
        serviceList.add(service);
      }
    });
    return Form(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomDropdown<Service>(
            choices: serviceList,
            notifier: service,
            textConverter: (item) => item.name,
            title: "Loại hình dịch vụ",
            validator: (e) => e != null ? null : "",
          ),
          SizedBox(height: 8),
          ValueListenableBuilder<Service>(
            valueListenable: service,
            builder: (context, value, child) {
              if (value == null) {
                return Container();
              }
              return CustomTextField(
                controller: priceController,
                keyboardType: TextInputType.number,
                title: "Giá dịch vụ",
                isDisabled: !value.priceNegotiation,
                validator: validateNumber,
              );
            },
          ),
          SizedBox(height: 8),
          TitleRow(title: "Thời gian thực hiện"),
          CustomDateRangePicker(
            startDateNotifier: fromDate,
            endDateNotifier: toDate,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildTopAppBar(),
      body: ListView(
        padding: EdgeInsets.all(12),
        children: [
          BlocConsumer(
            bloc: dataBloc,
            listener: (context, state) {
              if (state is ErrorState) {
                showErrorFlushbar(state.message, true);
              }
              if (state is TokenExpireForData) {
                BlocProvider.of<AuthenticationBloc>(context).logOut();
              }
            },
            buildWhen: (prevState, curState) =>
                curState is ServiceListGotten || curState is GettingServiceList,
            builder: (context, state) {
              if (state is ServiceListGotten) {
                return buildServiceForm(state.serviceList);
              }
              return Container(
                height: 300,
                child: Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(UIColors.mainColor()),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
