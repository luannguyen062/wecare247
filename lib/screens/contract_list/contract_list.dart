import 'package:wecare247/bloc/common_state.dart';
import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/contract.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ContractList extends StatefulWidget {
  ContractList({Key key}) : super(key: key);

  @override
  _ContractListState createState() => _ContractListState();
}

class _ContractListState extends State<ContractList> {
  final ContractBloc contractBloc = ContractBloc();
  final ValueNotifier<int> pageCountNotifier = ValueNotifier<int>(null);
  final ValueNotifier<int> pageController = ValueNotifier<int>(null);

  void listenToContractBloc(BuildContext context, state) {
    if (state is ErrorState) {
      showErrorFlushbar(state.message);
    }
    if (state is ContractGotten) {
      pageCountNotifier.value = state.model.pages;
      pageController.value = state.model.currentPage;
    } else if (state is TokenExpireForContract) {
      BlocProvider.of<AuthenticationBloc>(context).logOut();
    } else if (state is MonitorPinged) {
      showErrorFlushbar("Đã gửi nhắc nhở tới giám sát viên.");
    }
  }

  @override
  void initState() {
    super.initState();
    contractBloc.getContract(
        dto: GetContractDto(
      page: 1,
      size: 10,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ContractBloc, ContractState>(
      bloc: contractBloc,
      listener: listenToContractBloc,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: UIColors.mainColor(),
          elevation: 0,
          title: Text(
            "Danh sách hợp đồng".toUpperCase(),
            style: fontMedium.copyWith(color: Colors.white, fontSize: 16),
          ),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              onPressed: () => Navigator.of(context).pop()),
          centerTitle: true,
        ),
        body: ListView(
          children: [
            Container(
              height: 20,
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(25.0),
                    bottomRight: Radius.circular(25.0)),
                color: Theme.of(context).accentColor,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Padding(
              padding: EdgeInsets.all(24),
              child: BlocBuilder(
                bloc: contractBloc,
                buildWhen: (prevState, curState) {
                  return curState is ContractGotten ||
                      curState is GettingContract;
                },
                builder: (context, state) {
                  if (state is ContractGotten) {
                    return Column(
                        children: List<Widget>.from(
                            state.model.items.map((item) => ContractCard(
                                  contract: item,
                                ))));
                  }
                  return SizedBox(
                    height: 500,
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(Colors.black),
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
        bottomNavigationBar: IndexPageIndicator(
          pageCountNotifier: pageCountNotifier,
          pageNotifier: pageController,
          goToPageFunction: (currentPage) => contractBloc.getContract(
              dto: GetContractDto(page: currentPage, size: 10)),
        ),
      ),
    );
  }
}
