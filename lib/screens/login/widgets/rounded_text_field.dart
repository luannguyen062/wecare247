import 'package:flutter/material.dart';

class RoundedTextField extends StatelessWidget {
  final TextEditingController controller;
  final String Function(String) validator;
  final String title;
  final bool obscure;
  final IconData prefixIcon;
  final TextInputType keyboardType;
  const RoundedTextField(
      {Key key,
      this.controller,
      this.validator,
      this.title,
      this.obscure = false,
      this.prefixIcon,
      this.keyboardType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.grey[700]),
          borderRadius: BorderRadius.circular(300)),
      padding: EdgeInsets.all(8),
      child: TextFormField(
        controller: controller,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        validator: validator,
        obscureText: obscure,
        decoration: InputDecoration(
          prefixIcon: Icon(
            prefixIcon,
            color: Colors.black,
          ),
          border: OutlineInputBorder(borderSide: BorderSide.none),
          focusedBorder: OutlineInputBorder(borderSide: BorderSide.none),
          focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide.none),
          errorBorder: OutlineInputBorder(borderSide: BorderSide.none),
          hintText: title,
          contentPadding: EdgeInsets.only(left: 16),
          errorMaxLines: 1,
          errorStyle: TextStyle(height: 0.3),
        ),
        keyboardType: keyboardType,
        expands: false,
      ),
    );
  }
}
