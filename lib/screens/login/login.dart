import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/screens/login/widgets/rounded_text_field.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:validators/validators.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginScreen> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  AuthenticationBloc authenticationBloc;
  final formKey = GlobalKey<FormState>();

  // React to authentication bloc state
  void listenToAuthenticationBloc(BuildContext context, state) async {
    if (state is AuthenticatingUser) {
      showLoadingDialog(context);
    } else if (state is UserAuthenticated) {
      Navigator.of(context).popUntil(ModalRoute.withName("login"));
      final name = await LocalCache.getSecured(USER_NAME_KEY);
      Navigator.of(context)
          .pushReplacementNamed("home", arguments: {"username": name});
    } else if (state is ErrorAuthenticatingUser) {
      Navigator.of(context).popUntil(ModalRoute.withName("login"));
      showErrorFlushbar(state.message);
    } else if (state is ConnectionErrorForAuthentication) {
      Navigator.of(context).popUntil(ModalRoute.withName("login"));
      showErrorFlushbar(state.message);
    }
  }

  // Check email format
  String validateEmail(String text) {
    if (text.isEmpty) {
      return "Vui lòng nhập email";
    }
    if (!isEmail(text)) {
      return "Email không hợp lệ";
    }
    return null;
  }

  // Check password
  String validatePassword(String text) {
    if (text.isEmpty) {
      return "Vui lòng nhập mật khẩu";
    }
    return null;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      listener: listenToAuthenticationBloc,
      bloc: authenticationBloc,
      child: Scaffold(
        body: Form(
          key: formKey,
          child: ListView(
            children: [
              Container(
                height: 400.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(50.0),
                      bottomRight: Radius.circular(50.0)),
                  image: DecorationImage(
                    image: AssetImage('assets/images/image-home.jpeg'),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(50.0),
                        bottomRight: Radius.circular(50.0)),
                  ),
                ),
              ),
              SizedBox(
                height: 24,
              ),
              RoundedTextField(
                controller: usernameController,
                title: "Email",
                // validator: validateEmail,
                keyboardType: TextInputType.emailAddress,
                prefixIcon: Icons.person,
              ),
              SizedBox(
                height: 24,
              ),
              RoundedTextField(
                controller: passwordController,
                title: "Mật khẩu",
                validator: validatePassword,
                prefixIcon: Icons.lock,
                obscure: true,
              ),
              SizedBox(
                height: 24,
              ),
              UIButton(
                label: "Đăng nhập",
                backgroundColor: UIColors.mainColor(),
                width: 200,
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                onPressCallback: () {
                  if (formKey.currentState.validate()) {
                    authenticationBloc.authenticateUser(
                        username: usernameController.text.trim(),
                        password: passwordController.text.trim());
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }
}
