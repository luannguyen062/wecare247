import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';

class BaseScreen extends StatefulWidget {
  BaseScreen({Key key}) : super(key: key);

  @override
  _BaseScreenState createState() => _BaseScreenState();
}

class _BaseScreenState extends State<BaseScreen> {
  @override
  void initState() {
    super.initState();
    validateToken().then((token) async {
      if (token != null) {
        final name = await LocalCache.getSecured(USER_NAME_KEY);
        Navigator.of(context)
            .pushReplacementNamed("home", arguments: {"username": name});
      } else {
        Navigator.of(context).pushReplacementNamed(
          "login",
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child:
            Container(width: 400, child: Image.asset('assets/images/logo.png')),
      ),
    );
  }
}
