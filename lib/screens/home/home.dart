import 'package:wecare247/bloc/contract/contract_bloc.dart';
import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/contract.dart';
import 'package:wecare247/screens/home/widgets/index.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wecare247/shared/index.dart';

class HomeScreen extends StatefulWidget {
  final String username;
  const HomeScreen({Key key, this.username}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomeScreen> {
  ContractBloc contractBloc;
  int stackIndex;
  void getLatestContract() {
    contractBloc.getContract(
        dto: GetContractDto(page: 1, size: 5, orderBy: "created", desc: true));
  }

  @override
  void initState() {
    super.initState();
    stackIndex = 0;
    BlocProvider.of<NotificationBloc>(context).initializeFirebase();
    BlocProvider.of<NotificationBloc>(context).sendFirebaseToken();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    contractBloc = BlocProvider.of<ContractBloc>(context);
    getLatestContract();
  }

  Widget buildBody() {
    switch (stackIndex) {
      case 1:
        return NotificationScreen();
      case 0:
      default:
        return MainScreen(
          username: widget.username,
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ContractBloc>.value(
      value: contractBloc,
      child: BlocListener<ContractBloc, ContractState>(
        listener: (context, state) {
          if (state is MonitorPinged) {
            Navigator.of(context).pop();
            showErrorFlushbar("Đã gửi nhắc nhở tới giám sát viên.");
          } else if (state is PingingMonitor) {
            showLoadingDialog(context);
          } else if (state is ErrorPingingMonitor) {
            Navigator.of(context).pop();
            showErrorFlushbar(state.message);
          }
        },
        child: Scaffold(
          body: buildBody(),
          bottomNavigationBar: CurvedNavigationBar(
            items: [
              Icon(
                Icons.home,
                size: 25,
                color: Theme.of(context).primaryColor,
              ),
              Icon(
                Icons.chat,
                size: 25,
                color: Theme.of(context).primaryColor,
              ),
            ],
            onTap: (index) {
              setState(() {
                stackIndex = index;
              });
            },
            color: Theme.of(context).accentColor,
            buttonBackgroundColor: Theme.of(context).accentColor,
            backgroundColor: Colors.transparent,
            animationCurve: Curves.easeInOut,
            animationDuration: Duration(milliseconds: 600),
          ),
        ),
      ),
    );
  }
}
