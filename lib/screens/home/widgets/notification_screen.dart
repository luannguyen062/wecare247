import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/screens/home/widgets/saved_message_display.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NotificationScreen extends StatefulWidget {
  NotificationScreen({Key key}) : super(key: key);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  Widget buildTopAppBar() {
    return AppBar(
      title: Text(
        "Thông báo gần đây",
        style: fontMedium.copyWith(fontSize: 16, color: Colors.white),
      ),
      backgroundColor: UIColors.mainColor(),
      centerTitle: true,
      elevation: 5,
    );
  }

  Widget buildBody(BuildContext context, NotificationState state) {
    if (state is ErrorGettingLatestNotifications) {
      return Center(
        child: Text(
          "Đã xảy ra lỗi khi tải dữ liệu.\nVui lòng thử lại sau.",
          style: fontMedium.copyWith(color: Colors.grey),
        ),
      );
    } else if (state is LatestNotificationsGotten) {
      if (state.messages.isEmpty) {
        return Center(
          child: Text(
            "Không có thông báo mới.",
            style: fontMedium.copyWith(color: Colors.grey),
          ),
        );
      }
      return ListView(
        padding: EdgeInsets.symmetric(vertical: 8),
        children:
            List<Widget>.from(state.messages.map((e) => SavedMessageDisplay(
                  message: e,
                ))),
      );
    } else {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(UIColors.mainColor()),
        ),
      );
    }
  }

  @override
  void didChangeDependencies() {
    BlocProvider.of<NotificationBloc>(context).getLatestNotifications();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async =>
          BlocProvider.of<NotificationBloc>(context).getLatestNotifications(),
      child: Scaffold(
        appBar: buildTopAppBar(),
        body: BlocBuilder<NotificationBloc, NotificationState>(
          bloc: BlocProvider.of<NotificationBloc>(context),
          buildWhen: (curState, prevState) =>
              curState is GettingLatestNotifications ||
              curState is ErrorGettingLatestNotifications ||
              curState is LatestNotificationsGotten,
          builder: buildBody,
        ),
      ),
    );
  }
}
