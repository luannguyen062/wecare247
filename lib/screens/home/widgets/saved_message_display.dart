import 'dart:convert';

import 'package:wecare247/models/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class SavedMessageDisplay extends StatefulWidget {
  final SavedMessage message;
  const SavedMessageDisplay({Key key, this.message}) : super(key: key);

  @override
  _SavedMessageDisplayState createState() => _SavedMessageDisplayState();
}

class _SavedMessageDisplayState extends State<SavedMessageDisplay> {
  Widget buildProcessIndicator() {
    if (widget.message.processedBy == null) {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 2),
        decoration: BoxDecoration(
            border: Border.all(color: UIColors.mainColor(), width: 2),
            borderRadius: BorderRadius.circular(20)),
        child: Text(
          "Chưa xử lý",
          style: fontMedium.copyWith(color: UIColors.mainColor(), fontSize: 10),
        ),
      );
    } else {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 2),
        decoration: BoxDecoration(
            border: Border.all(color: UIColors.mainColor(), width: 2),
            borderRadius: BorderRadius.circular(20),
            color: UIColors.mainColor()),
        child: Text(
          "Đã được xử lý",
          style: fontMedium.copyWith(color: Colors.white, fontSize: 10),
        ),
      );
    }
  }

  Widget buildSelectPatientCarerDisplay(Map<String, dynamic> jsonData) {
    final dateFormat = DateFormat.yMd().add_jm();
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: TextButton(
        style: TextButton.styleFrom(
            padding: EdgeInsets.all(20),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
                side: BorderSide(color: Colors.grey[400]))),
        onPressed: () {},
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 12,
              height: 12,
              decoration: BoxDecoration(
                  // borderRadius: BorderRadius.circular(20),
                  border: Border.all(color: UIColors.mainColor(), width: 2),
                  color: UIColors.mainColor(),
                  shape: BoxShape.circle),
            ),
            SizedBox(width: 20),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Đã chọn được người nuôi bệnh cho hợp đồng ${jsonData["contractId"]}",
                    style: fontBook.copyWith(
                        fontSize: 16,
                        color: Color(0xff4f4f4f),
                        height: 24 / 16),
                    softWrap: true,
                  ),
                  SizedBox(height: 16),
                  Text(
                    "${dateFormat.format(widget.message.created.toLocal())}",
                    style: fontBook.copyWith(
                        fontSize: 12,
                        height: 18 / 12,
                        color: Color(0xff828282)),
                  ),
                  SizedBox(height: 16),
                  buildProcessIndicator(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildContent(Map<String, dynamic> jsonData) {
    switch (jsonData["type"]) {
      case "SelectPatientCarerEvent":
        return buildSelectPatientCarerDisplay(jsonData);
      default:
        return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    final jsonData = jsonDecode(widget.message.message);
    if (jsonData["type"] == null) {
      return Container();
    }
    return buildContent(jsonData);
  }
}
