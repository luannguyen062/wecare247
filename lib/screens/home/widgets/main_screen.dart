import 'package:flutter/widgets.dart';
import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/contract.dart';
import 'package:wecare247/shared/index.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainScreen extends StatefulWidget {
  final String username;
  MainScreen({Key key, this.username}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  AuthenticationBloc authenticationBloc;

  Widget buildRoundButton(String image, String title, String subtitle) {
    return Container(
      width: 100.0,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              height: 80,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(100.0),
              ),
              child: Center(
                child: Image(
                  image: AssetImage(image),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 65),
            width: 100.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  title,
                  style: fontMedium.copyWith(color: Colors.black, fontSize: 12),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  subtitle,
                  style: fontMedium.copyWith(color: Colors.black, fontSize: 10),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildRoundButtonRow() {
    return Container(
      margin: const EdgeInsets.only(top: 80.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          TextButton(
            style: TextButton.styleFrom(
              padding: EdgeInsets.all(0),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(150)),
            ),
            onPressed: () async {
              bool success = (await Navigator.of(context).pushNamed(
                  'create_edit_contract',
                  arguments: {"username": widget.username})) as bool;
              if (success != null) {
                BlocProvider.of<ContractBloc>(context).getContract(
                    dto: GetContractDto(
                        page: 1, size: 5, orderBy: "created", desc: true));
              }
            },
            child:
                buildRoundButton("assets/images/nurse.png", "Thêm", "HỢP ĐỒNG"),
          ),
          TextButton(
            style: TextButton.styleFrom(
              padding: EdgeInsets.all(0),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(150)),
            ),
            onPressed: () {
              Navigator.of(context).pushNamed('new_contract_list');
            },
            child: buildRoundButton(
                "assets/images/pill.png", "Chưa xử lý", "HỢP ĐỒNG"),
          ),
          TextButton(
            style: TextButton.styleFrom(
              padding: EdgeInsets.all(0),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(150)),
            ),
            onPressed: () {
              Navigator.of(context).pushNamed('contract_list');
            },
            child: buildRoundButton(
                "assets/images/microscope.png", "Lịch sử", "HỢP ĐỒNG"),
          ),
        ],
      ),
    );
  }

  Widget buildContractCarousel() {
    return BlocBuilder<ContractBloc, ContractState>(
      bloc: BlocProvider.of<ContractBloc>(context),
      buildWhen: (prevState, curState) =>
          curState is ContractGotten || curState is GettingContract,
      builder: (context, state) {
        if (state is ContractGotten) {
          return CarouselSlider(
              options: CarouselOptions(enableInfiniteScroll: false),
              items:
                  List<Widget>.from(state.model.items.map((e) => ContractCard(
                        contract: e,
                      ))));
        }
        return SizedBox(
          height: 200,
          child: Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(UIColors.mainColor()),
            ),
          ),
        );
      },
    );
  }

  void logOut() async {
    final confirmed = await showDialog(
        context: context,
        builder: (context) {
          return Center(
            child: Card(
              margin: EdgeInsets.symmetric(horizontal: 12),
              child: Padding(
                padding: const EdgeInsets.all(12),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "Xác nhận",
                      style: fontMedium.copyWith(fontSize: 16),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      "Bạn muốn đăng xuất?",
                      style: fontMedium.copyWith(fontSize: 16),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        TextButton(
                            style: TextButton.styleFrom(
                                backgroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: UIColors.mainColor(), width: 2),
                                    borderRadius: BorderRadius.circular(8))),
                            onPressed: () => Navigator.of(context).pop(false),
                            child: Container(
                                width: 120,
                                alignment: Alignment.center,
                                child: Text(
                                  "Không",
                                  style: fontMedium.copyWith(
                                      color: UIColors.mainColor()),
                                ))),
                        TextButton(
                            style: TextButton.styleFrom(
                                backgroundColor: UIColors.mainColor(),
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: UIColors.mainColor(), width: 2),
                                    borderRadius: BorderRadius.circular(8))),
                            onPressed: () => Navigator.of(context).pop(true),
                            child: Container(
                              width: 120,
                              alignment: Alignment.center,
                              child: Text(
                                "Có",
                                style: fontMedium.copyWith(color: Colors.white),
                              ),
                            )),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        });
    if (confirmed != null && confirmed) {
      authenticationBloc.logOut();
    }
  }

  @override
  void initState() {
    super.initState();
    BlocProvider.of<ContractBloc>(context).getContract(
        dto: GetContractDto(page: 1, size: 5, orderBy: "created", desc: true));
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async => BlocProvider.of<ContractBloc>(context).getContract(
          dto:
              GetContractDto(page: 1, size: 5, orderBy: "created", desc: true)),
      child: BlocListener<AuthenticationBloc, AuthenticationState>(
        bloc: authenticationBloc,
        listener: (context, state) {
          if (state is LoggingOut) {
            showLoadingDialog(context);
          }
        },
        child: ListView(
          children: [
            Stack(
              children: <Widget>[
                Container(
                  height: 120,
                  padding:
                      const EdgeInsets.only(left: 20.0, right: 20.0, top: 30),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(25.0),
                        bottomRight: Radius.circular(25.0)),
                    color: Theme.of(context).accentColor,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${widget.username}",
                        style: TextStyle(fontSize: 16.0, color: Colors.white),
                      ),
                      IconButton(
                          icon: Icon(
                            Icons.exit_to_app,
                            color: Colors.white,
                          ),
                          onPressed: () => logOut()),
                    ],
                  ),
                ),
                buildRoundButtonRow()
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12, top: 24, bottom: 12),
              child: Text(
                'HỢP ĐỒNG GẦN ĐÂY',
                style: fontMedium.copyWith(fontSize: 14.0, color: Colors.black),
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(vertical: 6, horizontal: 0),
              child: buildContractCarousel(),
            ),
          ],
        ),
      ),
    );
  }
}
