import 'package:wecare247/models/contract.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';

class UnpaidContractCard extends StatefulWidget {
  final ContractDetail contract;
  UnpaidContractCard({Key key, this.contract}) : super(key: key);

  @override
  _UnpaidContractCardState createState() => _UnpaidContractCardState();
}

class _UnpaidContractCardState extends State<UnpaidContractCard> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(top: 20.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            boxShadow: [
              BoxShadow(
                  color: Theme.of(context).primaryColor.withOpacity(0.1),
                  offset: Offset(0, 4),
                  blurRadius: 10)
            ],
          ),
          width: MediaQuery.of(context).size.width * 0.32,
          height: 100,
          child: Card(
            elevation: 0.2,
            child: Padding(
              padding: EdgeInsets.only(top: 50),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text("HỢP ĐỒNG",
                      style:
                          fontBook.copyWith(fontSize: 12, color: Colors.black)),
                  Text("#${widget.contract.id}",
                      style: fontMedium.copyWith(
                          fontSize: 10, color: Colors.black)),
                ],
              ),
            ),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.32,
          alignment: Alignment.topCenter,
          child: UICircleAvatar(size: 60),
        ),
      ],
    );
  }
}
