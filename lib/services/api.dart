import 'dart:convert';
import 'dart:io';

import 'package:wecare247/shared/constants/constants.dart';
import 'package:ansicolor/ansicolor.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

void debugPrintResponseDebugMessage(
    Response response, ResponseInterceptorHandler handler) {
  if (response == null) {
    return;
  }
  AnsiPen pen = AnsiPen()..green();
  debugPrint(pen.write("=============RESPONSE============="));
  debugPrint(pen.write(
      "url: ${response.requestOptions.baseUrl + response.requestOptions.path}"));
  debugPrint(pen.write("statusCode: ${response.statusCode}"));
  debugPrint(pen.write("body: ${response.data}"));
  debugPrint(pen.write("messageValue: ${response.statusMessage}"));
  debugPrint(pen.write("=================================="));
  return handler.next(response);
}

void debugPrintErrorDebugMessage(
    DioError error, ErrorInterceptorHandler handler) {
  if (error == null) {
    return;
  }
  AnsiPen pen = AnsiPen()..red();
  Response response = error.response;
  if (response == null) {
    return;
  }
  debugPrint(pen.write("=============ERROR============="));
  debugPrint(pen.write(
      "url: ${error.requestOptions.baseUrl + error.requestOptions.path}"));
  debugPrint(pen.write("error: ${error.error.toString()}"));
  debugPrint(pen.write("statusCode: ${response?.statusCode}"));
  debugPrint(pen.write("body: ${response?.data}"));
  debugPrint(pen.write("messageValue: ${response?.statusMessage}"));
  debugPrint(pen.write("=================================="));
  return handler.next(error);
}

void debugPrintRequestDebugMessage(
    RequestOptions request, RequestInterceptorHandler handler) {
  if (request == null) {
    return;
  }
  String dataString;
  if (request.data is FormData) {
    dataString = (request.data as FormData)
        .files
        .map((e) => e.value.filename)
        .join(", ");
  } else {
    dataString = jsonEncode(request.data);
  }
  AnsiPen pen = AnsiPen()..yellow();
  debugPrint(pen("=============REQUEST============="));
  debugPrint(pen("method: ${request.method}"));
  debugPrint(pen("baseUrl: ${request.baseUrl + request.path}"));
  debugPrint(pen(
      "authenticationHeader: ${request.headers[HttpHeaders.authorizationHeader]}"));
  debugPrint(pen("body: $dataString"));
  debugPrint(pen("parameters: ${request.queryParameters}"));
  debugPrint(pen("=================================\n"));
  return handler.next(request);
}

class API {
  static final Dio dio = Dio(BaseOptions(
      baseUrl: API_BASE_URL, headers: {'Content-Type': 'application/json'}))
    ..interceptors.add(InterceptorsWrapper(
        onRequest: debugPrintRequestDebugMessage,
        onResponse: debugPrintResponseDebugMessage,
        onError: debugPrintErrorDebugMessage));

  API._();

  static API get _instance => API._();

  factory API() => _instance;

  static Future<Response> get(
    String path, {
    Map<String, dynamic> queryParameters,
    Options options,
  }) async {
    return await dio.get(
      path,
      queryParameters: queryParameters,
      options: options,
    );
  }

  static Future<Response> put(
    String path, {
    dynamic data,
    Map<String, dynamic> queryParameters,
    Options options,
  }) async {
    return await dio.put(
      path,
      queryParameters: queryParameters,
      data: data,
      options: options,
    );
  }

  static Future<Response> post(
    String path, {
    dynamic data,
    Map<String, dynamic> queryParameters,
    Options options,
  }) async {
    return await dio.post(
      path,
      queryParameters: queryParameters,
      data: data,
      options: options,
    );
  }

  static Future<Response> delete(
    String path, {
    Map<String, dynamic> queryParameters,
    Options options,
  }) async {
    return await dio.delete(
      path,
      queryParameters: queryParameters,
      options: options,
    );
  }
}
