import 'dart:io';

import 'package:wecare247/models/index.dart';
import 'package:wecare247/services/api.dart';
import 'package:wecare247/services/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class CustomerService {
  CustomerService._();

  factory CustomerService() => _instance;

  static CustomerService get _instance => CustomerService._();

  static Future<bool> editCustomerInfo(String token,
      {EditCustomerInfoDto dto}) async {
    try {
      final Response response = await API.put(
        "$CUSTOMER_PREFIX/${dto.id}",
        data: dto.toJson(),
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        return true;
      }
      return false;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return false;
    }
  }

  static Future<CustomerDetail> getTypeaheadCustomer(String token,
      {String contactPhone}) async {
    try {
      final Response response = await API.get(
        "$TYPEAHEAD_PREFIX/customers",
        queryParameters: {'contactPhone': contactPhone},
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      if (response.statusCode == 200) {
        if (response.data["data"] == null) {
          return CustomerDetail();
        }
        return CustomerDetail.fromJson(response.data["data"]);
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }
}
