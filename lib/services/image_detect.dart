import 'dart:io';

import 'package:wecare247/services/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:http_parser/http_parser.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class ImageDetectService {
  ImageDetectService._();

  factory ImageDetectService() => _instance;

  static ImageDetectService get _instance => ImageDetectService._();

  static Future<String> uploadFile(String token, {String filePath}) async {
    try {
      FormData formData = FormData.fromMap({
        "file": await MultipartFile.fromFile(filePath,
            contentType: MediaType("image", "jpg"))
      });
      final Response response = await API.post(
        FILE_UPLOAD,
        data: formData,
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      return response.data["path"];
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }
}
