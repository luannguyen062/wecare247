import 'package:wecare247/services/index.dart';
import 'package:wecare247/shared/constants/index.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class AuthenticationService {
  AuthenticationService._();

  factory AuthenticationService() => _instance;

  static AuthenticationService get _instance => AuthenticationService._();

  static Future<Map<String, dynamic>> authenticateUser(
      {String username, String password}) async {
    try {
      final Response response = await API
          .post(USER_LOGIN, data: {"username": username, "password": password});
      if (response.statusCode == 200) {
        return response.data;
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }
}
