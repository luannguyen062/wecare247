import 'dart:io';

import 'package:wecare247/models/index.dart';
import 'package:wecare247/services/api.dart';
import 'package:wecare247/services/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class DataService {
  DataService._();

  factory DataService() => _instance;

  static DataService get _instance => DataService._();

  static Future<List<Ward>> getWardList(String token,
      {String districtId}) async {
    try {
      final Response response = await API.get(
        "$SELECT_LIST_PREFIX/$WARD_LIST",
        queryParameters: {"districtId": districtId},
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        return List<Ward>.from(
            response.data["data"].map((data) => Ward.fromSelectList(data)));
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<List<District>> getDistrictList(String token,
      {String provinceId}) async {
    try {
      final Response response = await API.get(
        "$SELECT_LIST_PREFIX/$DISTRICT_LIST",
        queryParameters: {"provinceId": provinceId},
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        return List<District>.from(
            response.data["data"].map((data) => District.fromSelectList(data)));
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<List<Province>> getProvinceList(String token) async {
    try {
      final Response response = await API.get(
        "$SELECT_LIST_PREFIX/$PROVINCE_LIST",
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        return List<Province>.from(
            response.data["data"].map((data) => Province.fromSelectList(data)));
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<List<PatientCarerSkill>> getPatientCarerSkills(String token,
      {String groupId}) async {
    try {
      final Response response = await API.get(
        "$SELECT_LIST_PREFIX/$GET_PATIENT_CARER_SKILLS",
        queryParameters: {"groupId": groupId},
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        return List<PatientCarerSkill>.from(response.data["data"]
            .map((e) => PatientCarerSkill.fromSelectList(e)));
      }
      return [];
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<List<Location>> getLocation(String token) async {
    try {
      final Response response = await API.get(
        "$SELECT_LIST_PREFIX/$LOCATION_LIST",
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        return List<Location>.from(
            response.data["data"].map((data) => Location.fromSelectList(data)));
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<List<PatientCarerLevel>> getPatientCarerLevels(
      String token) async {
    try {
      final Response response = await API.get(
        "$SELECT_LIST_PREFIX/$PATIENT_CARER_LEVEL_LIST",
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        return List<PatientCarerLevel>.from(response.data["data"]
            .map((data) => PatientCarerLevel.fromSelectList(data)));
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<List<Service>> getServiceList(String token) async {
    try {
      final Response response = await API.get(
        "$SELECT_LIST_PREFIX/$GET_SERVICE_LIST",
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        return List<Service>.from(response.data["data"].map((data) =>
            Service.fromSelectList(SelectListServiceData.fromJson(data))));
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<List<LocationDepartment>> getLocationDeparmentList(String token,
      {String locationId}) async {
    try {
      final Response response = await API.get(
        "$SELECT_LIST_PREFIX/$GET_LOCATION_DEPARTMENT_LIST",
        queryParameters: {'LocationId': locationId},
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        return List<LocationDepartment>.from(response.data["data"]
            .map((data) => LocationDepartment.fromJson(data)));
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<List<ContractSource>> getContractSourceList(
      String token) async {
    try {
      final Response response = await API.get(
        "$SELECT_LIST_PREFIX/$GET_CONTRACT_SOURCE_LIST",
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        return List<ContractSource>.from(
            response.data["data"].map((data) => ContractSource.fromJson(data)));
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }
}
