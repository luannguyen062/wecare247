import 'dart:io';

import 'package:wecare247/models/index.dart';
import 'package:wecare247/services/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class PatientCarerService {
  PatientCarerService._();

  factory PatientCarerService() => _instance;

  static PatientCarerService get _instance => PatientCarerService._();

  static Future<bool> createPatientCarer(String token,
      {PatientCarer patientCarer}) async {
    try {
      final patientCarerData = patientCarer.toJson();
      final Response response = await API.post(
        PATIENT_CARER_PREFIX,
        data: patientCarerData,
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      return response.statusCode == 200;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<bool> editPatientCarer(String token,
      {PatientCarer patientCarer}) async {
    try {
      final patientCarerData = patientCarer.toJson();
      final Response response = await API.put(
        "$PATIENT_CARER_PREFIX/${patientCarer.id}",
        data: patientCarerData,
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      return response.statusCode == 200;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<PaginationModel<PatientCarer>> getPatientCarerList(String token,
      {PaginationModel params}) async {
    try {
      final Response response = await API.get(PATIENT_CARER_PREFIX,
          options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
          ),
          queryParameters: {
            "Page": params?.currentPage ?? 0,
            "Size": params?.size ?? 20
          });
      if (response.statusCode == 200) {
        final data = response.data["data"];
        return PaginationModel.fromJson(data,
            converter: (data) => PatientCarer.fromJson(data));
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<PatientCarer> getPatientCarer(String token, {String id}) async {
    try {
      final Response response = await API.get(
        "$PATIENT_CARER_PREFIX/$id",
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      if (response.statusCode == 200) {
        final data = response.data["data"];
        return PatientCarer.fromJson(data);
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }
}
