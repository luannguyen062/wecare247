import 'dart:io';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/services/api.dart';
import 'package:wecare247/shared/index.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class ContractService {
  ContractService._();

  factory ContractService() => _instance;

  static ContractService get _instance => ContractService._();

  static Future<String> createContract(
    String token, {
    CreateContractDto dto,
  }) async {
    try {
      final Response response = await API.post(
        CONTRACT_PREFIX,
        data: dto.toJson(),
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        return response.data["data"];
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<PaginationModel<ContractShort>> getContract(String token,
      {GetContractDto dto}) async {
    try {
      final Response response = await API.get(
        "$CONTRACT_PREFIX?${dto.toParams()}",
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        final data = response.data["data"];
        return PaginationModel.fromJson(data,
            converter: (e) => ContractShort.fromJson(e));
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<ContractDetail> getContractSpecific(String token,
      {String id}) async {
    try {
      final Response response = await API.get(
        "$CONTRACT_PREFIX/$id",
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        final data = response.data["data"];
        return ContractDetail.fromJson(data);
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<List<PatientCarer>> getSuggestPatientCarer(String token,
      {String id}) async {
    try {
      final Response response = await API.get(
        "$CONTRACT_PREFIX/$id/$GET_SUGGEST_PATIENT_CARER",
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      if (response.statusCode == 200) {
        final data = response.data["data"];
        return List<PatientCarer>.from(
            data.map((e) => PatientCarer.fromJson(e)));
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<List<PatientCarerFromSelectList>>
      getSuggestPatientCarerSelectList(String token,
          {String id, DateTime from, DateTime to}) async {
    try {
      final Response response = await API.get(
        "$SELECT_LIST_PREFIX/$PATIENT_CARER_PREFIX",
        queryParameters: {
          "id": id,
          "from": from.toUtc().toIso8601String(),
          "to": to.toUtc().toIso8601String(),
        },
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      if (response.statusCode == 200) {
        final data = response.data["data"];
        return List<PatientCarerFromSelectList>.from(
            data.map((e) => PatientCarerFromSelectList.fromData(e)));
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }

  static Future<bool> selectPatientCarer(String token,
      {SelectPatientCarerDto dto}) async {
    try {
      final data = dto.toJson();
      data["createdBy"] = await LocalCache.getSecured(USER_NAME_KEY);
      final Response response =
          await API.post("$CONTRACT_PREFIX/$SELECT_PATIENT_CARER",
              options: Options(
                headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
              ),
              data: data);
      if (response.statusCode == 200) {
        return true;
      }
      return false;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return false;
    }
  }

  static Future<bool> confirmPatientCarer(String token,
      {String contractId, String patientCarerId}) async {
    try {
      final Response response =
          await API.put("$CONTRACT_PREFIX/$contractId/$CONFIRM_PATIENT_CARER",
              options: Options(
                headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
              ),
              data: {"patientCarerId": patientCarerId});
      if (response.statusCode == 200) {
        return true;
      }
      return false;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return false;
    }
  }

  static Future<bool> pingMonitor(String token, {String id}) async {
    try {
      final Response response = await API.post(
        "$CONTRACT_PREFIX/$id/$PING_MONITOR",
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      if (response.statusCode == 200) {
        return true;
      }
      return false;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return false;
    }
  }

  static Future<bool> editContract(String token, {EditContractDto dto}) async {
    try {
      final Response response = await API.put(
        "$CONTRACT_PREFIX/${dto.id}",
        data: dto.toJson(),
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      if (response.statusCode == 200) {
        return true;
      }
      return false;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return false;
    }
  }

  static Future<bool> createNewService(String token,
      {ServiceEditData data}) async {
    try {
      final Response response = await API.post(
        "$CONTRACT_SERVICE_PREFIX",
        data: data.toJson(),
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      if (response.statusCode == 200) {
        return true;
      }
      return false;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return false;
    }
  }

  static Future<bool> editService(String token,
      {ServiceEditData data, String id}) async {
    try {
      final Response response = await API.put(
        "$CONTRACT_SERVICE_PREFIX/$id",
        data: data.toJson(),
        options: Options(
          headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        ),
      );
      if (response.statusCode == 200) {
        return true;
      }
      return false;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return false;
    }
  }
}
