import 'dart:io';

import 'package:wecare247/models/index.dart';
import 'package:wecare247/services/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

Future<bool> sendFirebaseTokenToServer(String token,
    {String firebaseToken}) async {
  try {
    final Response response = await API.post(
      "$NOTIFICATION_PREFIX/$SEND_FIREBASE_TOKEN",
      data: {"fireBaseToken": firebaseToken},
      options:
          Options(headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
    );
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  } catch (error) {
    if (error is DioError) {
      rethrow;
    }
    debugPrint(error.toString());
    return false;
  }
}

Future<bool> deleteFirebaseTokenFromServer(String token,
    {String userId}) async {
  try {
    final Response response = await API.delete(
      "$NOTIFICATION_PREFIX/$userId/$SEND_FIREBASE_TOKEN",
      options:
          Options(headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
    );
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  } catch (error) {
    if (error is DioError) {
      rethrow;
    }
    debugPrint(error.toString());
    return false;
  }
}

class NotificationService {
  NotificationService._();

  factory NotificationService() => _instance;

  static NotificationService get _instance => NotificationService._();

  static Future<List<SavedMessage>> getLatestNotifications(String token) async {
    try {
      final Response response = await API.get(
        NOTIFICATION_PREFIX,
        options: Options(
            headers: {HttpHeaders.authorizationHeader: "Bearer $token"}),
      );
      if (response.statusCode == 200) {
        return List<SavedMessage>.from(
            response.data["data"].map((e) => SavedMessage.fromJson(e)));
      }
      return null;
    } catch (error) {
      if (error is DioError) {
        rethrow;
      }
      debugPrint(error.toString());
      return null;
    }
  }
}
