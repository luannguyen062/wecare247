import 'package:firebase_core/firebase_core.dart';
import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/contract.dart';
import 'package:wecare247/router/router.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

final GlobalKey<NavigatorState> navKey = GlobalKey<NavigatorState>();

final GlobalKey<_AppState> appKey = GlobalKey<_AppState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(AppWrapper());
}

class AppWrapper extends StatefulWidget {
  @override
  _AppWrapperState createState() => _AppWrapperState();
}

class _AppWrapperState extends State<AppWrapper> {
  NotificationBloc notificationBloc;
  AuthenticationBloc authenticationBloc;

  void listenToNotificationBloc(BuildContext context, state) {
    if (state is TokenExpireForNotification) {
      BlocProvider.of<AuthenticationBloc>(context).logOut();
    }
  }

  void listenToAuthenticationBloc(BuildContext context, state) {
    if (state is UserLoggedOut) {
      showErrorFlushbar(TOKEN_EXPIRE_MESSAGE);
      returnToLoginScreen();
    }
  }

  void listenToContractBloc(BuildContext context, state) {
    if (state is TokenExpireForContract) {
      BlocProvider.of<AuthenticationBloc>(context).logOut();
    }
  }

  @override
  void initState() {
    super.initState();
    notificationBloc = NotificationBloc();
    authenticationBloc = AuthenticationBloc();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<NotificationBloc>.value(
          value: notificationBloc,
        ),
        BlocProvider<ContractBloc>(
          create: (context) => ContractBloc(),
        ),
        BlocProvider<AuthenticationBloc>.value(
          value: authenticationBloc,
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<NotificationBloc, NotificationState>(
            listener: listenToNotificationBloc,
          ),
          BlocListener<AuthenticationBloc, AuthenticationState>(
            listener: listenToAuthenticationBloc,
          ),
          BlocListener<ContractBloc, ContractState>(
            listener: listenToContractBloc,
          ),
        ],
        child: App(
          navKey: navKey,
          key: appKey,
        ),
      ),
    );
  }

  @override
  void dispose() {
    notificationBloc.close();
    super.dispose();
  }
}

Future<void> initializeApp() async {
  await Firebase.initializeApp();
}

class App extends StatefulWidget {
  App({
    Key key,
    @required this.navKey,
  }) : super(key: key);

  final GlobalKey<NavigatorState> navKey;

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final Future _initialization = initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _initialization,
        builder: (context, future) {
          if (future.connectionState == ConnectionState.done) {
            return MaterialApp(
              navigatorKey: widget.navKey,
              navigatorObservers: [BlocNavigatorObserver(context)],
              localizationsDelegates: [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              supportedLocales: [
                Locale('vi', ''),
              ],
              title: 'Sales Admin',
              onGenerateRoute: AppRouter.generateRoute,
              initialRoute: "base",
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                fontFamily: 'Avant Garde',
                primaryColor: Colors.white,
                brightness: Brightness.light,
                accentColor: UIColors.mainColor(),
                focusColor: UIColors.accentColor(),
                hintColor: UIColors.secondColor(),
                timePickerTheme: TimePickerTheme.of(context).copyWith(
                  dialHandColor: UIColors.mainColor(),
                  entryModeIconColor: UIColors.mainColor(),
                  hourMinuteColor: UIColors.mainColor(0.2),
                  hourMinuteTextColor: UIColors.mainColor(),
                  dayPeriodTextColor: UIColors.mainColor(),
                ),
                textTheme: TextTheme(
                  button: TextStyle(color: Theme.of(context).primaryColor),
                  headline5:
                      TextStyle(fontSize: 20.0, color: UIColors.secondColor()),
                  headline4: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.w600,
                      color: UIColors.secondColor()),
                  headline3: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w600,
                      color: UIColors.secondColor()),
                  headline2: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.w700,
                      color: UIColors.mainColor()),
                  headline1: TextStyle(
                      fontSize: 22.0,
                      fontWeight: FontWeight.w300,
                      color: UIColors.secondColor()),
                  subtitle1: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.w500,
                      color: UIColors.secondColor()),
                  headline6: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                      color: UIColors.mainColor()),
                  bodyText2:
                      TextStyle(fontSize: 12.0, color: UIColors.secondColor()),
                  bodyText1: TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.w600,
                      color: UIColors.secondColor()),
                  caption: TextStyle(
                      fontSize: 12.0, color: UIColors.secondColor(0.6)),
                ),
              ),
            );
          }
          return Container();
        });
  }
}

class BlocNavigatorObserver extends NavigatorObserver {
  final BuildContext context;
  BlocNavigatorObserver(this.context) : super();
  void getContract(Route route, Route previousRoute) async {
    final token = await validateToken();
    if (token != null &&
        token.isNotEmpty &&
        previousRoute.settings.name == "home" &&
        route.settings.name != null) {
      BlocProvider.of<ContractBloc>(context).getContract(
          dto:
              GetContractDto(page: 1, size: 5, orderBy: "created", desc: true));
    }
  }

  @override
  void didPop(Route route, Route previousRoute) {
    getContract(route, previousRoute);
  }
}
