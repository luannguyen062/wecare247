part of 'notification_bloc.dart';

enum NotificationType { GetSuggestPatientCarer, SelectPatientCarer }

@immutable
abstract class NotificationState {}

class NotificationInitial extends NotificationState {}

class InitializingFirebase extends NotificationState {}

class FirebaseInitialized extends NotificationState {}

class ErrorInitializingFirebase extends ErrorState with NotificationState {
  ErrorInitializingFirebase({String message}) : super(message);
  String get message => super.message;
}

class SendingNotification extends NotificationState {}

class NotificationSent extends NotificationState {
  final NotificationType type;
  final String payload;
  NotificationSent({this.type, this.payload});
}

class ErrorSendingNotification extends ErrorState with NotificationState {
  ErrorSendingNotification({String message}) : super(message);
  String get message => super.message;
}

class NotificationReceived extends NotificationState {
  final String payload;
  NotificationReceived({
    this.payload,
  });
}

class GetSuggestPatientCarerNotificationReceived extends NotificationState {
  final String id;
  GetSuggestPatientCarerNotificationReceived({this.id});
}

class SuggestedPatientCarerNotificationReceived extends NotificationState {
  final String contractId;
  final PatientCarer patientCarer;
  SuggestedPatientCarerNotificationReceived(
      {this.contractId, this.patientCarer});
}

class GettingLatestNotifications extends NotificationState {}

class LatestNotificationsGotten extends NotificationState {
  final List<SavedMessage> messages;
  LatestNotificationsGotten({this.messages});
}

class ErrorGettingLatestNotifications extends ErrorState
    with NotificationState {
  ErrorGettingLatestNotifications({String message}) : super(message);
  String get message => super.message;
}

class ConnectionErrorForNotifications extends ConnectionErrorState
    with NotificationState {}

class TokenExpireForNotification extends TokenExpire with NotificationState {}

class SendingFirebaseToken extends NotificationState {}

class FirebaseTokenSent extends NotificationState {}

class ErrorSendingFirebaseToken extends ErrorState with NotificationState {
  ErrorSendingFirebaseToken({String message}) : super(message);
  String get message => super.message;
}

class DeletingFirebaseToken extends NotificationState {}

class FirebaseTokenDeleted extends NotificationState {}

class ErrorDeletingFirebaseToken extends ErrorState with NotificationState {
  ErrorDeletingFirebaseToken({String message}) : super(message);
  String get message => super.message;
}
