part of 'notification_bloc.dart';

@immutable
abstract class NotificationEvent {}

class ReceiveNotification extends NotificationEvent {
  final String payload;
  ReceiveNotification({this.payload});
}

class InitializeFirebase extends NotificationEvent {}

class SendNotification extends NotificationEvent {
  final NotificationType type;
  final String title;
  final String message;
  final dynamic payload;
  SendNotification({this.message, this.payload, this.title, this.type});
}

class RemoveLastNotification extends NotificationEvent {}

class GetLatestNotifications extends NotificationEvent {}

class SendFirebaseToken extends NotificationEvent {}

class DeleteFirebaseToken extends NotificationEvent {}
