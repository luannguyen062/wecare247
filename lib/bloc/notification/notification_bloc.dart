import 'dart:async';
import 'dart:convert';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/main.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/services/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:meta/meta.dart';

part 'notification_event.dart';
part 'notification_state.dart';

const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('app_icon');
const IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings();
const MacOSInitializationSettings initializationSettingsMac =
    MacOSInitializationSettings();
final InitializationSettings initializationSettings = InitializationSettings(
    android: initializationSettingsAndroid,
    iOS: initializationSettingsIOS,
    macOS: initializationSettingsMac);

final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin()
  ..initialize(initializationSettings,
      onSelectNotification: onSelectNotification);

Future<void> onSelectNotification(String payload) async {
  BlocProvider.of<NotificationBloc>(appKey.currentContext)
      .receiveNotification(payload: payload);
}

Future<bool> sendLocalNotification(
    {String title, String message, String payload, int id = 0}) async {
  try {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
            'wecare247',
            'Thông báo cho nhân viên kinh doanh',
            'Nhận thông báo về các vấn đề liên quan tới người nuôi bệnh của hợp đồng',
            importance: Importance.max,
            priority: Priority.high,
            ticker: 'ticker',
            styleInformation: BigTextStyleInformation(""));
    await flutterLocalNotificationsPlugin.show(id, title, message,
        NotificationDetails(android: androidPlatformChannelSpecifics),
        payload: payload.toString());
    return true;
  } catch (e) {
    print(e);
    return false;
  }
}

void receiveHubMessage(RemoteMessage message) {
  final data = message.data;
  print("MESSAGE TO SALE");
  print(data);
  switch (data["type"]) {
    case "SelectPatientCarerEvent":
      sendLocalNotification(
          title: "Đã tìm thấy người nuôi bệnh",
          message:
              "Người nuôi bệnh phù hợp cho hợp đồng mã ${data["contractId"]} đã được tìm thấy.",
          payload: data.toString());
  }
}

Future<void> onBackgroundMessageReceive(RemoteMessage message) async {
  await Firebase.initializeApp();
  receiveHubMessage(message);
}

Future<void> setupFirebase() async {
  await FirebaseMessaging.instance.subscribeToTopic(
      FIREBASE_PREFIX + await LocalCache.getSecured(USER_ROLE_KEY));
  FirebaseMessaging.instance.onTokenRefresh.listen((newToken) async {
    final token = await validateToken();
    if (token == null) {
      return;
    }
    await sendFirebaseTokenToServer(token, firebaseToken: newToken);
  });
  FirebaseMessaging.onBackgroundMessage(onBackgroundMessageReceive);
}

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  NotificationBloc() : super(NotificationInitial()) {
    FirebaseMessaging.onMessage.listen(receiveHubMessage);
  }

  void sendFirebaseToken() => add(SendFirebaseToken());

  void deleteFirebaseToken() => add(DeleteFirebaseToken());

  void receiveNotification({String payload}) =>
      add(ReceiveNotification(payload: payload));

  void initializeFirebase() => add(InitializeFirebase());

  void removeLastNotification() => add(RemoveLastNotification());

  void sendNotification(
          {String title,
          String message,
          dynamic payload,
          NotificationType type}) =>
      add(SendNotification(
        message: message,
        title: title,
        payload: payload,
        type: type,
      ));

  void getLatestNotifications() => add(GetLatestNotifications());

  Future<NotificationState> blocSendNotification(SendNotification event) async {
    try {
      if (await sendLocalNotification(
        title: event.title,
        message: event.message,
        payload: event.payload,
      )) {
        return NotificationSent(
            type: event.type, payload: event.payload.toString());
      }
      return ErrorSendingNotification();
    } catch (e) {
      return ErrorSendingNotification(message: e.toString());
    }
  }

  Future<NotificationState> blocInitializeFirebase() async {
    try {
      await setupFirebase();
      return FirebaseInitialized();
    } catch (e) {
      return ErrorInitializingFirebase(message: e.toString());
    }
  }

  Future<NotificationState> blocReceiveNotification(
      ReceiveNotification event) async {
    try {
      if (await validateToken() != null) {
        final data = jsonDecode(event.payload);
        switch (data["type"]) {
          case "GetSuggestPatientCarerEvent":
            return GetSuggestPatientCarerNotificationReceived(
                id: data["contractId"]);
          case "SelectPatientCarerEvent":
            return SuggestedPatientCarerNotificationReceived(
                contractId: data["contractId"],
                patientCarer:
                    PatientCarer.fromJson(data["matchedPatientCarer"]));
        }
      }
      return NotificationReceived(payload: event.payload);
    } catch (e) {
      print(e.toString());
      return NotificationReceived(payload: event.payload);
    }
  }

  Future<NotificationState> blocGetLatestNotifications(
      GetLatestNotifications event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForNotification();
        }
        final data = await NotificationService.getLatestNotifications(token);
        if (data != null) {
          return LatestNotificationsGotten(messages: data);
        }
        return ErrorGettingLatestNotifications();
      } catch (e) {
        return ErrorGettingLatestNotifications(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForNotifications();
    }
  }

  Future<NotificationState> blocSendFirebaseToken(
      SendFirebaseToken event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForNotification();
        }
        final firebaseToken = await FirebaseMessaging.instance.getToken();
        final success = await sendFirebaseTokenToServer(token,
            firebaseToken: firebaseToken);
        if (success) {
          return FirebaseTokenSent();
        }
        return ErrorSendingFirebaseToken();
      } catch (e) {
        return ErrorSendingFirebaseToken(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForNotifications();
    }
  }

  // Future<NotificationState> blocDeleteFirebaseToken(
  //     SendFirebaseToken event) async {
  //   if (await ConnectionHelper.hasConnection()) {
  //     try {
  //       final token = await validateToken();
  //       if (token == null) {
  //         return TokenExpireForNotification();
  //       }
  //       final userId = await LocalCache.getSecured(USER_ID_KEY);
  //       final success =
  //           await deleteFirebaseTokenFromServer(token, userId: userId);
  //       if (success) {
  //         return FirebaseTokenSent();
  //       }
  //       return ErrorSendingFirebaseToken();
  //     } catch (e) {
  //       return ErrorSendingFirebaseToken(message: getErrorMessage(e));
  //     }
  //   } else {
  //     return ConnectionErrorForNotifications();
  //   }
  // }

  @override
  Stream<NotificationState> mapEventToState(
    NotificationEvent event,
  ) async* {
    if (event is SendNotification) {
      yield SendingNotification();
      yield await blocSendNotification(event);
    } else if (event is InitializeFirebase) {
      yield InitializingFirebase();
      yield await blocInitializeFirebase();
    } else if (event is ReceiveNotification) {
      yield await blocReceiveNotification(event);
      yield NotificationInitial();
    } else if (event is RemoveLastNotification) {
      flutterLocalNotificationsPlugin.cancel(0);
    } else if (event is GetLatestNotifications) {
      yield GettingLatestNotifications();
      yield await blocGetLatestNotifications(event);
    } else if (event is SendFirebaseToken) {
      yield SendingFirebaseToken();
      yield await blocSendFirebaseToken(event);
    }
  }
}
