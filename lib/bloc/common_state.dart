import 'dart:convert';

import 'package:dio/dio.dart';

abstract class TokenExpired {}

abstract class ConnectionErrorState {
  String get message => "Mất kết nối mạng";
}

abstract class ErrorState {
  final String messageValue;
  ErrorState([this.messageValue = ""]);

  String get message => messageValue ?? "Đã có lỗi xảy ra";
}

abstract class TokenExpire {
  TokenExpire();
  String get message => "Phiên đăng nhập hết hạn.";
}

String getErrorMessage(Exception e) {
  String errorMessage;
  try {
    if (e is DioError) {
      try {
        switch (e.response.statusCode) {
          case 400:
            final data = e.response.data["errors"] as Map<String, dynamic>;
            List<String> messageList = [];
            data.forEach((key, value) => messageList.add("$key: $value"));
            errorMessage = messageList.join("\n");
            break;
          case 404:
          case 500:
            final data = jsonDecode(e.response.data);
            errorMessage = data["detail"].toString();
            break;
          default:
            errorMessage = e.response.data.toString();
        }
      } catch (insideError) {
        errorMessage = e.response.data.toString();
      }
    } else {
      errorMessage = e.toString();
    }
  } catch (error) {
    errorMessage = e.toString();
  }
  return errorMessage;
}
