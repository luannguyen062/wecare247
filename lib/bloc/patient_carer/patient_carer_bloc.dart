import 'dart:async';

import 'package:wecare247/bloc/common_state.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/services/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'patient_carer_event.dart';
part 'patient_carer_state.dart';

class PatientCarerBloc extends Bloc<PatientCarerEvent, PatientCarerState> {
  PatientCarerBloc() : super(PatientCarerInitial());

  void createPatientCarer({PatientCarer patientCarer}) =>
      add(CreatePatientCarer(patientCarer: patientCarer));

  void editPatientCarer({PatientCarer patientCarer}) =>
      add(EditPatientCarer(patientCarer: patientCarer));

  void getPatientCarerList({PaginationModel model}) =>
      add(GetPatientCarerList(model: model));

  void getPatientCarer({String id}) => add(GetPatientCarer(id: id));

  Future<PatientCarerState> blocCreatePatientCarer(
      CreatePatientCarer event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForPatientCarer();
        }
        final success = await PatientCarerService.createPatientCarer(token,
            patientCarer: event.patientCarer);
        if (success) {
          return PatientCarerCreated();
        }
        return ErrorCreatingPatientCarer();
      } catch (e) {
        return ErrorCreatingPatientCarer(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForPatientCarer();
    }
  }

  Future<PatientCarerState> blocEditPatientCarer(EditPatientCarer event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForPatientCarer();
        }
        final success = await PatientCarerService.editPatientCarer(token,
            patientCarer: event.patientCarer);
        if (success) {
          return PatientCarerEdited();
        }
        return ErrorEditingPatientCarer();
      } catch (e) {
        return ErrorEditingPatientCarer(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForPatientCarer();
    }
  }

  Future<PatientCarerState> blocGetPatientCarerList(
      GetPatientCarerList event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForPatientCarer();
        }
        final data = await PatientCarerService.getPatientCarerList(token,
            params: event.model);
        if (data != null) {
          return PatientCarerListGotten(patientCarers: data);
        }
        return ErrorGettingPatientCarerList();
      } catch (e) {
        return ErrorGettingPatientCarerList(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForPatientCarer();
    }
  }

  Future<PatientCarerState> blocGetPatientCarer(GetPatientCarer event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForPatientCarer();
        }
        final data =
            await PatientCarerService.getPatientCarer(token, id: event.id);
        if (data != null) {
          return PatientCarerGotten(patientCarer: data);
        }
        return ErrorGettingPatientCarer();
      } catch (e) {
        return ErrorGettingPatientCarer(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForPatientCarer();
    }
  }

  @override
  Stream<PatientCarerState> mapEventToState(
    PatientCarerEvent event,
  ) async* {
    if (event is CreatePatientCarer) {
      yield CreatingPatientCarer();
      yield await blocCreatePatientCarer(event);
    } else if (event is EditPatientCarer) {
      yield EditingPatientCarer();
      yield await blocEditPatientCarer(event);
    } else if (event is GetPatientCarerList) {
      yield GettingPatientCarerList();
      yield await blocGetPatientCarerList(event);
    } else if (event is GetPatientCarer) {
      yield GettingPatientCarer();
      yield await blocGetPatientCarer(event);
    }
  }
}
