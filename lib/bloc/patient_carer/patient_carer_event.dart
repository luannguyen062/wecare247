part of 'patient_carer_bloc.dart';

@immutable
abstract class PatientCarerEvent {}

class CreatePatientCarer extends PatientCarerEvent {
  final PatientCarer patientCarer;
  CreatePatientCarer({this.patientCarer});
}

class EditPatientCarer extends PatientCarerEvent {
  final PatientCarer patientCarer;
  EditPatientCarer({this.patientCarer});
}

class GetPatientCarerList extends PatientCarerEvent {
  final String desc;
  final String orderBy;
  final PaginationModel model;
  GetPatientCarerList({this.desc, this.orderBy, this.model});
}

class GetPatientCarer extends PatientCarerEvent {
  final String id;
  GetPatientCarer({this.id});
}
