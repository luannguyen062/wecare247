part of 'patient_carer_bloc.dart';

@immutable
abstract class PatientCarerState {}

class PatientCarerInitial extends PatientCarerState {}

class CreatingPatientCarer extends PatientCarerState {}

class PatientCarerCreated extends PatientCarerState {}

class ErrorCreatingPatientCarer extends ErrorState with PatientCarerState {
  ErrorCreatingPatientCarer({String message}) : super(message);
  String get message => super.message;
}

class EditingPatientCarer extends PatientCarerState {}

class PatientCarerEdited extends PatientCarerState {}

class ErrorEditingPatientCarer extends ErrorState with PatientCarerState {
  ErrorEditingPatientCarer({String message}) : super(message);
  String get message => super.message;
}

class GettingPatientCarerList extends PatientCarerState {}

class PatientCarerListGotten extends PatientCarerState {
  final PaginationModel<PatientCarer> patientCarers;
  PatientCarerListGotten({this.patientCarers});
}

class ErrorGettingPatientCarerList extends ErrorState with PatientCarerState {
  ErrorGettingPatientCarerList({String message}) : super(message);
  String get message => super.message;
}

class GettingPatientCarer extends PatientCarerState {}

class PatientCarerGotten extends PatientCarerState {
  final PatientCarer patientCarer;
  PatientCarerGotten({this.patientCarer});
}

class ErrorGettingPatientCarer extends ErrorState with PatientCarerState {
  ErrorGettingPatientCarer({String message}) : super(message);
  String get message => super.message;
}

class ConnectionErrorForPatientCarer extends ConnectionErrorState
    with PatientCarerState {}

class TokenExpireForPatientCarer extends TokenExpire with PatientCarerState {}
