part of 'data_bloc.dart';

@immutable
abstract class DataState {}

class DataInitial extends DataState {}

class GettingPatientCarerSkill extends DataState {}

class PatientCarerSkillGotten extends DataState {
  final List<PatientCarerSkill> skills;
  final String groupId;
  PatientCarerSkillGotten({this.skills, this.groupId});
}

class ErrorGettingPatientCarerSkill extends ErrorState with DataState {
  ErrorGettingPatientCarerSkill({String message}) : super(message);
  String get message => super.message;
}

class GettingProvinceList extends DataState {}

class ProvinceListGotten extends DataState {
  final List<Province> provinceList;
  ProvinceListGotten({this.provinceList});
}

class ErrorGettingProvinceList extends ErrorState with DataState {
  ErrorGettingProvinceList({String message}) : super(message);
  String get message => super.message;
}

class GettingDistricstList extends DataState {}

class DistrictListGotten extends DataState {
  final List<District> districtList;
  DistrictListGotten({this.districtList});
}

class ErrorGettingDistrictList extends ErrorState with DataState {
  ErrorGettingDistrictList({String message}) : super(message);
  String get message => super.message;
}

class GettingWardList extends DataState {}

class WardListGotten extends DataState {
  final List<Ward> wardList;
  WardListGotten({this.wardList});
}

class ErrorGettingWardList extends ErrorState with DataState {
  ErrorGettingWardList({String message}) : super(message);
  String get message => super.message;
}

class GettingLocationList extends DataState {}

class LocationListGotten extends DataState {
  final List<Location> locationList;
  LocationListGotten({this.locationList});
}

class ErrorGettingLocationList extends ErrorState with DataState {
  ErrorGettingLocationList({String message}) : super(message);
  String get message => super.message;
}

class GettingPatientCarerLevels extends DataState {}

class PatientCarerLevelsGotten extends DataState {
  final List<PatientCarerLevel> levelList;
  PatientCarerLevelsGotten({this.levelList});
}

class ErrorGettingPatientCarerLevels extends ErrorState with DataState {
  ErrorGettingPatientCarerLevels({String message}) : super(message);
  String get message => super.message;
}

class GettingServiceList extends DataState {}

class ServiceListGotten extends DataState {
  final List<Service> serviceList;
  ServiceListGotten({this.serviceList});
}

class ErrorGettingServiceList extends ErrorState with DataState {
  ErrorGettingServiceList({String message}) : super(message);
  String get message => super.message;
}

class GettingLocationDepartmentList extends DataState {}

class LocationDepartmentListGotten extends DataState {
  final List<LocationDepartment> departmentList;
  LocationDepartmentListGotten({this.departmentList});
}

class ErrorGettingLocationDepartmentList extends ErrorState with DataState {
  ErrorGettingLocationDepartmentList({String message}) : super(message);
  String get message => super.message;
}

class GettingContractSourceList extends DataState {}

class ContractSourceListGotten extends DataState {
  final List<ContractSource> contractSourceList;
  ContractSourceListGotten({this.contractSourceList});
}

class ErrorGettingContractSourceList extends ErrorState with DataState {
  ErrorGettingContractSourceList({String message}) : super(message);
  String get message => super.message;
}

class ConnectionErrorForData extends ConnectionErrorState with DataState {}

class TokenExpireForData extends TokenExpire with DataState {}
