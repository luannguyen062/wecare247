part of 'data_bloc.dart';

@immutable
abstract class DataEvent {}

class GetPatientCarerSkill extends DataEvent {
  final String groupId;
  GetPatientCarerSkill({this.groupId});
}

class GetProvinceList extends DataEvent {}

class GetDistrictList extends DataEvent {
  final String provinceId;
  GetDistrictList({this.provinceId});
}

class GetWardList extends DataEvent {
  final String districtId;
  GetWardList({this.districtId});
}

class GetLocationList extends DataEvent {}

class GetServiceList extends DataEvent {}

class GetPatientCarerLevels extends DataEvent {}

class GetLocationDepartmentList extends DataEvent {
  final String locationId;
  GetLocationDepartmentList({this.locationId});
}

class GetContractSourceList extends DataEvent {}
