import 'dart:async';

import 'package:wecare247/bloc/common_state.dart';
import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/services/data_service.dart';
import 'package:wecare247/shared/index.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'data_event.dart';
part 'data_state.dart';

class DataBloc extends Bloc<DataEvent, DataState> {
  DataBloc() : super(DataInitial());

  void getPatientCarerSkill() => add(GetPatientCarerSkill());

  void getServiceList() => add(GetServiceList());

  void getProvinceList() => add(GetProvinceList());

  void getDistrictList({String provinceId}) =>
      add(GetDistrictList(provinceId: provinceId));

  void getWardList({String districtId}) =>
      add(GetWardList(districtId: districtId));

  void getLocationList() => add(GetLocationList());

  void getPatientCarerLevels() => add(GetPatientCarerLevels());

  void getLocationDepartmentList({String locationId}) =>
      add(GetLocationDepartmentList(locationId: locationId));

  void getContractSourceList() => add(GetContractSourceList());

  Future<DataState> blocGetPatientCarerSkill(GetPatientCarerSkill event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForData();
        }
        final data = await DataService.getPatientCarerSkills(token);
        if (data != null) {
          return PatientCarerSkillGotten(skills: data, groupId: event.groupId);
        }
        return ErrorGettingPatientCarerSkill();
      } catch (e) {
        return ErrorGettingPatientCarerSkill(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForData();
    }
  }

  Future<DataState> blocGetProvinceList(GetProvinceList event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForData();
        }
        final List<Province> provinceList =
            await DataService.getProvinceList(token);
        if (provinceList != null) {
          return ProvinceListGotten(provinceList: provinceList);
        }
        return ErrorGettingProvinceList();
      } catch (e) {
        return ErrorGettingProvinceList(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForData();
    }
  }

  Future<DataState> blocGetDistrictList(GetDistrictList event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForData();
        }
        final List<District> districtList = await DataService.getDistrictList(
            token,
            provinceId: event.provinceId);
        if (districtList != null) {
          return DistrictListGotten(districtList: districtList);
        }
        return ErrorGettingDistrictList();
      } catch (e) {
        return ErrorGettingDistrictList(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForData();
    }
  }

  Future<DataState> blocGetWardList(GetWardList event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForData();
        }
        final List<Ward> wardList =
            await DataService.getWardList(token, districtId: event.districtId);
        if (wardList != null) {
          return WardListGotten(wardList: wardList);
        }
        return ErrorGettingWardList();
      } catch (e) {
        return ErrorGettingWardList(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForData();
    }
  }

  Future<DataState> blocGetServiceList() async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForData();
        }
        final List<Service> serviceList =
            await DataService.getServiceList(token);
        if (serviceList != null) {
          return ServiceListGotten(serviceList: serviceList);
        }
        return ErrorGettingServiceList();
      } catch (e) {
        return ErrorGettingServiceList(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForData();
    }
  }

  Future<DataState> blocGetLocationList() async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForData();
        }
        final List<Location> locationList =
            await DataService.getLocation(token);
        if (locationList != null) {
          return LocationListGotten(locationList: locationList);
        }
        return ErrorGettingLocationList();
      } catch (e) {
        return ErrorGettingLocationList(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForData();
    }
  }

  Future<DataState> blocGetPatientCarerLevels(
      GetPatientCarerLevels event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForData();
        }
        final List<PatientCarerLevel> levelList =
            await DataService.getPatientCarerLevels(token);
        if (levelList != null) {
          return PatientCarerLevelsGotten(levelList: levelList);
        }
        return ErrorGettingPatientCarerLevels();
      } catch (e) {
        return ErrorGettingPatientCarerLevels(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForData();
    }
  }

  Future<DataState> blocGetLocationDepartmentList(
      GetLocationDepartmentList event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForData();
        }
        final List<LocationDepartment> departmentList =
            await DataService.getLocationDeparmentList(token,
                locationId: event.locationId);
        if (departmentList != null) {
          return LocationDepartmentListGotten(departmentList: departmentList);
        }
        return ErrorGettingLocationDepartmentList();
      } catch (e) {
        return ErrorGettingLocationDepartmentList(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForData();
    }
  }

  Future<DataState> blocGetContractSourceList() async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForData();
        }
        final List<ContractSource> contractSourceList =
            await DataService.getContractSourceList(token);
        if (contractSourceList != null) {
          return ContractSourceListGotten(
              contractSourceList: contractSourceList);
        }
        return ErrorGettingContractSourceList();
      } catch (e) {
        return ErrorGettingContractSourceList(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForData();
    }
  }

  @override
  Stream<DataState> mapEventToState(
    DataEvent event,
  ) async* {
    if (event is GetPatientCarerSkill) {
      yield GettingPatientCarerSkill();
      yield await blocGetPatientCarerSkill(event);
    } else if (event is GetProvinceList) {
      yield GettingProvinceList();
      yield await blocGetProvinceList(event);
    } else if (event is GetDistrictList) {
      yield GettingDistricstList();
      yield await blocGetDistrictList(event);
    } else if (event is GetWardList) {
      yield GettingWardList();
      yield await blocGetWardList(event);
    } else if (event is GetLocationList) {
      yield GettingLocationList();
      yield await blocGetLocationList();
    } else if (event is GetPatientCarerLevels) {
      yield GettingPatientCarerLevels();
      yield await blocGetPatientCarerLevels(event);
    } else if (event is GetServiceList) {
      yield GettingServiceList();
      yield await blocGetServiceList();
    } else if (event is GetLocationDepartmentList) {
      yield GettingLocationDepartmentList();
      yield await blocGetLocationDepartmentList(event);
    } else if (event is GetContractSourceList) {
      yield GettingContractSourceList();
      yield await blocGetContractSourceList();
    }
  }
}
