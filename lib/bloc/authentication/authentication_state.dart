part of 'authentication_bloc.dart';

@immutable
abstract class AuthenticationState {}

class AuthenticationInitial extends AuthenticationState {}

class AuthenticatingUser extends AuthenticationState {}

class UserAuthenticated extends AuthenticationState {}

class ErrorAuthenticatingUser extends ErrorState with AuthenticationState {
  ErrorAuthenticatingUser({String message}) : super(message);
  String get message => super.message;
}

class ConnectionErrorForAuthentication extends ConnectionErrorState
    with AuthenticationState {}

class LoggingOut extends AuthenticationState {}

class UserLoggedOut extends AuthenticationState {}

class ErrorLoggingOut extends ErrorState with AuthenticationState {
  ErrorLoggingOut({String message}) : super(message);
  String get message => super.message;
}
