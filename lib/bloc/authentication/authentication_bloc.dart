import 'dart:async';

import 'package:wecare247/bloc/common_state.dart';
import 'package:wecare247/services/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:meta/meta.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc() : super(AuthenticationInitial());

  void authenticateUser({String username, String password}) =>
      add(AuthenticateUser(username: username, password: password));

  void logOut() => add(LogOutUser());

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AuthenticateUser) {
      yield AuthenticatingUser();
      yield await blocAuthenticateUSer(event);
    } else if (event is LogOutUser) {
      yield LoggingOut();
      yield await blocLogOut();
    }
  }

  Future<AuthenticationState> blocAuthenticateUSer(
      AuthenticateUser event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final data = await AuthenticationService.authenticateUser(
            username: event.username, password: event.password);
        if (data == null) {
          return ErrorAuthenticatingUser();
        }
        final realData = data["data"];
        Map<String, dynamic> decodedToken =
            JwtDecoder.decode(realData["accessToken"]);
        if (!['NVKD', 'QTV'].contains(decodedToken["role"])) {
          return ErrorAuthenticatingUser(
              message:
                  "Tài khoản của bạn không có quyền hạn sử dụng ứng dụng này.");
        }
        LocalCache.putSecured(USER_ACCESS_TOKEN_KEY, realData["accessToken"]);
        LocalCache.putSecured(USER_REFRESH_TOKEN_KEY, realData["refreshToken"]);
        LocalCache.putSecured(USER_NAME_KEY, decodedToken["nameid"]);
        LocalCache.putSecured(USER_ID_KEY, decodedToken["sub"]);
        LocalCache.putSecured(USER_ROLE_KEY, decodedToken["role"]);
        DateTime currentTime = DateTime.now().toUtc();
        DateTime expireTime =
            currentTime.add(Duration(seconds: realData["expiredIn"]));
        // DateTime expireTime = currentTime.add(Duration(seconds: 30));
        LocalCache.putSecured(
            USER_TOKEN_EXPIRE_KEY, expireTime.toUtc().toIso8601String());
        return UserAuthenticated();
      } catch (e) {
        return ErrorAuthenticatingUser(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForAuthentication();
    }
  }

  Future<AuthenticationState> blocLogOut() async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        await Future.wait([
          LocalCache.deleteSecured(USER_ACCESS_TOKEN_KEY),
          LocalCache.deleteSecured(USER_REFRESH_TOKEN_KEY),
          LocalCache.deleteSecured(USER_NAME_KEY),
          LocalCache.deleteSecured(USER_TOKEN_EXPIRE_KEY),
          LocalCache.deleteSecured(USER_ROLE_KEY),
        ]);
        return UserLoggedOut();
      } catch (e) {
        return ErrorLoggingOut(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForAuthentication();
    }
  }
}
