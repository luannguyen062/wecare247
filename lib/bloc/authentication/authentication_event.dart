part of 'authentication_bloc.dart';

@immutable
abstract class AuthenticationEvent {}

class AuthenticateUser extends AuthenticationEvent {
  final String username;
  final String password;
  AuthenticateUser({this.password, this.username});
}

class LogOutUser extends AuthenticationEvent {}
