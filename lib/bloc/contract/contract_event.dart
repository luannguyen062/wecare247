part of 'contract_bloc.dart';

@immutable
abstract class ContractEvent {}

class CreateContract extends ContractEvent {
  final CreateContractDto dto;
  CreateContract({this.dto});
}

class GetContract extends ContractEvent {
  final GetContractDto dto;
  GetContract({this.dto});
}

class GetContractSpecific extends ContractEvent {
  final String id;
  GetContractSpecific({this.id});
}

class GetSuggestPatientCarerEvent extends ContractEvent {
  final String id;
  final DateTime from;
  final DateTime to;
  GetSuggestPatientCarerEvent({this.id, this.from, this.to});
}

class SelectPatientCarer extends ContractEvent {
  final SelectPatientCarerDto dto;
  SelectPatientCarer({this.dto});
}

class PingMonitor extends ContractEvent {
  final String contractId;
  PingMonitor({this.contractId});
}

class ConfirmPatientCarer extends ContractEvent {
  final String contractId;
  final String patientCarerId;
  ConfirmPatientCarer({this.contractId, this.patientCarerId});
}

class EditContract extends ContractEvent {
  final EditContractDto dto;
  EditContract({this.dto});
}

class CreateNewService extends ContractEvent {
  final ServiceEditData data;
  CreateNewService({this.data});
}

class EditService extends ContractEvent {
  final ServiceEditData data;
  final String id;
  EditService({this.data, this.id});
}

class EditCustomerInfo extends ContractEvent {
  final EditCustomerInfoDto dto;
  EditCustomerInfo({this.dto});
}

class GetTypeaheadCustomer extends ContractEvent {
  final String contactPhone;
  GetTypeaheadCustomer({this.contactPhone});
}
