import 'dart:async';

import 'package:wecare247/bloc/common_state.dart';
import 'package:wecare247/bloc/index.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/services/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'contract_event.dart';
part 'contract_state.dart';

class ContractBloc extends Bloc<ContractEvent, ContractState> {
  ContractBloc() : super(ContractInitial());

  void createContract({CreateContractDto dto}) => add(CreateContract(dto: dto));

  void getContract({GetContractDto dto}) => add(GetContract(dto: dto));

  void pingMonitor({String contractId}) =>
      add(PingMonitor(contractId: contractId));
  void confirmPatientCarer({String contractId, String patientCarerId}) =>
      add(ConfirmPatientCarer(
          contractId: contractId, patientCarerId: patientCarerId));

  void getSuggestedPatientCarer({String id, DateTime from, DateTime to}) =>
      add(GetSuggestPatientCarerEvent(id: id, from: from, to: to));

  void selectPatientCarer({SelectPatientCarerDto dto}) =>
      add(SelectPatientCarer(dto: dto));

  void getContractSpecific({String id}) => add(GetContractSpecific(id: id));

  void editContract({EditContractDto dto}) => add(EditContract(dto: dto));

  void createNewService({ServiceEditData data}) =>
      add(CreateNewService(data: data));

  void editService({ServiceEditData data, String id}) =>
      add(EditService(data: data, id: id));

  void editCustomerInfo({EditCustomerInfoDto dto}) =>
      add(EditCustomerInfo(dto: dto));

  void getTypeaheadCustomer({String contactPhone}) =>
      add(GetTypeaheadCustomer(contactPhone: contactPhone));

  Future<ContractState> blocCreateContract(CreateContract event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForContract();
        }
        final data =
            await ContractService.createContract(token, dto: event.dto);
        if (data != null) {
          return ContractCreated(id: data);
        }
        return ErrorCreatingContract();
      } catch (e) {
        return ErrorCreatingContract(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForContract();
    }
  }

  Future<ContractState> blocGetContract(GetContract event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForContract();
        }
        final data = await ContractService.getContract(token, dto: event.dto);
        if (data != null) {
          return ContractGotten(model: data);
        }
        return ErrorGettingContract();
      } catch (e) {
        return ErrorGettingContract(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForContract();
    }
  }

  Future<ContractState> blocGetContractSpecific(
      GetContractSpecific event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForContract();
        }
        final data =
            await ContractService.getContractSpecific(token, id: event.id);
        if (data != null) {
          return SpecificContractGotten(contract: data);
        }
        return ErrorGettingContract();
      } catch (e) {
        return ErrorGettingContract(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForContract();
    }
  }

  Future<ContractState> blocGetSuggestPatientCarer(
      GetSuggestPatientCarerEvent event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForContract();
        }
        List<PatientCarer> data;
        data = await ContractService.getSuggestPatientCarer(
          token,
          id: event.id,
        );
        if (data != null) {
          return SuggestedPatientCarerGotten(patientCarerList: data);
        }
        return ErrorGettingSuggestedPatientCarer();
      } catch (e) {
        return ErrorGettingSuggestedPatientCarer(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForContract();
    }
  }

  Future<ContractState> blocGetSuggestPatientCarerSelectList(
      GetSuggestPatientCarerEvent event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForContract();
        }
        List<PatientCarerFromSelectList> data;
        data = await ContractService.getSuggestPatientCarerSelectList(token,
            id: event.id, from: event.from, to: event.to);
        if (data != null) {
          return SuggestedPatientCarerSelectListGotten(patientCarerList: data);
        }
        return ErrorGettingSuggestedPatientCarer();
      } catch (e) {
        return ErrorGettingSuggestedPatientCarer(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForContract();
    }
  }

  Future<ContractState> blocSelectPatientCarer(SelectPatientCarer event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForContract();
        }
        final data =
            await ContractService.selectPatientCarer(token, dto: event.dto);
        if (data != null && data) {
          return PatientCarerSelected();
        }
        return ErrorSelectingPatientCarer();
      } catch (e) {
        return ErrorSelectingPatientCarer(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForContract();
    }
  }

  Future<ContractState> blocPingMonitor(PingMonitor event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForContract();
        }
        final data =
            await ContractService.pingMonitor(token, id: event.contractId);
        if (data != null) {
          return MonitorPinged();
        }
        return ErrorPingingMonitor();
      } catch (e) {
        return ErrorPingingMonitor(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForContract();
    }
  }

  Future<ContractState> blocConfirmPatientCarer(
      ConfirmPatientCarer event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForContract();
        }
        final data = await ContractService.confirmPatientCarer(token,
            contractId: event.contractId, patientCarerId: event.patientCarerId);
        if (data != null) {
          return PatientCarerConfirmed();
        }
        return ErrorConfirmingPatientCarer();
      } catch (e) {
        return ErrorConfirmingPatientCarer(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForContract();
    }
  }

  Future<ContractState> blocEditContract(EditContract event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForContract();
        }
        final data = await ContractService.editContract(token, dto: event.dto);
        if (data != null) {
          return ContractEdited();
        }
        return ErrorEditingContract();
      } catch (e) {
        return ErrorEditingContract(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForContract();
    }
  }

  Future<ContractState> blocCreateNewService(CreateNewService event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForContract();
        }
        final data =
            await ContractService.createNewService(token, data: event.data);
        if (data != null) {
          return NewServiceCreated();
        }
        return ErrorCreatingNewService();
      } catch (e) {
        return ErrorCreatingNewService(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForContract();
    }
  }

  Future<ContractState> blocEditService(EditService event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForContract();
        }
        final data = await ContractService.editService(token,
            data: event.data, id: event.id);
        if (data != null) {
          return ServiceEdited();
        }
        return ErrorEditingService();
      } catch (e) {
        return ErrorEditingService(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForContract();
    }
  }

  Future<ContractState> blocEditCustomerInfo(EditCustomerInfo event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForContract();
        }
        final success =
            await CustomerService.editCustomerInfo(token, dto: event.dto);
        if (success) {
          return CustomerInfoEdited();
        }
        return ErrorEditingCustomerInfo();
      } catch (e) {
        return ErrorEditingCustomerInfo(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForContract();
    }
  }

  Future<ContractState> blocGetTypeaheadCustomer(
      GetTypeaheadCustomer event) async {
    if (await ConnectionHelper.hasConnection()) {
      try {
        final token = await validateToken();
        if (token == null) {
          return TokenExpireForContract();
        }
        final customer = await CustomerService.getTypeaheadCustomer(token,
            contactPhone: event.contactPhone);
        if (customer != null) {
          return TypeaheadCustomerGotten(customer: customer);
        }
        return ErrorGettingTypeaheadCustomer();
      } catch (e) {
        return ErrorGettingTypeaheadCustomer(message: getErrorMessage(e));
      }
    } else {
      return ConnectionErrorForContract();
    }
  }

  @override
  Stream<ContractState> mapEventToState(
    ContractEvent event,
  ) async* {
    if (event is CreateContract) {
      yield CreatingContract();
      yield await blocCreateContract(event);
    } else if (event is GetContract) {
      yield GettingContract();
      yield await blocGetContract(event);
    } else if (event is GetContractSpecific) {
      yield GettingContract();
      yield await blocGetContractSpecific(event);
    } else if (event is GetSuggestPatientCarerEvent) {
      yield GettingSuggestedPatientCarer();
      if (event.from != null && event.to != null) {
        yield await blocGetSuggestPatientCarerSelectList(event);
      } else {
        yield await blocGetSuggestPatientCarer(event);
      }
    } else if (event is SelectPatientCarer) {
      yield SelectingPatientCarer();
      yield await blocSelectPatientCarer(event);
    } else if (event is PingMonitor) {
      yield PingingMonitor();
      yield await blocPingMonitor(event);
    } else if (event is ConfirmPatientCarer) {
      yield ConfirmingPatientCarer();
      yield await blocConfirmPatientCarer(event);
    } else if (event is EditContract) {
      yield EditingContract();
      yield await blocEditContract(event);
    } else if (event is CreateNewService) {
      yield CreatingNewService();
      yield await blocCreateNewService(event);
    } else if (event is EditService) {
      yield EditingService();
      yield await blocEditService(event);
    } else if (event is EditCustomerInfo) {
      yield EditingCustomerInfo();
      yield await blocEditCustomerInfo(event);
    } else if (event is GetTypeaheadCustomer) {
      yield GettingTypeaheadCustomer();
      yield await blocGetTypeaheadCustomer(event);
    }
  }
}
