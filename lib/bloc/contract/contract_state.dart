part of 'contract_bloc.dart';

@immutable
abstract class ContractState {}

class ContractInitial extends ContractState {}

class CreatingContract extends ContractState {}

class ContractCreated extends ContractState {
  final String id;
  ContractCreated({this.id});
}

class ErrorCreatingContract extends ErrorState with ContractState {
  ErrorCreatingContract({String message}) : super(message);
  String get message => super.message;
}

class GettingContract extends ContractState {}

class ContractGotten extends ContractState {
  final PaginationModel<ContractShort> model;
  ContractGotten({this.model});
}

class SpecificContractGotten extends ContractState {
  final ContractDetail contract;
  SpecificContractGotten({this.contract});
}

class ErrorGettingContract extends ErrorState with ContractState {
  ErrorGettingContract({String message}) : super(message);
  String get message => super.message;
}

class ConnectionErrorForContract extends ConnectionErrorState
    with ContractState {}

class GettingSuggestedPatientCarer extends ContractState {}

class SuggestedPatientCarerGotten extends ContractState {
  final List<PatientCarer> patientCarerList;
  SuggestedPatientCarerGotten({this.patientCarerList});
}

class ErrorGettingSuggestedPatientCarer extends ErrorState with ContractState {
  ErrorGettingSuggestedPatientCarer({String message}) : super(message);
  String get message => super.message;
}

class GettingSuggestedPatientCarerSelectList extends ContractState {}

class SuggestedPatientCarerSelectListGotten extends ContractState {
  final List<PatientCarerFromSelectList> patientCarerList;
  SuggestedPatientCarerSelectListGotten({this.patientCarerList});
}

class ErrorSuggestedPatientCarerSelectListGotten extends ErrorState
    with ContractState {
  ErrorSuggestedPatientCarerSelectListGotten({String message}) : super(message);
  String get message => super.message;
}

class SelectingPatientCarer extends ContractState {}

class PatientCarerSelected extends ContractState {}

class ErrorSelectingPatientCarer extends ErrorState with ContractState {
  ErrorSelectingPatientCarer({String message}) : super(message);
  String get message => super.message;
}

class PingingMonitor extends ContractState {}

class MonitorPinged extends ContractState {}

class ErrorPingingMonitor extends ErrorState with ContractState {
  ErrorPingingMonitor({String message}) : super(message);
  String get message => super.message;
}

class ConfirmingPatientCarer extends ContractState {}

class PatientCarerConfirmed extends ContractState {}

class ErrorConfirmingPatientCarer extends ErrorState with ContractState {
  ErrorConfirmingPatientCarer({String message}) : super(message);
  String get message => super.message;
}

class EditingContract extends ContractState {}

class ContractEdited extends ContractState {}

class ErrorEditingContract extends ErrorState with ContractState {
  ErrorEditingContract({String message}) : super(message);
  String get message => super.message;
}

class CreatingNewService extends ContractState {}

class NewServiceCreated extends ContractState {}

class ErrorCreatingNewService extends ErrorState with ContractState {
  ErrorCreatingNewService({String message}) : super(message);
  String get message => super.message;
}

class EditingService extends ContractState {}

class ServiceEdited extends ContractState {}

class ErrorEditingService extends ErrorState with ContractState {
  ErrorEditingService({String message}) : super(message);
  String get message => super.message;
}

class EditingCustomerInfo extends ContractState {}

class CustomerInfoEdited extends ContractState {}

class ErrorEditingCustomerInfo extends ErrorState with ContractState {
  ErrorEditingCustomerInfo({String message}) : super(message);
  String get message => super.message;
}

class GettingTypeaheadCustomer extends ContractState {}

class TypeaheadCustomerGotten extends ContractState {
  final CustomerDetail customer;
  TypeaheadCustomerGotten({this.customer});
}

class ErrorGettingTypeaheadCustomer extends ErrorState with ContractState {
  ErrorGettingTypeaheadCustomer({String message}) : super(message);
  String get message => super.message;
}

class TokenExpireForContract extends TokenExpire with ContractState {}
