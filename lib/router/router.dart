import 'package:wecare247/screens/index.dart';
import 'package:flutter/material.dart';

class AppRouter {
  static String routeName;
  static MaterialPageRoute navigateTo(Widget destination) {
    return MaterialPageRoute(
        settings: RouteSettings(name: routeName),
        builder: (context) => destination);
  }

  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments as Map<String, dynamic> ?? {};
    routeName = settings.name;
    switch (settings.name) {
      case "contract_list":
        return navigateTo(ContractList());
      case "create_edit_contract":
        return navigateTo(CreateEditContractScreen());
      case "contract_detail":
        return navigateTo(ContractDetailScreen(
          id: args["id"],
        ));
      case "new_contract_list":
        return navigateTo(NewContractList());
      case "home":
        return navigateTo(HomeScreen(
          username: args["username"],
        ));
      case "login":
        return navigateTo(LoginScreen());
      case "base":
      default:
        return navigateTo(BaseScreen());
    }
  }
}
