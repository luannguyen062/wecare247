import 'package:wecare247/models/index.dart';

class CustomerDetail {
  final String fullName;
  final String contactPhone;
  final String street;
  final String avatar;
  final String note;
  final int gender;
  final String id;
  final Ward ward;
  final District district;
  final Province province;
  CustomerDetail(
      {this.fullName,
      this.contactPhone,
      this.street,
      this.avatar,
      this.note,
      this.gender,
      this.id,
      this.ward,
      this.district,
      this.province});

  CustomerDetail.fromJson(Map<String, dynamic> data)
      : fullName = data["fullName"],
        contactPhone = data["contactPhone"],
        street = data["street"],
        avatar = data["avatar"],
        note = data["note"],
        gender = data["gender"],
        id = data["id"],
        ward = data["ward"] != null ? Ward.fromJson(data["ward"]) : null,
        district = data["district"] != null
            ? District.fromJson(data["district"])
            : null,
        province = data["province"] != null
            ? Province.fromJson(data["province"])
            : null;
}

class PatientDetail {
  final String fullName;
  final int gender;
  final int yearOfBirth;
  final double weight;
  final String id;
  PatientDetail.fromJson(Map<String, dynamic> data)
      : fullName = data["fullName"],
        gender = data["gender"],
        yearOfBirth = data["yearOfBirth"],
        weight = data["weight"]?.toDouble(),
        id = data["id"];
}

class CreateCustomerDto {
  final String avatar;
  final String fullName;
  final String contactPhone;
  final String street;
  final String wardId;
  final String districtId;
  final String provinceId;
  final int gender;
  final String note;
  CreateCustomerDto(
      {this.fullName,
      this.avatar,
      this.contactPhone,
      this.wardId,
      this.districtId,
      this.street,
      this.provinceId,
      this.gender,
      this.note});
  Map<String, dynamic> toJson() {
    return {
      "avatar": "",
      "fullName": fullName,
      "contactPhone": contactPhone,
      "street": street,
      "wardId": wardId,
      "districtId": districtId,
      "provinceId": provinceId,
      "gender": gender,
      "note": note
    };
  }
}

class CreatePatientDto {
  final String fullName;
  final int gender;
  final int yearOfBirth;
  final double weight;
  CreatePatientDto({this.fullName, this.gender, this.yearOfBirth, this.weight});

  Map<String, dynamic> toJson() {
    return {
      "fullName": fullName,
      "gender": gender,
      "yearOfBirth": yearOfBirth,
      "weight": weight
    };
  }
}

class EditCustomerInfoDto {
  String id;
  String fullName;
  String contactPhone;
  String secondaryPhone;
  String wardId;
  String districtId;
  String provinceId;
  String street;
  String note;
  int gender;
  EditPatientDto patient;
  EditCustomerInfoDto(
      {this.id,
      this.fullName,
      this.contactPhone,
      this.secondaryPhone,
      this.wardId,
      this.districtId,
      this.provinceId,
      this.street,
      this.note,
      this.gender,
      this.patient});

  EditCustomerInfoDto.fromDetail(CustomerDetail detail)
      : id = detail.id,
        street = detail.street,
        fullName = detail.fullName,
        contactPhone = detail.contactPhone,
        secondaryPhone = detail.contactPhone,
        wardId = detail.ward.id,
        districtId = detail.district.id,
        provinceId = detail.province.id,
        gender = detail.gender;

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'fullName': fullName,
      'contactPhone': contactPhone,
      'secondaryPhone': secondaryPhone,
      'wardId': wardId,
      'districtId': districtId,
      'provinceId': provinceId,
      'street': street,
      'note': note,
      'gender': gender,
      'patient': patient.toJson()
    };
  }
}

class EditPatientDto {
  // final String createdBy;
  // final DateTime created;
  // final String modifiedBy;
  // final DateTime modified;
  // final bool isDeleted;
  final String id;
  final String fullName;
  final int gender;
  final int yearOFBirth;
  final double weight;
  EditPatientDto(
      {this.id,
      // this.createdBy,
      // this.created,
      // this.modifiedBy,
      // this.modified,
      // this.isDeleted,
      this.fullName,
      this.gender,
      this.yearOFBirth,
      this.weight});

  EditPatientDto.fromDetail(PatientDetail detail)
      : id = detail.id,
        fullName = detail.fullName,
        weight = detail.weight,
        yearOFBirth = detail.yearOfBirth,
        gender = detail.gender;

  Map<String, dynamic> toJson() {
    return {
      // 'createdBy': createdBy,
      // 'created': created.toIso8601String(),
      // 'modifiedBy': modifiedBy,
      // 'modified': modified.toIso8601String(),
      // 'isDeleted': isDeleted,
      'id': id,
      'fullName': fullName,
      'gender': gender,
      'yearOFBirth': yearOFBirth,
      'weight': weight,
    };
  }
}
