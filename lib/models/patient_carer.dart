import 'package:wecare247/models/index.dart';
import 'package:wecare247/shared/utils/index.dart';

class PatientCarerSkill {
  final String id;
  final String skillId;
  final String skillName;
  final String name;
  final String description;
  final String groupId;
  bool value;
  PatientCarerSkill(
      {this.skillId,
      this.id,
      this.description,
      this.name,
      this.skillName,
      this.groupId,
      this.value});

  PatientCarerSkill.fromSelectList(Map<String, dynamic> data)
      : id = data["value"],
        skillId = null,
        skillName = data["label"],
        description = data["label"],
        name = data["label"],
        groupId = data["extra"]["groupId"],
        value = false;

  PatientCarerSkill.fromJson(Map<String, dynamic> data)
      : id = data["id"],
        skillId = data["skillId"],
        skillName = data["skillName"],
        description = data["description"],
        name = data["name"],
        groupId = data["groupId"],
        value = data["value"];

  Map<String, dynamic> toJson() {
    return {
      "id": skillId ?? id,
      // "name": name,
      // "description": description,
      // "groupId": groupId,
      "value": true,
    };
  }

  @override
  bool operator ==(other) =>
      other is PatientCarerSkill &&
      (this.id == other.id ||
          this.id == other.skillId ||
          this.skillId == other.id);

  @override
  int get hashCode => id.hashCode;
}

class PatientCarerLevel {
  final String id;
  final String name;
  PatientCarerLevel({this.id, this.name});

  PatientCarerLevel.fromJson(Map<String, dynamic> data)
      : id = data["id"],
        name = data["name"];

  PatientCarerLevel.fromSelectList(Map<String, dynamic> data)
      : id = data["value"],
        name = data["label"];
}

class PatientCarerFromSelectList {
  final String id;
  final String fullName;
  final String level;
  final String avatar;
  final String contactPhone;
  PatientCarerFromSelectList.fromData(Map<String, dynamic> data)
      : id = data["value"],
        fullName = data["label"],
        level = data["extra"]["level"],
        avatar = data["extra"]["avatar"],
        contactPhone = data["extra"]["contactPhone"];
}

class PatientCarer {
  final String id;
  final String fullName;
  final DateTime birthday;
  final String cardNo;
  final DateTime cardDate;
  final String cardIssuer;
  final DateTime signingDate;
  final String contactPhone;
  final String contactAddress;
  final String permanentAddress;
  final String levelId;
  final String preferLocationId;
  final Location preferLocation;
  final String levelName;
  final PatientCarerLevel level;
  final int gender;
  final String cardPicture;
  final String avatar;
  final List<PatientCarerSkill> skills;
  final int matchedSkillsCount;
  PatientCarer(
      {this.cardDate,
      this.level,
      this.cardIssuer,
      this.cardNo,
      this.contactAddress,
      this.contactPhone,
      this.birthday,
      this.fullName,
      this.levelId,
      this.id,
      this.permanentAddress,
      this.preferLocationId,
      this.preferLocation,
      this.signingDate,
      this.levelName,
      this.skills,
      this.gender,
      this.avatar,
      this.cardPicture,
      this.matchedSkillsCount});

  PatientCarer.fromJson(
    Map<String, dynamic> data,
  )   : cardDate = convertDataToDateTime(data["cardDate"]),
        cardIssuer = data["cardIssuer"],
        cardNo = data["cardNo"],
        contactAddress = data["contactAddress"],
        contactPhone = data["contactPhone"],
        birthday = convertDataToDateTime(data["birthday"]),
        levelName = data["levelName"],
        fullName = data["fullName"],
        levelId = data["levelId"],
        id = data["id"],
        permanentAddress = data["permanentAddress"],
        preferLocationId = data["preferLocationId"],
        preferLocation = data["preferLocation"] != null
            ? Location.fromJson(data["preferLocation"])
            : null,
        signingDate = convertDataToDateTime(data["signingDate"]),
        gender = data["gender"],
        skills = data["personalSkills"] != null
            ? List<PatientCarerSkill>.from(data["personalSkills"]
                ?.map((e) => PatientCarerSkill.fromJson(e)))
            : null,
        level = data["level"] != null
            ? PatientCarerLevel.fromJson(data["level"])
            : null,
        cardPicture = data["cardPicture"],
        avatar = data["avatar"],
        matchedSkillsCount = data["matchedSkillsCount"];

  Map<String, dynamic> toJson() {
    return {
      "cardDate": cardDate?.toIso8601String(),
      "cardIssuer": cardIssuer,
      "cardNo": cardNo,
      "contactAddress": contactAddress,
      "contactPhone": contactPhone,
      "birthday": birthday?.toIso8601String(),
      "fullName": fullName,
      "levelId": levelId,
      "id": id,
      "gender": gender,
      "permanentAddress": permanentAddress,
      "preferLocationId": preferLocationId,
      "cardPicture": cardPicture,
      "signingDate": signingDate?.toIso8601String(),
      "skills": List<Map<String, dynamic>>.from(skills?.map((e) => e.toJson())),
      "avatar": avatar
    };
  }

  Map<String, dynamic> toParams() {
    return {
      "CardDate": cardDate?.toIso8601String(),
      "CardIssuer": cardIssuer,
      "CardNo": cardNo,
      "ContactAddress": contactAddress,
      "ContactPhone": contactPhone,
      "Birthday": birthday?.toIso8601String(),
      "FullName": fullName,
      "LevelId": levelId,
      "Id": id,
      "Gender": gender,
      "PermanentAddress": permanentAddress,
      "PreferLocationId": preferLocationId,
      "Skills": List<Map<String, dynamic>>.from(skills?.map((e) => e.toJson())),
      "Avatar": avatar
    };
  }
}
