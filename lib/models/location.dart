abstract class ObjectWithId {
  final String id;
  ObjectWithId({this.id});

  @override
  bool operator ==(other) => other is ObjectWithId && other.id == this.id;

  @override
  int get hashCode => id.hashCode;
}

class Province extends ObjectWithId {
  final String id;
  final String name;
  Province({this.id, this.name});

  Province.fromJson(Map<String, dynamic> data)
      : id = data["id"],
        name = data["name"];

  Province.fromSelectList(Map<String, dynamic> data)
      : id = data["value"],
        name = data["label"];
}

class District extends ObjectWithId {
  final String id;
  final String name;
  final String provinceId;
  District({this.id, this.name, this.provinceId});

  District.fromJson(Map<String, dynamic> data)
      : id = data["id"],
        name = data["name"],
        provinceId = data["provinceId"];

  District.fromSelectList(Map<String, dynamic> data)
      : id = data["value"],
        name = data["label"],
        provinceId = null;
}

class Ward extends ObjectWithId {
  final String id;
  final String name;
  final String districtId;
  final String provinceId;
  Ward({
    this.provinceId,
    this.id,
    this.districtId,
    this.name,
  });

  Ward.fromJson(Map<String, dynamic> data)
      : id = data["id"],
        name = data["name"],
        districtId = data["districtId"],
        provinceId = data["provinceId"];

  Ward.fromSelectList(Map<String, dynamic> data)
      : id = data["value"],
        name = data["label"],
        districtId = null,
        provinceId = null;
}

class Location extends ObjectWithId {
  final String id;
  final String name;
  final String description;
  final String streetAddress;
  Location({this.id, this.name, this.description, this.streetAddress});

  Location.fromJson(Map<String, dynamic> data)
      : id = data["id"],
        name = data["name"],
        description = data["description"],
        streetAddress = data["streetAddress"];

  Location.fromSelectList(Map<String, dynamic> data)
      : id = data["value"],
        name = data["label"],
        description = null,
        streetAddress = "";

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
      "description": description,
      "streetAddress": streetAddress
    };
  }
}

class LocationDepartment extends ObjectWithId {
  final String id;
  final String name;
  LocationDepartment({this.id, this.name});

  LocationDepartment.fromJson(Map<String, dynamic> data)
      : id = data["value"],
        name = data["label"];
}

class ContractSource extends ObjectWithId {
  final String id;
  final String name;
  ContractSource({this.id, this.name});

  ContractSource.fromJson(Map<String, dynamic> data)
      : id = data["value"],
        name = data["label"];
}
