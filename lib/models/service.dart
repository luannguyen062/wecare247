import 'package:wecare247/shared/index.dart';

class SelectListServiceData {
  final String value;
  final String label;
  final double hours;
  final double price;
  final bool priceNegotiation;

  SelectListServiceData(
      {this.value, this.hours, this.label, this.price, this.priceNegotiation});

  SelectListServiceData.fromJson(Map<String, dynamic> data)
      : value = data["value"],
        label = data["label"],
        hours = data["extra"]["hours"],
        price = data["extra"]["price"],
        priceNegotiation = data["extra"]["priceNegotiation"];
}

class Service {
  final String id;
  final String name;
  final double price;
  final bool priceNegotiation;
  Service({this.id, this.name, this.price, this.priceNegotiation});

  Service.fromJson(Map<String, dynamic> data)
      : id = data["id"],
        name = data["name"],
        price = data["price"]?.toDouble(),
        priceNegotiation = data["priceNegotiation"];

  Service.fromSelectList(SelectListServiceData data)
      : id = data.value,
        name = data.label,
        price = data.price,
        priceNegotiation = data.priceNegotiation;

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
      "price": price,
    };
  }
}

class ServiceEditData {
  String serviceId;
  String locationId;
  DateTime from;
  DateTime to;
  String contractId;
  String patientCarerId;
  double price;
  ServiceEditData(
      {this.serviceId,
      this.from,
      this.locationId,
      this.to,
      this.contractId,
      this.patientCarerId,
      this.price});

  Map<String, dynamic> toJson() => {
        "serviceId": serviceId,
        "locationId": locationId,
        "from": from.toIso8601String(),
        "to": to.toIso8601String(),
        "contractId": contractId,
        "patientCarerId": patientCarerId,
        "price": price,
      };
}

class ServiceData {
  final String id;
  final String serviceId;
  final String serviceName;
  final String patientCarerId;
  final String patientCarerName;
  final DateTime from;
  final DateTime to;
  final double calendarHours;
  final double actualHours;
  final double totalPrice;
  final double price;
  final DateTime created;
  ServiceData({
    this.id,
    this.serviceName,
    this.patientCarerName,
    this.serviceId,
    this.patientCarerId,
    this.from,
    this.to,
    this.calendarHours,
    this.price,
    this.created,
    this.totalPrice,
    this.actualHours,
  });

  ServiceData.fromJson(Map<String, dynamic> data)
      : id = data["id"],
        serviceId = data["serviceId"],
        serviceName = data["serviceName"],
        patientCarerName = data["patientCarerName"],
        patientCarerId = data["patientCarerId"],
        from = convertDataToDateTime(data["from"]),
        to = convertDataToDateTime(data["to"]),
        created = convertDataToDateTime(data["created"]),
        calendarHours = data["calendarHours"]?.toDouble(),
        totalPrice = data["totalPrice"]?.toDouble(),
        price = data["price"]?.toDouble(),
        actualHours = data["actualHours"]?.toDouble();
}
