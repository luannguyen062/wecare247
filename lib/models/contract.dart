import 'package:wecare247/models/index.dart';
import 'package:wecare247/shared/utils/index.dart';

class ContractShort {
  final String id;
  final DateTime signingDate;
  final DateTime effectFrom;
  final DateTime effectTo;
  final int totalDays;
  final double totalAmount;
  final double paymentAmount;
  final double paidAmount;
  final double remainingAmount;
  final int actualDays;
  final String signingBy;
  final String signingByName;
  final String customerId;
  final String customerName;
  final String contractSourceName;
  final String address;
  final String wardName;
  final String districtName;
  final String provinceName;
  final String note;
  final int status;
  final String statusValue;
  ContractShort(
      {this.id,
      this.signingDate,
      this.effectFrom,
      this.effectTo,
      this.totalDays,
      this.totalAmount,
      this.paymentAmount,
      this.paidAmount,
      this.remainingAmount,
      this.actualDays,
      this.signingBy,
      this.signingByName,
      this.customerId,
      this.customerName,
      this.contractSourceName,
      this.address,
      this.wardName,
      this.districtName,
      this.provinceName,
      this.note,
      this.status,
      this.statusValue});

  ContractShort.fromJson(Map<String, dynamic> data)
      : id = data["id"],
        signingDate = convertDataToDateTime(data["signingDate"]),
        signingBy = data["signingBy"],
        signingByName = data["signingByName"],
        effectFrom = convertDataToDateTime(data["effectFrom"]),
        effectTo = convertDataToDateTime(data["effectTo"]),
        totalDays = data["totalDays"],
        totalAmount = data["totalAmount"],
        paymentAmount = data["paymentAmount"],
        paidAmount = data["paidAmount"],
        remainingAmount = data["remainingAmount"],
        actualDays = data["actualDays"],
        customerId = data["customerId"],
        customerName = data["customerName"],
        contractSourceName = data["contractSourceName"],
        address = data["address"],
        wardName = data["wardName"],
        districtName = data["districtName"],
        provinceName = data["provinceName"],
        note = data["note"],
        status = data["status"],
        statusValue = data["statusValue"];
}

class ContractDetail {
  final String id;
  final DateTime signingDate;
  final String signingBy;
  final String customerId;
  final String address;
  final String note;
  final String contractLocationId;
  final String contractLocationName;
  final String contractSourceId;
  final String contractSourceName;
  final String locationDepartmentId;
  final String locationDepartmentName;
  final String patientId;
  final int status;
  final DateTime effectFrom;
  final DateTime effectTo;
  final double totalHours;
  final double totalAmount;
  final double paymentAmount;
  final double paidAmount;
  final double remainingAmount;
  final double actualDays;
  final CustomerDetail customer;
  final PatientDetail patient;
  final List<ServiceData> services;
  final List<PatientCarerSkill> requiredSkills;

  ContractDetail(
      {this.id,
      this.signingDate,
      this.signingBy,
      this.customerId,
      this.address,
      this.note,
      this.contractLocationId,
      this.contractLocationName,
      this.customer,
      this.status,
      this.effectFrom,
      this.effectTo,
      this.totalHours,
      this.totalAmount,
      this.paymentAmount,
      this.paidAmount,
      this.contractSourceId,
      this.contractSourceName,
      this.locationDepartmentId,
      this.locationDepartmentName,
      this.patientId,
      this.patient,
      this.remainingAmount,
      this.requiredSkills,
      this.actualDays,
      this.services});

  ContractDetail.fromJson(Map<String, dynamic> data)
      : status = data["status"],
        id = data["id"],
        signingDate = convertDataToDateTime(data["signingDate"]),
        effectFrom = convertDataToDateTime(data["effectFrom"]),
        effectTo = convertDataToDateTime(data["effectTo"]),
        totalHours = data["totalHours"],
        totalAmount = data["totalAmount"],
        paymentAmount = data["paymentAmount"],
        paidAmount = data["paidAmount"],
        remainingAmount = data["remainingAmount"],
        actualDays = data["actualDays"],
        address = data["address"],
        note = data["note"],
        customer = data["customer"] != null
            ? CustomerDetail.fromJson(data["customer"])
            : null,
        services = data["services"] != null
            ? List<ServiceData>.from(
                data["services"].map((e) => ServiceData.fromJson(e)))
            : null,
        requiredSkills = data["requiredSkills"] != null
            ? List<PatientCarerSkill>.from(data["requiredSkills"]
                .map((e) => PatientCarerSkill.fromJson(e)))
            : null,
        contractLocationId = data["contractLocationId"],
        contractLocationName = data["contractLocationName"],
        contractSourceId = data["contractSourceId"],
        contractSourceName = data["contractSourceName"],
        locationDepartmentId = data["locationDepartmentId"],
        locationDepartmentName = data["locationDepartmentName"],
        patientId = data["patientId"],
        patient = data["patient"] != null
            ? PatientDetail.fromJson(data["patient"])
            : null,
        customerId = data["customerId"],
        signingBy = data["signingBy"];
}

// API Event Model
class CreateContractDto {
  final String note;
  final String contractLocationId;
  final String contractSourceId;
  final String locationDepartmentId;
  final CreateCustomerDto customer;
  final CreatePatientDto patient;
  final List<ServiceEditData> services;
  final List<PatientCarerSkill> requiredSkills;
  final bool useDefaultInformation;
  CreateContractDto(
      {this.contractSourceId,
      this.locationDepartmentId,
      this.note,
      this.contractLocationId,
      this.customer,
      this.patient,
      this.services,
      this.requiredSkills,
      this.useDefaultInformation});

  Map<String, dynamic> toJson() {
    return {
      "note": note,
      "contractLocationId": contractLocationId,
      "contractSourceId": contractSourceId,
      "locationDepartmentId": locationDepartmentId,
      "customer": customer.toJson(),
      "patient": patient.toJson(),
      "useDefaultInformation": useDefaultInformation,
      "services":
          List<Map<String, dynamic>>.from(services.map((e) => e.toJson())),
      "requiredSkills":
          List<Map<String, dynamic>>.from(requiredSkills.map((e) => e.toJson()))
    };
  }
}

class GetContractDto {
  final String contractId;
  final DateTime from;
  final DateTime to;
  final String userId;
  final List<int> status;
  final int page;
  final int size;
  final String orderBy;
  final bool desc;
  GetContractDto(
      {this.contractId,
      this.desc,
      this.from,
      this.to,
      this.orderBy,
      this.userId,
      this.page,
      this.size,
      this.status});

  String toParams() {
    List<String> urlComponent = [];
    if (contractId != null) {
      urlComponent.add("ContractId=$contractId");
    }
    if (from != null) {
      urlComponent.add("From=${from.toIso8601String()}");
    }
    if (to != null) {
      urlComponent.add("To=${to.toIso8601String()}");
    }
    if (orderBy != null) {
      urlComponent.add("OrderBy=$orderBy");
    }
    if (userId != null) {
      urlComponent.add("UserId=$userId");
    }
    if (page != null) {
      urlComponent.add("Page=$page");
    }
    if (size != null) {
      urlComponent.add("Size=$size");
    }
    if (status != null && status.isNotEmpty) {
      status.forEach((e) => urlComponent.add("Status=$e"));
    }
    if (desc != null) {
      urlComponent.add("Desc=$desc");
    }
    return Uri.encodeFull(urlComponent.join("&"));
  }
}

class SelectPatientCarerDto {
  final String contractId;
  final String patientCarerId;
  SelectPatientCarerDto({this.contractId, this.patientCarerId});

  Map<String, dynamic> toJson() {
    return {"contractId": contractId, "patientCarerId": patientCarerId};
  }
}

class EditContractDto {
  final String id;
  final String contractLocationId;
  final String contractSourceId;
  final String locationDepartmentId;
  final List<PatientCarerSkill> requiredSkills;
  EditContractDto(
      {this.contractSourceId,
      this.locationDepartmentId,
      this.id,
      this.contractLocationId,
      this.requiredSkills});

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "contractLocationId": contractLocationId,
      "contractSourceId": contractSourceId,
      "locationDepartmentId": locationDepartmentId,
      "requiredSkills": requiredSkills != null
          ? List.from(requiredSkills.map((e) => e.toJson()))
          : null,
    };
  }
}

class CreateNewServiceDto {
  final String serviceId;
  final String locationId;
  final DateTime from;
  final DateTime to;
  final String contractId;
  CreateNewServiceDto(
      {this.serviceId, this.contractId, this.from, this.locationId, this.to});

  Map<String, dynamic> toJson() {
    return {
      "contractId": contractId,
      "locationId": locationId,
      "from": from.toIso8601String(),
      "to": to.toIso8601String(),
      "serviceId": serviceId,
    };
  }
}
