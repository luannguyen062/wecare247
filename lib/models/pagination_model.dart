class PaginationModel<T> {
  final int pages;
  final int size;
  final int currentPage;
  final List<T> items;
  PaginationModel({this.pages, this.size, this.currentPage, this.items});

  PaginationModel.fromJson(Map<String, dynamic> data,
      {T Function(dynamic) converter})
      : pages = data["pages"],
        size = data["size"],
        currentPage = data["currentPage"],
        items = List<T>.from(data["items"].map(converter ?? (e) => e));

  Map<String, dynamic> toParams() => {
        "Size": size,
        "Page": currentPage,
      };
}
