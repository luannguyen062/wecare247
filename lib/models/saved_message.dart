import 'package:wecare247/shared/index.dart';

class SavedMessage {
  final String createdBy;
  final DateTime created;
  final String lastModifiedBy;
  final DateTime lastModified;
  final String id;
  final bool isDeleted;
  final String to;
  final String toRole;
  final String toGroup;
  final String message;
  final String messageHash;
  final bool processed;
  final String processedBy;
  SavedMessage(
      {this.createdBy,
      this.created,
      this.lastModifiedBy,
      this.lastModified,
      this.id,
      this.isDeleted,
      this.to,
      this.toRole,
      this.toGroup,
      this.message,
      this.messageHash,
      this.processed,
      this.processedBy});

  SavedMessage.fromJson(Map<String, dynamic> data)
      : createdBy = data["createdBy"],
        created = convertDataToDateTime(data["created"]),
        lastModifiedBy = data["lastModifiedBy"],
        lastModified = convertDataToDateTime(data["lastModified"]),
        id = data["id"],
        isDeleted = data["isDeleted"],
        to = data["to"],
        toRole = data["toRole"],
        toGroup = data["toGroup"],
        message = data["message"],
        messageHash = data["messageHash"],
        processed = data["processed"],
        processedBy = data["processedBy"];
}
