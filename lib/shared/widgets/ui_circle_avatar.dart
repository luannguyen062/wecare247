import 'package:flutter/material.dart';

class UICircleAvatar extends StatelessWidget {
  final String imageUrl;
  final double size;
  const UICircleAvatar({Key key, this.imageUrl, this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (imageUrl == null || imageUrl.isEmpty) {
      return CircleAvatar(
          radius: size / 2,
          backgroundColor: Colors.grey[800],
          child: Icon(
            Icons.person,
            size: size,
            color: Colors.grey,
          ));
    }
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(size / 2),
        image: DecorationImage(
          image: Image.network(imageUrl).image,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
