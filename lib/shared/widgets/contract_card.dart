import 'package:wecare247/bloc/contract/contract_bloc.dart';
import 'package:wecare247/models/index.dart';
import 'package:wecare247/shared/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class ContractCard extends StatefulWidget {
  final ContractShort contract;
  const ContractCard({Key key, this.contract}) : super(key: key);

  @override
  _ContractCardState createState() => _ContractCardState();
}

class _ContractCardState extends State<ContractCard> {
  Widget buildButtonRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          width: 80,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              primary: UIColors.mainColor(),
            ),
            child: FittedBox(
              child: Text(
                "Xem thông tin",
                style: fontMedium.copyWith(color: Colors.white, fontSize: 10),
              ),
            ),
            onPressed: () => Navigator.of(context).pushNamed("contract_detail",
                arguments: {"id": widget.contract.id}),
          ),
        ),
        widget.contract.status == 10
            ? SizedBox(
                width: 80,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                    primary: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                        side:
                            BorderSide(color: UIColors.mainColor(), width: 2)),
                  ),
                  child: FittedBox(
                    child: Text(
                      "PING",
                      style: fontMedium.copyWith(color: UIColors.mainColor()),
                    ),
                  ),
                  onPressed: () {
                    BlocProvider.of<ContractBloc>(context)
                        .pingMonitor(contractId: widget.contract.id);
                  },
                ),
              )
            : Container(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    String statusText;
    Color statusColor;
    switch (widget.contract.status) {
      case 0:
        statusText = "Chờ xử lý";
        statusColor = Colors.orangeAccent;
        break;
      case 10:
        statusText = "Khởi tạo";
        statusColor = Colors.blueAccent;
        break;
      case 20:
        statusText = "Đang hiệu lực";
        statusColor = Colors.green;
        break;
      case 100:
        statusText = "Hoàn thành";
        statusColor = Colors.purple;
        break;
      case 99:
      default:
        statusText = "Đã hủy";
        statusColor = Colors.red;
    }
    final formatter = DateFormat('dd/MM/yyyy');
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
          border: Border.all(width: 3, color: UIColors.mainColor()),
          borderRadius: BorderRadius.circular(24),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                blurRadius: 2, offset: Offset(4, 4), color: Colors.black45)
          ]),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "HỢP ĐỒNG ${widget.contract.id}",
                    style:
                        fontMedium.copyWith(fontSize: 14, color: Colors.black),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Ký ngày: ${formatter.format(widget.contract.effectFrom)}",
                            style: fontMedium.copyWith(
                                color: Colors.black, fontSize: 10),
                          ),
                          Text.rich(
                            TextSpan(text: "Trạng thái: ", children: [
                              TextSpan(
                                  text: statusText,
                                  style: TextStyle(color: statusColor))
                            ]),
                            style: fontMedium.copyWith(fontSize: 10),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Row(
                    children: [
                      UICircleAvatar(
                        size: 60,
                      ),
                      SizedBox(width: 8),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Khách hàng: ${widget.contract.customerName}",
                              style: fontMedium.copyWith(color: Colors.black),
                            ),
                            SizedBox(height: 8),
                            Container(
                              width: 200,
                              child: Text(
                                "Tại: ${widget.contract.contractSourceName}",
                                style: fontMedium.copyWith(color: Colors.black),
                                softWrap: true,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ],
          ),
          buildButtonRow()
        ],
      ),
    );
  }
}
