import 'package:flutter/material.dart';

class UIAppBarCenter extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final String textAlignmentRight;
  final String router;
  final VoidCallback onPressTextAlignmentRight;
  final double textAlignmentRightSize;
  final Color backgroundColor;
  UIAppBarCenter(
      {this.title = "",
      this.textAlignmentRight = "",
      this.router = "",
      this.onPressTextAlignmentRight,
      this.backgroundColor,
      this.textAlignmentRightSize});

  @override
  Size get preferredSize => Size.fromHeight(50);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      elevation: 1,
      backgroundColor: backgroundColor ?? Theme.of(context).appBarTheme.color,
      title: Container(
        height: preferredSize.height,
        child: Stack(
          alignment: AlignmentDirectional.center,
          children: [
            Positioned(
              left: 0,
              right: 0,
              child: Container(
                width: double.maxFinite,
                child: Center(
                  child: Text(title, style: TextStyle(fontSize: 20)),
                ),
              ),
            ),
            Positioned(
              left: 0,
              child: IconButton(
                alignment: AlignmentDirectional.centerStart,
                padding: const EdgeInsets.all(0),
                onPressed: () => Navigator.of(context).pop(),
                icon: Icon(
                  Icons.arrow_back_ios,
                ),
              ),
            ),
            Positioned(
              right: 0,
              child: GestureDetector(
                onTap: onPressTextAlignmentRight ?? () {},
                child: Text(
                  textAlignmentRight,
                  style: TextStyle(fontSize: textAlignmentRightSize ?? 16),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
