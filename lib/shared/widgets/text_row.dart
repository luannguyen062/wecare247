import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';

class TextRow extends StatelessWidget {
  final String title;
  final String value;
  final Color valueColor;
  final Color titleColor;
  final Function() onTap;
  const TextRow(
      {Key key,
      this.title,
      this.value,
      this.onTap,
      this.valueColor = Colors.black,
      this.titleColor = Colors.black})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onTap,
      style: TextButton.styleFrom(
        padding: EdgeInsets.symmetric(vertical: 8),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: fontMedium.copyWith(color: titleColor, fontSize: 15),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(value,
                  style: fontBook.copyWith(color: valueColor, fontSize: 14)),
              onTap != null
                  ? Container(
                      height: 18,
                      width: 24,
                      margin: EdgeInsets.only(left: 6),
                      child: Icon(
                        Icons.chevron_right,
                        color: Colors.black,
                        size: 18,
                      ),
                    )
                  : Container()
            ],
          ),
        ],
      ),
    );
  }
}
