import 'package:wecare247/shared/widgets/ui_switch.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UIRowWithSwitch extends StatefulWidget {
  final Widget icon;
  final String label;
  final Widget child;
  final bool initialValue;
  final Function(bool) onSwitchCallback;
  UIRowWithSwitch(
      {Key key,
      this.onSwitchCallback,
      this.child,
      this.icon,
      this.label,
      this.initialValue})
      : assert((child != null ||
                (child == null && icon != null && label != null)) &&
            initialValue != null),
        super(key: key);

  @override
  _UIRowWithSwitchState createState() => _UIRowWithSwitchState();
}

class _UIRowWithSwitchState extends State<UIRowWithSwitch> {
  Widget buildDefaultStyleChild() {
    return Row(
      children: <Widget>[
        widget.icon,
        SizedBox(width: 10),
        Text(
          widget.label,
          style: TextStyle(
            fontSize: 14,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget child;
    if (widget.child != null) {
      child = child;
    } else {
      child = buildDefaultStyleChild();
    }
    return Row(
      children: <Widget>[
        Expanded(child: child),
        UISwitch(
          onSwitch: widget.onSwitchCallback,
          initialValue: widget.initialValue,
        ),
      ],
    );
  }
}
