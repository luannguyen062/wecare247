import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';

class IndexPageIndicator extends StatefulWidget {
  final ValueNotifier<int> pageNotifier;
  final ValueNotifier<int> pageCountNotifier;
  final Function(int) goToPageFunction;
  IndexPageIndicator(
      {Key key,
      this.pageNotifier,
      this.pageCountNotifier,
      this.goToPageFunction})
      : super(key: key);

  @override
  _IndexPageIndicatorState createState() => _IndexPageIndicatorState();
}

class _IndexPageIndicatorState extends State<IndexPageIndicator> {
  int currentPage;
  int pageCount;

  Widget buildPageIndicatorGlobe(int index) {
    bool isSelected = index == currentPage;
    return Container(
      height: 30,
      width: 30,
      margin: EdgeInsets.symmetric(horizontal: 4),
      child: TextButton(
        onPressed: () {
          if (pageCount == null || currentPage == null) return;
          if (currentPage != index) widget.goToPageFunction(index);
        },
        style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          padding: EdgeInsets.zero,
          backgroundColor:
              isSelected ? UIColors.mainColor() : Colors.transparent,
        ),
        child: Text(
          index.toString(),
          style: fontMedium.copyWith(
              color: isSelected ? Colors.white : UIColors.mainColor(),
              fontSize: 14),
        ),
      ),
    );
  }

  Widget buildPageNavigatorButton({IconData icon, Function onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(8),
        child: Icon(
          icon,
          size: 30,
          color: UIColors.mainColor(),
        ),
      ),
    );
  }

  Widget goToPreviousPage() {
    return buildPageNavigatorButton(
      onTap: () {
        if (currentPage > 1) {
          widget.goToPageFunction(currentPage - 1);
        }
      },
      icon: Icons.navigate_before,
    );
  }

  Widget goToFirstPage() {
    return buildPageNavigatorButton(
      onTap: () {
        if (currentPage > 1) {
          widget.goToPageFunction(1);
        }
      },
      icon: Icons.first_page,
    );
  }

  Widget goToNextPage() {
    return buildPageNavigatorButton(
      onTap: () {
        if (currentPage < pageCount) {
          widget.goToPageFunction(currentPage + 1);
        }
      },
      icon: Icons.navigate_next,
    );
  }

  Widget goToLastPage() {
    return buildPageNavigatorButton(
      onTap: () {
        if (currentPage < pageCount) {
          widget.goToPageFunction(pageCount);
        }
      },
      icon: Icons.last_page,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: widget.pageNotifier,
      builder: (context, value, child) {
        currentPage = widget.pageNotifier.value;
        pageCount = widget.pageCountNotifier.value;
        List<Widget> children = [
          goToFirstPage(),
          goToPreviousPage(),
        ];
        if (pageCount != null && currentPage != null) {
          int index =
              pageCount - currentPage > 2 ? -2 : pageCount - currentPage - 4;
          int currentGlobeCount = 0;
          while (currentGlobeCount < 5 && index + currentPage <= pageCount) {
            if (index + currentPage > 0) {
              children.add(buildPageIndicatorGlobe(currentPage + index));
              currentGlobeCount += 1;
            }
            index += 1;
          }
        }
        children.addAll([goToNextPage(), goToLastPage()]);
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: children,
        );
      },
    );
  }
}
