import 'package:flutter/material.dart';

class TapableText extends StatefulWidget {
  final Color normalColor;
  final Color onTapColor;
  final String text;
  final TextStyle textStyle;
  final Function() onTap;
  TapableText(
      {Key key,
      this.normalColor,
      this.onTapColor,
      this.text,
      this.textStyle,
      this.onTap})
      : super(key: key);

  @override
  _TapableTextState createState() => _TapableTextState();
}

class _TapableTextState extends State<TapableText> {
  bool isSelected;
  @override
  void initState() {
    isSelected = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (details) {
        setState(() {
          isSelected = true;
        });
      },
      onTap: widget.onTap,
      onTapUp: (details) => setState(() {
        isSelected = false;
      }),
      onTapCancel: () => setState(() {
        isSelected = false;
      }),
      child: Text(
        widget.text,
        style: widget.textStyle.copyWith(
            color: isSelected ? widget.onTapColor : widget.normalColor),
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}
