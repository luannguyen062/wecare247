import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UISwitch extends StatefulWidget {
  UISwitch({Key key, this.onSwitch, this.initialValue = false})
      : super(key: key);

  final Function onSwitch;
  final bool initialValue;

  @override
  State<StatefulWidget> createState() => _UISwitchState();
}

class _UISwitchState extends State<UISwitch> {
  bool _enabled;

  @override
  void initState() {
    super.initState();
    _enabled = widget.initialValue;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        CupertinoSwitch(
          value: _enabled,
          onChanged: (newValue) {
            widget.onSwitch(newValue);
            setState(() {
              _enabled = newValue;
            });
          },
          trackColor: Colors.lightGreenAccent[400],
        ),
        Positioned(
            top: 12,
            left: 8,
            child: IgnorePointer(
              child: _enabled
                  ? Text(
                      "ON",
                      style: TextStyle(
                        fontSize: 9,
                      ),
                    )
                  : Container(),
            )),
        Positioned(
            top: 12,
            right: 6,
            child: IgnorePointer(
              child: !_enabled
                  ? Text(
                      "OFF",
                      style: TextStyle(
                        fontSize: 9,
                      ),
                    )
                  : Container(),
            )),
      ],
    );
  }
}
