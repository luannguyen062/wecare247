import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';

class UIButton extends StatelessWidget {
  final double width;
  final double height;
  final String label;
  final TextStyle labelStyle;
  final Widget prefix;
  final Color backgroundColor;
  final Color color;
  final Color borderColor;
  final BoxFit fit;
  final BorderRadius borderRadius;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final Function onPressCallback;
  UIButton(
      {this.width,
      this.height,
      this.label,
      this.labelStyle,
      this.prefix,
      this.backgroundColor,
      this.color,
      this.borderColor,
      this.fit = BoxFit.scaleDown,
      this.borderRadius,
      this.margin = const EdgeInsets.all(0),
      this.padding = const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      this.onPressCallback});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: margin,
      width: width,
      height: height,
      color: Colors.transparent,
      child: Center(
        child: TextButton(
          style: TextButton.styleFrom(
            padding: padding,
            backgroundColor: backgroundColor,
            shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: borderColor ??
                        (backgroundColor != Colors.transparent &&
                                backgroundColor != null
                            ? backgroundColor
                            : Theme.of(context).buttonColor),
                    width: 2),
                borderRadius: borderRadius ?? BorderRadius.circular(6)),
          ),
          onPressed: onPressCallback,
          child: FittedBox(
            fit: fit,
            child: Row(
              children: <Widget>[
                prefix ?? Container(),
                prefix != null ? SizedBox(width: 10) : Container(),
                Text(label,
                    style: labelStyle ??
                        fontMedium.copyWith(
                            fontSize: 15,
                            color: color ??
                                Theme.of(context)
                                    .primaryTextTheme
                                    .button
                                    .color)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
