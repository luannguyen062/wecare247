import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';

class TextCol extends StatelessWidget {
  final String title;
  final String value;
  final Color valueColor;
  final Color titleColor;
  final Function() onTap;
  const TextCol(
      {Key key,
      this.title,
      this.value,
      this.onTap,
      this.valueColor = Colors.black,
      this.titleColor = Colors.black})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title ?? "",
                    style:
                        fontMedium.copyWith(color: titleColor, fontSize: 15)),
                SizedBox(height: 4),
                Text(
                  value ?? "",
                  style: fontBook.copyWith(color: valueColor, fontSize: 14),
                  softWrap: true,
                )
              ],
            ),
          ),
          onTap != null
              ? IconButton(
                  icon: Icon(Icons.edit),
                  color: Colors.black,
                  onPressed: onTap,
                )
              : Container()
        ],
      ),
    );
  }
}
