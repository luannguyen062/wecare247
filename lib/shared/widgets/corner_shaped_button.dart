import 'package:wecare247/shared/index.dart';
import 'package:flutter/material.dart';

class CornerShapeButton extends StatelessWidget {
  const CornerShapeButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Material(
        shape: BeveledRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15), bottomLeft: Radius.circular(15)),
            side: BorderSide(color: UIColors.mainColor())),
        child: Padding(
          padding: EdgeInsets.only(top: 4, bottom: 4, left: 20, right: 10),
          child: Text("Ping", style: fontMedium.copyWith(fontSize: 10)),
        ),
      ),
    );
  }
}
