import 'package:flutter/cupertino.dart';
import 'package:wecare247/main.dart';
import 'package:wecare247/shared/index.dart';

Future<String> validateToken() async {
  final String token = await LocalCache.getSecured(USER_ACCESS_TOKEN_KEY);
  if (token == null) {
    return null;
  }
  DateTime currentTime = DateTime.now().toUtc();
  DateTime expireTime =
      DateTime.parse(await LocalCache.getSecured(USER_TOKEN_EXPIRE_KEY))
          .toUtc();
  if (currentTime.isAfter(expireTime)) {
    return null;
  }
  return token;
}

void returnToLoginScreen() {
  navKey.currentState.popUntil(ModalRoute.withName(""));
  navKey.currentState.pushNamed('login');
}
