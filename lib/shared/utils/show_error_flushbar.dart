import 'package:wecare247/main.dart';
import 'package:wecare247/shared/index.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

void showErrorFlushbar(String message, [bool allowPopup = false]) {
  Future.delayed(
    Duration(milliseconds: 100),
    () {
      if (message.length > 30 && allowPopup) {
        showDialog(
            context: navKey.currentContext,
            builder: (context) => Center(
                  child: AlertDialog(
                    title: Text(
                      "Lỗi",
                    ),
                    titleTextStyle:
                        fontMedium.copyWith(fontSize: 16, color: Colors.black),
                    content: Text(message),
                    contentTextStyle:
                        fontBook.copyWith(fontSize: 12, color: Colors.black),
                  ),
                ));
      } else {
        Flushbar(
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.TOP,
          messageText: Text(
            message,
            style: TextStyle(color: Colors.white),
          ),
        )..show(navKey.currentContext);
      }
    },
  );
}
