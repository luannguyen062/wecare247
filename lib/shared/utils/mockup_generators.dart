import 'dart:math';

String getMockupName() {
  final firstGroup = ["Trần", "Lê", "Nguyễn"];
  final secondGroup = ["Văn", "Thanh", "Anh"];
  final thirdGroup = ["Khoa", "Thu", "Nhi"];
  final first = firstGroup[Random().nextInt(firstGroup.length - 1)];
  final second = secondGroup[Random().nextInt(firstGroup.length - 1)];
  final third = thirdGroup[Random().nextInt(firstGroup.length - 1)];
  return [first, second, third].join(" ");
}

String getMockupNumber() {
  final first = Random().nextInt(99999);
  final second = Random().nextInt(99999);
  return [first.toString().padLeft(5, "0"), second.toString().padLeft(5, "0")]
      .join();
}
