import 'dart:async';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

// Config get and put method to cache data using Shared Preferences
class LocalCache {
  const LocalCache();
  static final secureStorage = FlutterSecureStorage();
  // Get value of a key
  static Future<dynamic> get(
    String key,
  ) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get(key);
  }

  static Future<String> getSecured(
    String key,
  ) async {
    return await secureStorage.read(key: key);
  }

  static Future<void> putSecured(String key, String value) async {
    await secureStorage.write(key: key, value: value);
  }

  static Future<void> deleteSecured(String key) async {
    await secureStorage.delete(key: key);
  }

  // Create/ Update a Key, Value pair into cache
  static Future<void> put(String key, dynamic value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    switch (value.runtimeType) {
      case int:
        prefs.setInt(key, value);
        break;
      case bool:
        prefs.setBool(key, value);
        break;
      case double:
        prefs.setDouble(key, value);
        break;
      case List:
        prefs.setStringList(key, value);
        break;
      case String:
      default:
        prefs.setString(key, value);
        break;
    }
  }

  // Remove a Key, Value pair from cache
  static Future<void> remove(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  // Remove a list of Keys from cache
  static Future<void> removeKeys(List<String> keys) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    keys.forEach((key) => prefs.remove(key));
  }

  static Future<dynamic> clear() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.clear();
  }
}
