// import 'package:wecare247/models/index.dart';

DateTime convertDataToDateTime(dynamic data) {
  return data != null ? DateTime.parse(data + "Z") : null;
}

String convertIntToGender(int value) {
  switch (value) {
    case 0:
      return "Khác";
    case 1:
      return "Nam";
    case 2:
    default:
      return "Nữ";
  }
}
