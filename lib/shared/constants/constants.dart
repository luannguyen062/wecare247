import 'package:wecare247/models/customer.dart';

const String EMAIL_REGEX =
    r'''(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])''';

// API
// const API_BASE_URL = "https://test-apps.nguoinuoibenh.com/api/";
// const STORAGE_KEY_PREFIX = 'test-';
// const FIREBASE_PREFIX = 'Test-';

const API_BASE_URL = "https://test-v1.nguoinuoibenh.com/api/";
const STORAGE_KEY_PREFIX = 'test-prod-';
const FIREBASE_PREFIX = 'TestV1-';

// const API_BASE_URL = "https://apps.nguoinuoibenh.com/api/";
// const STORAGE_KEY_PREFIX = 'prod-';
// const FIREBASE_PREFIX = 'Production-';

// Login API
const USER_LOGIN = "oauth/token";

// Customer API
const CUSTOMER_PREFIX = 'customers';

// Patient Carer API
const PATIENT_CARER_PREFIX = "patient-carers";

// Get Server Data API
const PROVINCE_LIST = "provinces";
const DISTRICT_LIST = "districts";
const WARD_LIST = "wards";
const LOCATION_LIST = "locations";
const PATIENT_CARER_LEVEL_LIST = "patient-carer-levels";
const GET_PATIENT_CARER_SKILLS = "require-skills";
const GET_SERVICE_LIST = "services";
const GET_LOCATION_DEPARTMENT_LIST = "location-departments";
const GET_CONTRACT_SOURCE_LIST = "contract-sources";

// Select List API
const SELECT_LIST_PREFIX = "select-list";

// Vision API
const FILE_UPLOAD = "files";
// const VERIFY_ID_IMAGE = "vision/id/verify";

// Contract API
const CONTRACT_PREFIX = "contracts";
const GET_SUGGEST_PATIENT_CARER = "patient-carer-suggest";
const SELECT_PATIENT_CARER = "patient-carer-select";
const CONFIRM_PATIENT_CARER = "patient-carers";
const PING_MONITOR = "ping";

// Notification API
const NOTIFICATION_PREFIX = "notifications";
const SEND_FIREBASE_TOKEN = "connections";

// Contract API
const CONTRACT_SERVICE_PREFIX = "contract-services";

// Typeahead API
const TYPEAHEAD_PREFIX = "typeahead";

// Cache key
const USER_ACCESS_TOKEN_KEY = STORAGE_KEY_PREFIX + "accessKey";
const USER_REFRESH_TOKEN_KEY = STORAGE_KEY_PREFIX + "refreshKey";
const USER_TOKEN_EXPIRE_KEY = STORAGE_KEY_PREFIX + "expireAt";
const FIRST_TIME = STORAGE_KEY_PREFIX + "firstTimeOpening";
const USER_NAME_KEY = STORAGE_KEY_PREFIX + "userCode";
const USER_ROLE_KEY = STORAGE_KEY_PREFIX + "userRole";
const USER_ID_KEY = STORAGE_KEY_PREFIX + "userId";

// Error
const TOKEN_EXPIRE_MESSAGE = 'Phiên đăng nhập hết hạn';

// Default Data
final defaultCustomer = CreateCustomerDto(
    fullName: "Mặc định",
    avatar: "",
    contactPhone: "Chưa có",
    wardId: "26734",
    districtId: "760HH",
    provinceId: "79TTT",
    street: "Chưa có",
    gender: 0,
    note: "");
final defaultPatient = CreatePatientDto(
    fullName: "Mặc định", yearOfBirth: 1985, gender: 0, weight: 50);
