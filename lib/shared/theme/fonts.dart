import 'package:flutter/cupertino.dart';

final fontLight = TextStyle(fontWeight: FontWeight.w200);
final fontBook = TextStyle(fontWeight: FontWeight.w400);
final fontMedium = TextStyle(fontWeight: FontWeight.w600);
final fontSemiBold = TextStyle(fontWeight: FontWeight.w800);
final fontBold = TextStyle(fontWeight: FontWeight.w900);
