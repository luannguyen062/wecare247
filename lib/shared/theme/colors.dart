import 'package:flutter/cupertino.dart';

class UIColors {
  static Color _mainColor = Color(0xFF0e7b6b);
  static Color _mainDarkColor = Color(0xFF7986CB);
  static Color _secondColor = Color(0x11111111);
  static Color _secondDarkColor = Color(0xdddddddd);
  static Color _accentColor = Color(0x11111111);
  static Color _accentDarkColor = Color(0xeeeeeeee);

  static const Color borderColor = Color(0xffE0E0E0);

  static Color mainColor([double opacity = 1]) {
    return _mainColor.withOpacity(opacity);
  }

  static Color secondColor([double opacity = 1]) {
    return _secondColor.withOpacity(opacity);
  }

  static Color accentColor([double opacity = 1]) {
    return _accentColor.withOpacity(opacity);
  }

  static Color mainDarkColor([double opacity = 1]) {
    return _mainDarkColor.withOpacity(opacity);
  }

  static Color secondDarkColor([double opacity = 1]) {
    return _secondDarkColor.withOpacity(opacity);
  }

  static Color accentDarkColor([double opacity = 1]) {
    return _accentDarkColor.withOpacity(opacity);
  }
}
